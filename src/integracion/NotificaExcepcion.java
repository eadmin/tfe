package src.integracion;

public class NotificaExcepcion  extends Exception {
	
	private static final long serialVersionUID = -3529425377061029445L;
	
	public NotificaExcepcion(String mensaje) {
		super(mensaje);
	}

	public NotificaExcepcion(String mensaje, Throwable error) {
		super(mensaje, error);
	}
}
