package src.integracion;



public enum EstadoNotificacion_enum {
	NO_EMITIDA_PROBLEMA_TECNICO(-9, true, "Fallo en emisi�n", "Ocurri� un fallo en TRAMITADOR al emitir por no conexi�n con Notifica, problema al persistir en base de datos u otra causa", "rojo-palido"),	
	DESISTIDA(-8, true, "Desistida", "Notificaci�n en via muerta en TRAMITADOR por superarse el n�mero de intentos de emisi�n hacia Notifica, consultar logs en TRAMITADOR", "rojo-palido"),
	EN_PAUSA(-7, true, "En pausa", "La notificaci�n encolada fue puesta en pausa en TRAMITADOR para evitar temporal o definitivamente su emisi�n", "morado"), 
	YA_NO_EXISTE_EN_NOTIFICA(-6, true, "No existe en Notifica", "La notificaci�n existe en TRAMITADOR pero fue borrada en Notifica debido a alguna incidencia interna en Notifica", "gris"),
	ANULADA(-5, true, "Anulada", "La notificaci�n fue anulada en TRAMITADOR antes de ser emitida. Se pasa a este estado cuando el intento de encolamiento resulta en una excepci�n. Es un ESTADO FINAL", "gris"),
	// -4 DISPONIBLE PARA USO FUTURO
	NO_PUESTA_A_DISP_PROBLEMA_TECNICO_NOTIFICA(-3, false, "Fallo III puesta a disposici�n", "Notificaci�n no puesta a disposici�n por problema t�cnico de Notifica", "rojo-palido"),
	NO_PUESTA_A_DISP_USUARIO_NO_ALTA_SERVICIO(-2, false, "Fallo II puesta a disposici�n", "Notificaci�n no puesta a disposici�n porque el usuario est� dado de alta en Notifica pero no en el Servicio", "rojo-palido"),
	NO_PUESTA_A_DISP_USUARIO_NO_ALTA_NOTIFICA(-1, false, "Fallo I puesta a disposici�n", "Notificaci�n no puesta a disposici�n porque el usuario no est� dado de alta en Notifica", "rojo-palido"),
	PUESTA_A_DISPOSICION(0, false, "A disposici�n", "Notificaci�n puesta a disposici�n en Notifica pero no accedida a�n por el usuario", "salmon"),
	// Nuevo estado que aparece en Notifica v4. No se intenta reenv�o por CRON porque se est� reintentando en Notifica
	PENDIENTE_PUESTA_A_DISPOSICION(1, false, "Pendiente de puesta a disposici�n", "Notificaci�n pendiente de puesta a disposici�n en Notifica. Las notificaciones en este estado se reintentar�n poner a disposici�n durante 24 horas", "naranja"),
	// 2, 3, 4 ESTADOS DEPRECADOS DE NOTIFICA. NO UTILIZAR
	LEIDA_POR_USUARIO(5, false, "Entregada", "Notificaci�n accedida y le�da por el usuario, o sea, entregada", "verde-claro"),
	// El estado 6, en Notifica v4, es complementado con el NUEVO estado 8 - Puesta a disposici�n y rechazada por transcurso de plazo
	// Cuando se migre a Notifica v4, basado en REST y que por tanto prescinde de librer�a, habr�a que cambiar 
	// el c�digo del estado interno de TRAMITADOR, EN_COLA, por ejemplo por el c�digo 15, para que no interfiera con el estado 8
	// No es algo cr�tico, sin embargo
	RECHAZADA_POR_USUARIO(6, false, "Rechazada", "Notificaci�n accedida y rechazada por el usuario, o bien dej� pasar los diez d�as desde la puesta a disposici�n (cuando no hay notificaci�n postal)", "rosa"),
	CADUCADA(7, false, "Caducada", "Notificaci�n caducada tras 10 d�as naturales desde puesta a disposici�n de la misma (cuando adem�s hay notificaci�n postal)", "marron"),
	EN_COLA(8, true, "En cola", "La notificaci�n fue puesta en la cola de env�o de TRAMITADOR pero a�n no ha sido emitida hacia Notifica", "amarillo-claro"),
	EMITIDA(9, true, "Emitida", "La notificaci�n ha sido emitida correctamente desde TRAMITADOR hacia Notifica y a�n no se conoce su estado en Notifica", "malva");

	private final int value;
	private final boolean interno;
	private final String nombre;
	private final String descripcion;
	private final String color;

	private EstadoNotificacion_enum(Integer value, boolean interno, String nombre, String descripcion, String color) {
		this.value = value;
		this.interno = interno;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.color = color;
	}

	public int getValue() {
		return value;
	}
	
	public boolean isInterno() {
		return interno;
	}	
	
	public String getNombre() {
		return nombre;
	}	
	
	public String getDescripcion() {
		return descripcion;
	}
	
	public String getColor() {
		return color;
	}		
	
	public static EstadoNotificacion_enum find(Integer value){
		
		if (value==null) return null;
		
	    for(EstadoNotificacion_enum v : values()){
	        if( v.getValue() == value){
	            return v;
	        }
	    }
	    return null;
	}	
	
	public static Map<String, Object[]> getAsMapa () {
		Map<String, Object[]> mapa = new TreeMap<String, Object[]>();
		for (EstadoNotificacion_enum estado : EstadoNotificacion_enum.values()) { 
		    mapa.put(estado.toString(), new Object[] {estado.getValue(), estado.isInterno(), estado.getNombre(), estado.getDescripcion(), estado.getColor()});
		}	
		return mapa;
	}
}
