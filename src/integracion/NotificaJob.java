package src.integracion;



// NOTA: Este Job implementa InterruptableJob por si en el futuro existiera el requerimiento de interrumpirlo
//  por encima de lo que indique el flag de activación/desactivación
@DisallowConcurrentExecution
public class NotificaJob implements InterruptableJob {
	
	private Thread currentThread;
	// logger
	private static final Logger LOG = Logger.getLogger(NotificaJob.class);
	
    public void execute(JobExecutionContext context) throws JobExecutionException {
    	
    	metodoExecute();
    	
    }

	@Override
	public void interrupt() throws UnableToInterruptJobException {
		String respuesta = "";
		
        if (currentThread != null) {
            currentThread.interrupt();
        	respuesta += "*** INTERRUPCION DE JOB DE SINCRONIZACION DE NOTIFICACIONES. FECHA " + new SimpleDateFormat("MMM dd yyyy hh:mma").format(new Date()) + "\r\n";
        	LOG.info(respuesta);            
        }    	
		
	}
	
	public void metodoExecute() {
    	currentThread = Thread.currentThread();
    	
    	String respuesta = "";
    	respuesta += "*** INICIO EJECUCION JOB SINCRONIZACION NOTIFICACIONES. " + " FECHA " + 
    			new SimpleDateFormat("MMM dd yyyy hh:mma").format(new Date()) + "\r\n";
    	
    	// Parametrización
        int minutos = 0, horas = 0;
        
        PropertyResourceBundle bundle = CargadorSingleton.getInstance().getPropertyResourceBundle(); 
        String horaCorteJob = bundle.getString("notifica.cron.horaCorteJob");
        Integer limiteItems = Integer.parseInt(bundle.getString("notifica.cron.limiteItems"));
        Integer pausaEntreItems = Integer.parseInt(bundle.getString("notifica.cron.pausaEntreItems"));
        
        
        
    	try {
    		
    		// Activación / desactivación (variable global del cluster)
    		String notificaCronEstado = PlanificadorDAO.obtenerValor("NOTIFICADOR_ACTIVO").trim().equals("1")?"activo":"inactivo";
	    	
    		if ("INACTIVO".equalsIgnoreCase(notificaCronEstado))
    			interrupt();
    		
			// interrupción manual
			if (!currentThread.isInterrupted()) {
	    	
				// tarea
				NotificaManager notificaManager = new NotificaManager();
				// Si es necesario, pasar limiteItems y pausaEntreItems a gestionaNotificaciones
				respuesta += notificaManager.gestionaNotificaciones();
				
//	    		// pausa entre procesado de items
//	    		if (pausaEntreItems > 0) {
//	    			Thread.sleep(pausaEntreItems);
//	    		}
	    		
		    	respuesta += "*** FIN EJECUCION JOB SINCRONIZACION NOTIFICACIONES " + " FECHA " + 
		    			new SimpleDateFormat("MMM dd yyyy hh:mma").format(new Date()) + "\r\n";

	    	} else {
	    		respuesta += "** Job NOTIFICA en modo desactivado (" + new SimpleDateFormat("MMM dd yyyy hh:mma").format(new Date()) + ").\r\n";	    		
	    	}
	    	LOG.info(respuesta);
    	}catch(Exception e) {
	    	respuesta += "** Error en Job NOTIFICA en la fecha " + new SimpleDateFormat("MMM dd yyyy hh:mma").format(new Date()) + ".\r\n";
	    	if (!Utilidades.esEntornoDesarrollo()) {
		    	LOG.error(respuesta);
		    	LOG.error(e,e);    		
	    	}	
    	}
	}
	
}
