package src.integracion;



/**
 * API para la integraci�n con Notifica
 * @author: 
 * @version: 1.0
 * @see 
 */
public class NotificaManager {
	
	
	
	// logger
	private static final Logger LOG = Logger.getLogger(NotificaManager.class);
	// Constantes
	private int codigoServicioDestinoNotificaciones = 0;
	private String codigoProcedimientoQueSeNotifica = "";
	private String codigoOrganoAlQueSeNotifica = "";
	public static final int NUM_MAX_INTENTOS_ENVIO = 3;
	// API configurada
	private MCSN mcsn;
	// Usuario auditor�a
	Bean dus;
	// Usado en la generaci�n del c�digo de identificaci�n del documento ENI Notificaci�n
	String codigoCincoDigitosHCV;
	
	
	
    /**
     * NO USAR: Invocado s�lo por el CRON
     * @throws NotificaExcepcion
     */	
	public NotificaManager() throws NotificaExcepcion {		
		this(null);
	}	
	
	
	public NotificaManager(Bean dus, Boolean noMCSN) throws NotificaExcepcion {	
		
		try {		
			if (noMCSN)
				this.dus = dus;
			else {
				this.mcsn = configuraMCSN();
				this.dus = dus;
			}
		} catch (Exception e) {
			throw new NotificaExcepcion( "** " + this.getClass() + "#" + "NotificaManager(). El objeto no pudo autoconfigurarse.", e );
		}
	}
    /**
     * Constructor indicando fichero de configuraci�n 
     * @param nombreFicheroConfiguracion El nombre del fichero de configuraci�n de la aplicaci�n, sin la extensi�n .properties
     * @param dus Los datos del usurio que usa los servicios. Necesario para la auditor�a de datos.
     * @throws NotificaExcepcion
     */	
	public NotificaManager(Bean dus) throws NotificaExcepcion {
		try {
			this.mcsn = configuraMCSN();
			this.dus = dus;
		} catch (Exception e) {
			throw new NotificaExcepcion( "** " + this.getClass() + "#" + "NotificaManager(). El objeto no pudo autoconfigurarse.", e );
		}
	}
	
	// devuelve el valor del elemento especificado del acuse de recibo de la notificaci�n indicada
	// puede especificarse el nombre del elemento o bien la subruta hasta el elemento cuando sea necesario
	// NOTA: la subruta debe empezar con '/'
	public static String dameValorAcuseReciboNotificacion (Integer idNotificacion, String elemento) 
		throws SQLException, NamingException, ConversorException, XPathExpressionException, IOException, SAXException,
		ParserConfigurationException {
		String expresion = "";
		if (elemento.contains("/")) {
			String[] elementos = elemento.split(":");
			int i = 0;
			for (String el: elementos) {
				expresion += "//*" + el.substring(0,el.lastIndexOf('/') + 1).trim() + 
				"*[local-name()='" + el.substring(el.lastIndexOf('/') + 1).trim() + "']";
				boolean ultimo = i == elementos.length - 1;
				i ++;
				if (!ultimo) expresion += " | ";
			}
		} else {
			expresion = "//*[local-name()='" + elemento + "']";
		}

		
		NotificacionFichero notificacion = NotificacionDAO.selectNotificacionById(idNotificacion) ;
		
		boolean acuseEsUnENIDOC =  true;
		
		if (notificacion.getEstado().getValue() == EstadoNotificacion_enum.RECHAZADA_POR_USUARIO.getValue() || idNotificacion < 194)
			acuseEsUnENIDOC = false;
			
		
		if (acuseEsUnENIDOC) {
			return es.jda.afirma.client.util.XPathUtils
					.getValuePath(new String((new DocumentoEni(new ByteArrayInputStream(Base64.decode(new String(RetornoHCV.obtenerFicheroPorId 
							(NotificacionDAO.selectNotificacionById(idNotificacion).getIdAcuseRecibo()), StandardCharsets.UTF_8))))).
							getContenido(), StandardCharsets.UTF_8), expresion);		
		} else {
			return es.jda.afirma.client.util.XPathUtils
					.getValuePath(new String(Base64.decode(new String(RetornoHCV.obtenerFicheroPorId 
							(NotificacionDAO.selectNotificacionById(idNotificacion).getIdAcuseRecibo()), 
							StandardCharsets.UTF_8).trim()),StandardCharsets.UTF_8) , expresion);	
		}
	}
	
	// s�lo una de las dos notificaciones posibles, manual o electr�nica, puede ser la activa 
	public void modificaActivacionNotificacionElectronica (Integer id, Integer activa) throws NotificaExcepcion {
		try {
			NotificacionDAO.modificaActivacionNotificacionElectronica(id, activa, dus);
		} catch (Exception e) {
			throw new NotificaExcepcion( "** " + this.getClass() + "#" + "modificaActivacionNotificacionElectronica.", e);	
		}			
	}
	
	/**
	 * Da de alta una notificaci�n manual. Las notificaciones manuales no entran en la gesti�n de la cola de
	 * notificaciones aunque est�n relacionadas, como el resto, con un acto firmable del tr�mite, a trav�s de
	 * la tabla r_NOTIF_PORT_DOC
	 * Las notificaciones manuales se relacionan con la nueva tabla NOTIF_DOCUM que guardan los documentos
	 * asociados a una notificaci�n manual
	 * @param fecha de acuse introducida manualmente, va al campo fh_recibo
	 * @param observaciones, va al campo observaciones
	 * @param activa, va al campo ES_activa (es posible grabar una notificaci�n manual sin activarla, y en el caso 
	 * de que no exista la notificaci�n electr�nica, no habr�a ninguna notificaci�n activa
	 * 
     * @return El Id del registro que se crea en la tabla NOTIFICACION
     * @throws NotificaExcepcion
	 */
	 public Integer altaNotificacionManual
	 	(String numExpediente, String convocatoriaLinea, Integer cdGestion, Date acuse, String observaciones, Boolean activa) 
	 	throws NotificaExcepcion {
			// valida par�metros obligatorios
			if (numExpediente == null || numExpediente.equals("")
					 || convocatoriaLinea == null || convocatoriaLinea.equals("")
					 || cdGestion == null || cdGestion==0) {
				throw new NotificaExcepcion( "** " + this.getClass() + "#" + "altaNotificacionManual. Falta par�metro obligatorio.");
			}
			
			// valida la longitud del c�digo de convocatoria y l�nea (6 caracteres)
			if (convocatoriaLinea.length() < 6) {
				throw new NotificaExcepcion( "** " + this.getClass() + "#" + "altaNotificacionManual. Longitud convocatoriaLinea incorrecta.");
			}
			
			// valida la longitud de las observaciones
			if (observaciones != null && observaciones.length() > 2000) {
				throw new NotificaExcepcion( "** " + this.getClass() + "#" + "altaNotificacionManual. La longitud del observaciones no puede exceder de 2000 caracteres.");	
			}
			// valida la longitud del n�mero de expediente
			if (numExpediente != null && numExpediente.length() > 25) {
				throw new NotificaExcepcion( "** " + this.getClass() + "#" + "altaNotificacionManual. La longitud del n�mero de expediente no puede exceder de 25 caracteres.");
			}
			
			NotificacionFichero notificacionFichero = 
					new NotificacionFichero(numExpediente, cdGestion, acuse, observaciones, convocatoriaLinea, activa);
			try {
				return Integer.valueOf(NotificacionDAO.insertaNotificacionNueva(notificacionFichero, dus).toString());
			} catch (Exception e) {
				throw new NotificaExcepcion( "** " + this.getClass() + "#" + "altaNotificacionManual.", e);	
			}			
	 }
	 
	 

	 public void modificacionNotificacionManual (Integer id, Date acuse, String observaciones, Boolean checkActiva) throws NotificaExcepcion {
			
			// valida la longitud de las observaciones
			if (observaciones != null && observaciones.length() > 2000) {
				throw new NotificaExcepcion( "** " + this.getClass() + "#" + "modificacionNotificacionManual. La longitud del observaciones no puede exceder de 2000 caracteres.");	
			}
			
			try {
				NotificacionDAO.modificaNotificacionManual(id, acuse, observaciones, checkActiva, dus);
			} catch (Exception e) {
				throw new NotificaExcepcion( "** " + this.getClass() + "#" + "modificacionNotificacionManual.", e);	
			}			
	 }	 
	
    /**
     * A�ade a la lista de notificaciones una nueva notificaci�n para su env�o programado (cron)
     * Cuando se eleve una excepci�n NotificaException al menos llamar al m�todo sacaNotificacion (Id devuelto)
     * @param fichero El fichero PDF firmado que se env�a a Notific@ 
     * 	Cuando no se especifique se genera uno seg�n el tipo de documento 
     *  en el que aparece adem�s el n�mero del expediente si se ha informado en el par�metro numExpediente
     * @param tipoDocumento Tipo de documento ENI
     * @param cdGestion C�digo num�rico de la gesti�n a la que corresponde el documento firmado que se notifica (p_ADM_FIRMABLES)
     * @param asunto Texto hasta 255 caracteres (Opcional)
     * @param numExpediente Aparece en el asunto cuando se genera autom�ticamente (Opcional)
     * @param cif El dni/cif del destinatario de la notificaci�n (Opcional)
     * @param dni El dni/cif del destinatario de la notificaci�n
     * @param nif El dni/cif del destinatario de la notificaci�n (Opcional)
     * @param nombre El nombre del destinatario de la notificaci�n
     * @param apellido1 El primer apellido del destinatario de la notificaci�n
     * @param apellido2 El segundo apellido del destinatario de la notificaci�n
     * @param email El correo complementario del destinatario de la notificaci�n (Opcional)
     * @param telefono El telefono complementario del destinatario de la notificaci�n (Opcional)
     * @param convocatoriaLinea Cadena de seis caracteres con el c�digo de Convocatoria y L�nea, por ejemplo CONV01
     * @param *Titular Datos del titular
     * 
     * @return El Id del registro que se crea en la tabla NOTIFICACION
     * @throws NotificaExcepcion
     */	
	public Integer encolaNotificacion 
		(byte[] fichero,  TipoDocumentalDocumento tipoDocumento, Integer cdGestion, String asunto, 
				String numExpediente, String cif, String dni, String nif,
				String nombre, String apellido1, String apellido2, String email, String telefono, String convocatoriaLinea,
				String cifTitular, String dniTitular, String nifTitular,
				String nombreTitular, String apellido1Titular, String apellido2Titular)
		throws NotificaExcepcion {
		
		
		// valida par�metros obligatorios
		if (fichero == null || fichero.length == 0 || tipoDocumento == null || tipoDocumento.equals("")
				 || dni == null || dni.equals("") || nombre == null || nombre.equals("") 
		//		 || apellido1 == null || apellido1.equals("")
				 || convocatoriaLinea == null || convocatoriaLinea.equals("")
				 || email == null || email.equals("")) {
			// discriminaci�n del par�metro faltante
			String parametro = "";
			if (fichero == null || fichero.length == 0)
				parametro = "fichero";
			else if (tipoDocumento == null || tipoDocumento.equals(""))
				parametro = "tipoDocumento";
			else if (dni == null || dni.equals(""))
				parametro = "dni";
			else if (nombre == null || nombre.equals(""))
				parametro = "nombre";
			else if (convocatoriaLinea == null || convocatoriaLinea.equals(""))
				parametro = "convocatoriaLinea";
			else if (email == null || email.equals(""))
				parametro = "email";
			
			throw new NotificaExcepcion( "** " + this.getClass() + "#" + "encolaNotificacion. Falta par�metro obligatorio: " + parametro);
		}
		
		// valida la longitud del c�digo de convocatoria y l�nea (6 caracteres)
		if (convocatoriaLinea.length() < 6) {
			throw new NotificaExcepcion( "** " + this.getClass() + "#" + "encolaNotificacion. Longitud convocatoriaLinea incorrecta.");
		}
		
		// valida la longitud del asunto
		if (asunto != null && asunto.length() > 255) {
			throw new NotificaExcepcion( "** " + this.getClass() + "#" + "encolaNotificacion. La longitud del asunto no puede exceder de 255 caracteres.");	
		}
		// valida la longitud del n�mero de expediente
		if (numExpediente != null && numExpediente.length() > 25) {
			throw new NotificaExcepcion( "** " + this.getClass() + "#" + "encolaNotificacion. La longitud del n�mero de expediente no puede exceder de 25 caracteres.");
		}

		// par�metros OK > se env�a al CRON
		// montar asunto si procede
		if (asunto == null || asunto.length() == 0) {
			asunto = "Notificaci�n de documento de " + tipoDocumento.getDescripcion();
			if (numExpediente != null && numExpediente.length() > 0) {
				asunto += ". Expte: " +  numExpediente;
			}
		}
		
		// guarda par�metro fichero en FICHERO_BF
		Integer idNotificado = guardaFichero(fichero, "notificado.pdf");
		
		NotificacionFichero notificacionFichero = 
				new NotificacionFichero(idNotificado, null, null, numExpediente, cdGestion, 
						asunto, cif, dni, nif, nombre, apellido1, apellido2, email, telefono, EstadoNotificacion_enum.EN_COLA, tipoDocumento.getValue(), 0, convocatoriaLinea,
						cifTitular, dniTitular, nifTitular, nombreTitular, apellido1Titular, apellido2Titular,
						null, false, null, Boolean.TRUE);
		
		try {
			return Integer.valueOf(NotificacionDAO.insertaNotificacionNueva(notificacionFichero, dus).toString());
		} catch (Exception e) {
			throw new NotificaExcepcion( "** " + this.getClass() + "#" + "encolaNotificacion.", e);	
		}
		
	}
	
	
    /**
     * Invalida/anula una notificaci�n en la cola de env�o. Pasa un estado final.
     * Los ficheros asociados son marcados como borrados
     * Se debe usar s�lo como parte del tratamiento de una excepci�n en un bloque try donde exsita un encolado
     * @param id El identificador en NOTIFICACION de la notificaci�n
     * @throws NotificaExcepcion
     */		
	public void anulaNotificacion (Integer id) throws NotificaExcepcion, SQLException, NamingException {
		if (id == null || id.intValue() <= 0) {
			throw new NotificaExcepcion( "** " + this.getClass() + "#" + "anulaNotificacion. El par�metro no es v�lido.");
		}
		NotificacionFichero notificacionFichero = new NotificacionFichero();
		notificacionFichero.setId(id);
		actualizaEstadoNotificacion (notificacionFichero, EstadoNotificacion_enum.ANULADA.getValue());
		// marcar a borrado ficheros asociados
		notificacionFichero = NotificacionDAO.selectNotificacionById (id);
		if (notificacionFichero.getIdFichero() != null)
			NotificacionDAO.marcaFicheroABorrado(notificacionFichero.getIdFichero(), this.dus);
		if (notificacionFichero.getIdAcusePuestaADisposicion() != null)
			NotificacionDAO.marcaFicheroABorrado(notificacionFichero.getIdAcusePuestaADisposicion(), this.dus);
		if (notificacionFichero.getIdAcuseRecibo() != null)
			NotificacionDAO.marcaFicheroABorrado(notificacionFichero.getIdAcuseRecibo(), this.dus);		
	}
	

    /**
     * Pausa una notificaci�n en la cola de env�o.
     * @param id El identificador en NOTIFICACION de la notificaci�n
     * @throws NotificaExcepcion
     */		
	public void pausaNotificacion (Integer id) throws NotificaExcepcion, SQLException, NamingException {
		if (id == null || id.intValue() <= 0) {
			throw new NotificaExcepcion( "** " + this.getClass() + "#" + "pausaNotificacion. El par�metro no es v�lido.");
		}
		NotificacionFichero notificacionFichero = new NotificacionFichero();
		notificacionFichero.setId(id);
		actualizaEstadoNotificacion (notificacionFichero, EstadoNotificacion_enum.EN_PAUSA.getValue());	
	}	
	
	
    /**
     * Reanuda una notificaci�n pausada
     * @param id El identificador en NOTIFICACION de la notificaci�n
     * @throws NotificaExcepcion
     */		
	public void reanudaNotificacion (Integer id) throws NotificaExcepcion, SQLException, NamingException {
		if (id == null || id.intValue() <= 0) {
			throw new NotificaExcepcion( "** " + this.getClass() + "#" + "reanudaNotificacion. El par�metro no es v�lido.");
		}
		NotificacionFichero notificacionFichero = new NotificacionFichero();
		notificacionFichero.setId(id);
		actualizaEstadoNotificacion (notificacionFichero, EstadoNotificacion_enum.EN_COLA.getValue());
	}		
	
	
    /**
     * Env�a orden de baja de servicio de Notifica para un abonado
     * @param abonado
     * @throws NotificaExcepcion
     */	
	public void bajaAbonadoNotifica (Abonado abonado) throws NotificaExcepcion {
		
		try {
			this.mcsn.solicitarBajaAbonado(abonado, codigoServicioDestinoNotificaciones);
		} catch (Exception e) {
			throw new NotificaExcepcion( "** " + this.getClass() + "#" + "bajaAbonadoNotifica.", e);
		}
	}
	

	
    /**
     * Env�a orden de alta de un abonado en un servicio de Notifica
     * @param abonado
     * @throws NotificaExcepcion
     */
	public void altaAbonadoNotifica (Abonado abonado) throws NotificaExcepcion {	
		
		try {
			// Comprobar si el DNI del abonado est� dado de alta en Notifica y en el servicio
			int[] estadoDestinatario = this.mcsn.solicitarEstadoAbonadosServicio(new String[] {abonado.getIdAbonado()}, codigoServicioDestinoNotificaciones);
			
			// Si no lo est� lo doy de alta
			if (estadoDestinatario[0] != 1) {
				this.mcsn.solicitarAltaAbonado(abonado, new FirmaInf(), codigoServicioDestinoNotificaciones);
			} else {
				throw new NotificaExcepcion( "** " + this.getClass() + "#" + "altaAbonadoNotifica. El Abonado ya est� dado de alta en el servicio.");
			}
		} catch (Exception e) {
			throw new NotificaExcepcion( "** " + this.getClass() + "#" + "altaAbonadoNotifica.", e);
		}
	}
	
	
	
    /**
     * NO USAR: Invocado s�lo por el CRON
     */	
	public String gestionaNotificaciones() {
		String informe = "*** Informe de CRON de Notificaciones:\n";
		try {
			
			int [] resultadoSincronizaNotificacionesEnviadas = sincronizaNotificacionesEnviadas();
			int [] resultadoEnviaNotificacionesNuevas = enviaNotificacionesNuevas();
			int [] resultadoReenviaNotificacionesFallidas = reenviaNotificacionesFallidas();
			int [] resultadoArchivaNotificacionesDesistidas = archivaNotificacionesDesistidas();
			
			informe+= "** Sincronizadas: OK: " + resultadoSincronizaNotificacionesEnviadas[0] 
					+ ". NO OK: " + resultadoSincronizaNotificacionesEnviadas[1] + ".\n";
			informe+= "** Enviadas: OK: " + resultadoEnviaNotificacionesNuevas[0] 
					+ ". NO OK: " + resultadoEnviaNotificacionesNuevas[1] + ".\n";
			informe+= "** Reenviadas: OK: " + resultadoReenviaNotificacionesFallidas[0] 
					+ ". NO OK: " + resultadoReenviaNotificacionesFallidas[1] + ".\n";
			informe+= "** Desistidas: OK: " + resultadoArchivaNotificacionesDesistidas[0] 
					+ ". NO OK: " + resultadoArchivaNotificacionesDesistidas[1] + ".\n";
		
		} catch (Exception e) {
			LOG.error( "*** " + this.getClass() + "#" + "gestionaNotificaciones. Hubo un problema al intentar gestionar las notificaciones.", e);
		}
		return informe;
	}
	
	
	
	private void enviaNotificacion (NotificacionFichero notificacionFichero) throws SQLException, NamingException, NotificaExcepcion {
		
		int estadoFinal = 0;
		
		try {
		// incrementa el contador de intentos
		incrementaNumeroIntentosEnvioNotificacion(notificacionFichero);
		
		// Leer el contenido binario del fichero a notificar
		byte[] documentoRaw = recuperaFichero (notificacionFichero.getIdFichero());
		
		// Nota: El campo nif de la clase Abonado est� deprecado. Por tanto no usarlo.
		
		Titular titular = null;
		Abonado destinatario = null;
		
		boolean existeTitular = !nvl(notificacionFichero.getDniTitular()).equals("");
		
		// en ppio hacemos que siempre exista titular, que ser� el interesado
		if (existeTitular) {
			titular = new Titular(nvl(notificacionFichero.getCifTitular()) + nvl(notificacionFichero.getDniTitular()) + nvl(notificacionFichero.getNifTitular()));
			titular.setNombre(notificacionFichero.getNombreTitular());
			titular.setApellidos(nvl(notificacionFichero.getApellido1Titular()) + " " + nvl(notificacionFichero.getApellido2Titular()));						 
		}
		

		destinatario = new Abonado(nvl(notificacionFichero.getCif()) + nvl(notificacionFichero.getDni()) + nvl(notificacionFichero.getNif()));
		destinatario.setNombre(notificacionFichero.getNombre());
		destinatario.setApellidos(nvl(notificacionFichero.getApellido1()) + " " + nvl(notificacionFichero.getApellido2()));
		// se avisar� con estos datos complementarios adem�s de con los datos que obren en Notifica si el usuario ya estaba o estuvo dado de alta
		destinatario.setEmail(notificacionFichero.getEmail());	// direcci�n de correo complementaria a la existente en Notifica
		destinatario.setTelefonoMovil(notificacionFichero.getTelefono());	// n�mero de m�vil complementario al existente en Notifica si existe alguno					


		// Comprobar que el DNI del destinatario est� dado de alta en Notifica y en el servicio
		int[] estadoDestinatario = this.mcsn.solicitarEstadoAbonadosServicio(new String[] {destinatario.getIdAbonado()}, codigoServicioDestinoNotificaciones);
		
		// Si no lo est� lo doy de alta
		if (estadoDestinatario[0] != 1) {
			this.mcsn.solicitarAltaAbonado(destinatario, new FirmaInf(), codigoServicioDestinoNotificaciones);

		}
		
		// se quita el else, para que los datos los tome de Notifica y tome el email y el n�mero de m�vil complementario de la solicitud de ayuda
		
		// montar la notificaci�n y enviar
		NotificacionDocMetadatos notificacionDocMetadatos = new NotificacionDocMetadatos(false);
		notificacionDocMetadatos.setAsunto(notificacionFichero.getAsunto());
		notificacionDocMetadatos.setEnidocVersionNti( "http://administracionelectronica.gob.es/ENI/XSD/v1.0/documento-e" );
		notificacionDocMetadatos.setCodigoRpaSia(codigoProcedimientoQueSeNotifica);
		notificacionDocMetadatos.setCodigoExpediente(notificacionFichero.getNumExpediente());
		notificacionDocMetadatos.setDocNotificacion(documentoRaw); 
		// AAAAMMDD T HH:MM:SS <ISO 8601>
		notificacionDocMetadatos.setEnidocFechaCaptura(new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(new Date()));		
		// Metadato Identificador UNICO en la Administraci�n
		// ES_<�rgano>_<AAAA>_<ID_espec�fico>: <ID_espec�fico> son 30 d�gitos. Los 5 primeros son el c�digo HCV de la aplicaci�n (rRADK), el resto un secuencial
		notificacionDocMetadatos.setEnidocIdentificador( "ES_" + codigoOrganoAlQueSeNotifica + "_" + new SimpleDateFormat("yyyy").format(new Date()) + "_" + this.codigoCincoDigitosHCV + StringUtils.leftPad(Integer.toString(notificacionFichero.getId()), 25, "0") );
		//Metadato CODIGO DE UNIDAD ORGANICA: Consejer�a de Agricultura, Pesca y Desarrollo Rural
		notificacionDocMetadatos.setEnidocOrgano( codigoOrganoAlQueSeNotifica );
		//Metadato ORIGEN CIUDADANIA
		notificacionDocMetadatos.setEniDocOrigenCiuAdmin( "1" ); 
		//Metadato Tipo: RESOLUCION
		notificacionDocMetadatos.setEniDocTipoDocumental( notificacionFichero.getTipoDocEni() );
		//Metadato Estado: ORIGINAL
		notificacionDocMetadatos.setEniDocValorEstElab( "EE01" );

		notificacionDocMetadatos.addDestinatarioV3(destinatario, null, true, false, titular);
		
		RemesaDocMetadatos remesa = new RemesaDocMetadatos(codigoServicioDestinoNotificaciones, codigoOrganoAlQueSeNotifica);
		remesa.addNotificacion(notificacionDocMetadatos);
		// Env�o de la remesa al Sistema de Notificaciones		
		Integer numRemesa = mcsn.enviarRemesaEni(remesa);
		
		// guarda el numero de remesa devuelto por Notifica
		informaNumeroRemesaNotificacion (notificacionFichero, numRemesa);
		// estado EMITIDA
		estadoFinal = EstadoNotificacion_enum.EMITIDA.getValue();
		// actualiza fecha de emisi�n
		informaFechaEmisionNotificacion(notificacionFichero);
		
		} catch (Exception e) {
			estadoFinal = EstadoNotificacion_enum.NO_EMITIDA_PROBLEMA_TECNICO.getValue();
			throw new NotificaExcepcion(EstadoNotificacion_enum.NO_EMITIDA_PROBLEMA_TECNICO.getDescripcion());
		} finally {
			// actualiza estado final
			actualizaEstadoNotificacion (notificacionFichero, estadoFinal);
		}
	}	
	
	
	
	private int[] enviaNotificacionesNuevas () throws SQLException, NamingException {

		ArrayList<NotificacionFichero> listaEnvios = NotificacionDAO.consultarNotificacionesNuevas();
		Iterator<NotificacionFichero> it = listaEnvios.iterator();
		int numEnvios = 0;
		int numEnviosError = 0;
		while(it.hasNext()) {
			NotificacionFichero aEnvio = it.next();
			try {
				enviaNotificacion(aEnvio);
				numEnvios ++;
			} catch (Exception e) {
				LOG.error( "** " + this.getClass() + "#" + "enviaNotificacionesNuevas. Hubo un problema al intentar enviar la notificaci�n id: " + aEnvio.getId() + ".", e);
				numEnviosError++;
			}
			
		}
		return new int[] {numEnvios, numEnviosError};
	}
	
	
	
	private int[] reenviaNotificacionesFallidas () throws SQLException, NamingException {
		ArrayList<NotificacionFichero> lista = NotificacionDAO.consultarNotificacionesFallidasParaReenvio();
		Iterator<NotificacionFichero> it = lista.iterator();
		int items = 0;
		int itemsError = 0;
		while(it.hasNext()) {
			NotificacionFichero notificacionFichero = it.next();
			try {
				enviaNotificacion(notificacionFichero);
				items ++;
			} catch (Exception e) {
				LOG.error( "** " + this.getClass() + "#" + "reenviaNotificacionesFallidas. Hubo un problema al intentar reenviar la notificaci�n id: " + notificacionFichero.getId() + ".", e);
				itemsError ++;
			}
		}
		return new int[] {items, itemsError};
	}
	
	
	// tras n intentos de env�o fallido desde TRAMITADOR la notificaci�n pasa a estado archivado
	private int[] archivaNotificacionesDesistidas () throws SQLException, NamingException {
		ArrayList<NotificacionFichero> lista = NotificacionDAO.consultarNotificacionesParaArchivado();
		Iterator<NotificacionFichero> it = lista.iterator();
		int items = 0;
		int itemsError = 0;
		while(it.hasNext()) {
			NotificacionFichero notificacionFichero = it.next();
			try {
				actualizaEstadoNotificacion (notificacionFichero, EstadoNotificacion_enum.DESISTIDA.getValue());
				items ++;
			} catch (Exception e) {
				LOG.error( "** " + this.getClass() + "#" + "archivaNotificacionesDesistidas. Hubo un problema al intentar archivar la notificaci�n id: " + notificacionFichero.getId() + ".", e);
				itemsError ++;
			}			
		}
		return new int[] {items, itemsError};		
	}
	
	
	
	// notificaciones enviadas con exito y en estados no finales: EMITIDA y PUESTA_A_DISPOSICION
	//  excluye estados finales: LEIDA_POR_USUARIO, RECHAZADA_POR_USUARIO y CADUCADA
	private int[] sincronizaNotificacionesEnviadas () throws SQLException, NamingException {
		ArrayList<NotificacionFichero> lista = NotificacionDAO.consultarNotificacionesSincronizables();
		Iterator<NotificacionFichero> it = lista.iterator();
		int items = 0;
		int itemsError = 0;
		while(it.hasNext()) {
			NotificacionFichero notificacionFichero = it.next();
			try {
				sincronizaNotificacion(notificacionFichero);
				actualizaEstados(notificacionFichero.getNumExpediente(), notificacionFichero.getCdGestion());
				items ++;
			} catch (Exception e) {
				LOG.error( "** " + this.getClass() + "#" + "sincronizaNotificacionesEnviadas. Hubo un problema al intentar sincronizar la notificaci�n id: " + notificacionFichero.getId() + ".", e);
				itemsError ++;
			}
		}
		return new int[] {items, itemsError}; 
	}
	
	
	public void actualizaEstados(String numExpediente, Integer gestion) {
		
		String estadoFinalExpediente =  CtesGenerales.ESTADO_RECURSO_PRESENTADO; ;
		String estadoFinalRecurso = RecEstadoRecurso_enum.RESUELTO.getValue();
		try {
			
			Integer idSOLIC_AYUDA = NotificacionDAO.selectIdSOLIC_AYUDAPorNumExpediente(numExpediente);
			
			switch(gestion) {
			  case 15:	// resolucion de recurso
				// cambio de estado del expediente (PR a RR o viceversa)
				// y cambio de estado de la resoluci�n de recurso (RESUELTO a FINALIZADO o viceversa)
				  
				Boolean esDesestimado = RecursoResolucionDAO.codigoResultadoResolucion(idSOLIC_AYUDA).equals(CtesGenerales.CD_ESTADO_ESTUDIO_RECURSO_DESESTIMADO) ;
				Boolean	esInadmitido = RecursoResolucionDAO.codigoResultadoResolucion(idSOLIC_AYUDA).equals(CtesGenerales.CD_ESTADO_ESTUDIO_RECURSO_INADMITIDO) ;
				  
				if (RecursoResolucionDAO.existeFechaAcuseActivaNotificacionExpediente(idSOLIC_AYUDA) && (esDesestimado || esInadmitido)) {
					// a estado RR y a estado FINALIZADO
					estadoFinalExpediente = CtesGenerales.ESTADO_RECURSO_RESUELTO;
					estadoFinalRecurso = RecEstadoRecurso_enum.NOTIFICADO.getValue();
				} else {
					//  aestado PR y a estado RESUELTO
					estadoFinalExpediente = CtesGenerales.ESTADO_RECURSO_PRESENTADO;
				}
				

			  case 39:	// modificacion de resolucion
				// cambio de estado del expediente (PR a RR o viceversa)
				// y cambio de estado de la resoluci�n de recurso (RESUELTO a FINALIZADO o viceversa)
				if (RecursoResolucionDAO.existeFechaAcuseActivaNotificacionExpedienteModifica(idSOLIC_AYUDA)) {
					// a estado RR y a estado FINALIZADO
					estadoFinalExpediente = CtesGenerales.ESTADO_RECURSO_RESUELTO;
					estadoFinalRecurso = RecEstadoRecurso_enum.NOTIFICADO_MODIFICATORIA.getValue();
				} else {
					//  aestado PR y a estado RESUELTO
					estadoFinalExpediente = CtesGenerales.ESTADO_RECURSO_PRESENTADO;
				}
				
			}
			
			NotificacionDAO.actualizaEstadoSolicitudAyuda(Integer.toString(idSOLIC_AYUDA), estadoFinalExpediente, this.dus);
			RecursoDAO.actualizaEstadoRecursoNoArchivadoByIdSOLIC_AYUDACdEstadoRecurso(idSOLIC_AYUDA, estadoFinalRecurso );
			// el cambio de estado del recurso asociado no se realiza por no ser relevante su paso a finalizado
			
		} catch (Exception e) {
			LOG.error(e, e);
		}		
	}
	
	
	public void sincronizaNotificacion(NotificacionFichero notificacionFichero) 
			throws MCSNException, SQLException, FileNotFoundException, IOException, NamingException, NotificaExcepcion, Exception {

		// si no hay c�digo de remesa entonces la notificaci�n no ha llegado a Notifica y no cabe la sincronizaci�n
		if (notificacionFichero.getCodigoRemesa()==0) return;
		
		// se opta por un modelo de una notificaci�n por remesa
		int[] remesa = new int[] {notificacionFichero.getCodigoRemesa()};

		// Se custodian los acuses
		RemesaInf[] remesaInf = this.mcsn.obtenerInfRemesas(remesa, true);
		
		// la notificaci�n est� bloqueada en TRAMITADOR y no lleg� a Notifica
		if (remesaInf==null && notificacionFichero.getEstado().isInterno() && 
				notificacionFichero.getEstado().getValue()<0) {			
			throw new NotificaExcepcion( "La sincronizaci�n no es posible. La remesa " + notificacionFichero.getCodigoRemesa() + " no lleg� a Notifica.");
		}
		
		// Devuelve dentro un array vac�o de NotificacionInf cuando la notificaci�n ha dejado de existir en Notifica, 
		//  por ejemplo si el usuario se ha dado de baja en el servicio
		if (remesaInf[0].getNotificaciones().length == 0
				&& notificacionFichero.getEstado() == EstadoNotificacion_enum.EMITIDA) {
			// cambiar estado, registrar en el log y lanzar NotificaExcepcion
			actualizaEstadoNotificacion (notificacionFichero, EstadoNotificacion_enum.YA_NO_EXISTE_EN_NOTIFICA.getValue());
			throw new NotificaExcepcion( "La remesa " + notificacionFichero.getCodigoRemesa() + " ya no existe en Notifica.");
		}
		// consulta estado actual en Notifica
		int estadoNotificacion = 
				((NotificacionInf)((RemesaInf)remesaInf[0]).getNotificaciones()[0]).getEstado();
		
		
			actualizaEstadoNotificacion (notificacionFichero, estadoNotificacion);
			
			boolean puestaADisposicion = estadoNotificacion == EstadoNotificacion_enum.PUESTA_A_DISPOSICION.getValue();
			boolean leida = estadoNotificacion == EstadoNotificacion_enum.LEIDA_POR_USUARIO.getValue();
			boolean rechazada = estadoNotificacion == EstadoNotificacion_enum.RECHAZADA_POR_USUARIO.getValue();
			boolean sinFechaPuestaADisposicion = notificacionFichero.getFechaPuestaADisposicion() == null;
			boolean sinFechaRecibo = notificacionFichero.getFechaRecibo() == null;
			
			// actualiza fechas de env�o y de entrega / rechazo cuando proceda
			if ((puestaADisposicion || leida || rechazada) && sinFechaPuestaADisposicion) {
				informaFechaEnvioNotificacion (notificacionFichero, 
						((NotificacionInf)((RemesaInf)remesaInf[0]).getNotificaciones()[0]).getFechaYHoraPuestaDisposicion());
				guardaAcuseEnvio(notificacionFichero, 
						((NotificacionInf)((RemesaInf)remesaInf[0]).getNotificaciones()[0]).getAcusePuestaDisposicion());
				} 
			
			if (sinFechaRecibo) {
				if (leida) {
					informaFechaEntregaNotificacion (notificacionFichero, 
							((NotificacionInf)((RemesaInf)remesaInf[0]).getNotificaciones()[0]).getFechaYHoraRecibo());
					guardaAcuseRecibo(notificacionFichero,
							((NotificacionInf)((RemesaInf)remesaInf[0]).getNotificaciones()[0]).getAcuseRecibo());				
				} else if (rechazada) {
					informaFechaEntregaNotificacion (notificacionFichero, 
							((NotificacionInf)((RemesaInf)remesaInf[0]).getNotificaciones()[0]).getFechaYHoraRechazo());
					guardaAcuseRecibo(notificacionFichero,
							((NotificacionInf)((RemesaInf)remesaInf[0]).getNotificaciones()[0]).getFirmaRechazo());					
				}
			}

	}
	
	
	

	
	
	private void guardaAcuseEnvio(NotificacionFichero notificacionFichero, byte[] acuseEnvio) throws SQLException, NamingException {
		Integer idAcuseEnvio = guardaFichero(acuseEnvio, "envio.pdf");
		NotificacionDAO.informaAcuseEnvioNotificacion(notificacionFichero.getId(), idAcuseEnvio, dameDatosUsuarioCron(notificacionFichero));
	}
	
	
	
	private void guardaAcuseRecibo(NotificacionFichero notificacionFichero, byte[] acuseRecibo) throws SQLException, NamingException {
		Integer idAcuseRecibo = guardaFichero(acuseRecibo, "recibo.pdf");
		NotificacionDAO.informaAcuseEntregaNotificacion(notificacionFichero.getId(), idAcuseRecibo,dameDatosUsuarioCron(notificacionFichero));
	}	
	

	private void informaFechaEmisionNotificacion (NotificacionFichero notificacionFichero) throws SQLException, NamingException {
		NotificacionDAO.informaFechaEmisionNotificacion(notificacionFichero.getId(), dameDatosUsuarioCron(notificacionFichero));
	}	
	
	private void informaFechaEnvioNotificacion (NotificacionFichero notificacionFichero, Date fecha) throws SQLException, NamingException {
		NotificacionDAO.informaFechaEnvioNotificacion(notificacionFichero.getId(), fecha, dameDatosUsuarioCron(notificacionFichero));
	}
	
	
	
	private void informaFechaEntregaNotificacion (NotificacionFichero notificacionFichero, Date fecha) throws SQLException, NamingException {
		NotificacionDAO.informaFechaEntregaNotificacion(notificacionFichero.getId(), fecha, dameDatosUsuarioCron(notificacionFichero));
	}
	
	
	
	private void actualizaEstadoNotificacion (NotificacionFichero notificacionFichero, int estado) throws SQLException, NamingException {
		NotificacionDAO.actualizarEstadoNotificacion(notificacionFichero.getId(), estado, dameDatosUsuarioCron(notificacionFichero));
	}
	
	
	
	private void informaNumeroRemesaNotificacion (NotificacionFichero notificacionFichero, int numRemesa) throws SQLException, NamingException {
		NotificacionDAO.informaNumeroRemesaNotificacion(notificacionFichero.getId(), numRemesa, dameDatosUsuarioCron(notificacionFichero));
	}
	
	
	private void incrementaNumeroIntentosEnvioNotificacion(NotificacionFichero notificacionFichero)
			 throws SQLException, NamingException{
		int nActual = notificacionFichero.getNumIntentos().intValue();
		NotificacionDAO.incrementarNumeroIntentosEnvioNotificacion(notificacionFichero.getId(), ++ nActual, dameDatosUsuarioCron(notificacionFichero));
	}	
	
	
	// autoconfiguraci�n del notificador
	private MCSN configuraMCSN() throws Exception {
		
		// configuraci�n e instanciaci�n de fachada API
		Properties propiedades = new Properties();
		PropertyResourceBundle bundle = CargadorSingleton.getInstance().getPropertyResourceBundle();
        
        propiedades.put("protocolo", bundle.getString("notifica.protocolo"));
        propiedades.put("direccion_ip", bundle.getString("notifica.direccion_ip"));
        propiedades.put("puerto", bundle.getString("notifica.puerto"));
        propiedades.put("path_acceso", bundle.getString("notifica.path_acceso"));
        propiedades.put("conexionproxy", bundle.getString("notifica.conexionproxy"));
        propiedades.put("pkcs12.archivo", bundle.getString("notifica.pkcs12.archivo"));
        propiedades.put("pkcs12.pass", bundle.getString("notifica.pkcs12.pass"));
        
    	this.codigoServicioDestinoNotificaciones = Integer.parseInt(bundle.getString("notifica.servicio"));
    	this.codigoProcedimientoQueSeNotifica = bundle.getString("notifica.procedimiento");
    	this.codigoOrganoAlQueSeNotifica = bundle.getString("notifica.organo");       
        
        propiedades.put("proxyhost", bundle.getString("notifica.proxyhost"));
        propiedades.put("proxyport", bundle.getString("notifica.proxyport"));
        propiedades.put("proxylogin", bundle.getString("notifica.proxylogin"));
        propiedades.put("proxypassword", bundle.getString("notifica.proxypassword"));
        
        this.codigoCincoDigitosHCV = bundle.getString("hcv.aplicacion");
            
		Configuration config = new Configuration().loadProperties(propiedades);
		
		return new MCSN( config );
	}
	
	
	// el fichero notificado y los acuses de recibo, firma de rechazo y acuse de puesta a disposici�n se guardan como ficheros
	private Integer guardaFichero(byte[] datos, String pNombreFichero) {

		InputStream inst = new ByteArrayInputStream(datos); 
		DataBase dbft = new DataBase();
		Integer idFichero = null;

		try {

			dbft.connect(this.dus);
			
			idFichero = Integer.parseInt(Long.toString(dbft.uploadInputStream(inst, pNombreFichero, "application/pdf", Long.valueOf(datos.length))));


			dbft.commit();
			return idFichero;


		} catch (IOException e) {
			try {
				dbft.rollback();
				LOG.error(e, e);
			} catch (SQLException e1) {
				LOG.error(e1, e1);
			}
		} catch (SQLException e) {
			try {
				dbft.rollback();
				LOG.error(e, e);
			} catch (SQLException e1) {
				LOG.error(e1, e1);
			}			
		} catch (Exception e) {
			try {
				dbft.rollback();
				LOG.error(e, e);
			} catch (SQLException e1) {
				LOG.error(e1, e1);
			}						
		} finally {
			try {
				dbft.disconnect();
				dbft.getCon().close();
			} catch (Exception e) {
				LOG.error(e, e);
			}
		}
		return idFichero;
	}	
	
	
	private byte[] recuperaFichero(Integer idFichero) throws  Exception {
		DataBase dbft = new DataBase();		
		byte[] byteFichero = null;
		
		try{
			
			if (idFichero != null){
				dbft.connect();
				byteFichero = dbft.downloadFileByte(idFichero);				
			}

		}finally{
			if (dbft.getCon() != null){
				dbft.disconnect();
				dbft.getCon().close();
			}			
		}

		return byteFichero;
	}		
	
	
	
	public String nvl(String valor) {
		if (valor == null) return ""; else return valor; 
	}


	
	private Bean dameDatosUsuarioCron (NotificacionFichero notificacionFichero) {
		
		int idUsuarioAuditoria = 1531;  
		try {
			idUsuarioAuditoria = Integer.parseInt(notificacionFichero.getUsuarioAuditoria());
		} catch (NumberFormatException e) {
			// Aqu� s�lo se debe entrar en el entorno de Desarrollo
			LOG.info("** Se toma el usuario de auditoria por defecto id = 1531. El usuario " + notificacionFichero.getUsuarioAuditoria() + " no es num�rico.");
		}
		
		return new Bean(idUsuarioAuditoria, notificacionFichero.getConvocatoriaLinea());		
	}
	
	private void poneACeroNumeroDeIntentosDeEnvio(NotificacionFichero notificacionFichero) throws SQLException, NamingException {
		NotificacionDAO.ponerACeroNumeroDeIntentosDeEnvio(notificacionFichero.getId(), dameDatosUsuarioCron(notificacionFichero));
	}
		
    /**
     * Reencolado manual desde la pantalla de gesti�n de notificaciones
     * @param notificacionFichero La notificaci�n que se quiere reencolar
     */			
	public int reencolaNotificacion (NotificacionFichero notificacionFichero) {
		try {
			actualizaEstadoNotificacion(notificacionFichero, EstadoNotificacion_enum.EN_COLA.getValue());
			poneACeroNumeroDeIntentosDeEnvio(notificacionFichero);
			return 0;
		} catch (Exception e) {
			LOG.error( "** " + this.getClass() + "#" + "reencolaNotificacion. Hubo un problema en el reencolado manual la notificaci�n id: " + notificacionFichero.getId() + ".", e);
			return 1;
		}
	}	
	
    /**
     * Adelanto manual de env�o desde la pantalla de gesti�n de notificaciones
     * @param notificacionFichero La notificaci�n que se quiere enviar
     */			
	public void enviaAnticipadoNotificacion (NotificacionFichero notificacionFichero) throws NotificaExcepcion, NamingException, SQLException {
			enviaNotificacion(notificacionFichero);
	}
	
    /**
     * Pausado manual de notificaci�n encolada desde la pantalla de gesti�n de notificaciones
     * @param notificacionFichero La notificaci�n a la que se quiere demorar (temporal o definitivamente) su envio
     */			
	public int pausaNotificacion (NotificacionFichero notificacionFichero) {
		try {
			pausaNotificacion(notificacionFichero.getId());
			return 0;
		} catch (Exception e) {
			LOG.error( "** " + this.getClass() + "#" + "pausaNotificacion. Hubo un problema en el pausado manual la notificaci�n id: " + notificacionFichero.getId() + ".", e);
			return 1;
		}
	}

	
    /**
     * Reanudado manual de notificaci�n pausada desde la pantalla de gesti�n de notificaciones
     * @param notificacionFichero La notificaci�n pausada a reinsertar en la cola de env�o
     */			
	public int reanudaNotificacion (NotificacionFichero notificacionFichero) {
		try {
			reanudaNotificacion(notificacionFichero.getId());
			return 0;
		} catch (Exception e) {
			LOG.error( "** " + this.getClass() + "#" + "pausadoNotificacion. Hubo un problema en el pausado manual la notificaci�n id: " + notificacionFichero.getId() + ".", e);
			return 1;
		}
	}


	public MCSN getMcsn() {
		return mcsn;
	}


	public void setMcsn(MCSN mcsn) {
		this.mcsn = mcsn;
	}
	
	
	
}
