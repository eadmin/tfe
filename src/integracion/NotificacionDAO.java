package src.integracion;



public class NotificacionDAO {
	
	private static final Logger LOG = Logger.getLogger(NotificacionDAO.class);
	
	private static String PROPERTY_JNDI = "ORGIENDATOS";
	private static PropertyResourceBundle m_connProperties = CargadorSingleton.getInstance().getPropertyResourceBundle();
	private static String m_nombreJNDI = (m_connProperties != null) ? m_connProperties.getString(PROPERTY_JNDI) : "";	
	
	// inserta una nueva notificaci�n
	private static String insertaNotificacionNueva =	
	" INSERT" +
	"  INTO NOTIFICACION" +
	"  " +
	"    )" +
	"    VALUES" +
	"    (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	
	
	private static String selectIdSOLIC_AYUDAPorNumExpediente =
			"select id_SOLIC_AYUDA from SOLIC_AYUDA where num_expte = ?";
	
	private static String notificacionActivaPorExpte =
			"  ";		
	
	private static String selectTodosLosCampos =
			" ";			
	
	
	private static String consultarNotificacionesNuevas = 
			selectTodosLosCampos +
			" ";
	
	private static String consultarNotificacionesFallidasParaReenvio =
			selectTodosLosCampos +
			" ";

	
	private static String consultarNotificacionesParaArchivado =
			selectTodosLosCampos +
			" ";
	
	
	private static String obtenerColaNotificaciones = 
			selectTodosLosCampos +
			" ";
					
	private static String consultarNotificacionesSincronizables = 
			selectTodosLosCampos +
			" ";
	
	private static String modificaNotificacionManual = 
			" ";
	
	private static String modificaActivacionNotificacionElectronica =
			" ";			
	
	private static String informaNumeroRemesaNotificacion =
			" ";
	
	private static String incrementarNumeroIntentosEnvioNotificacion =
			" ";	
	
	private static String actualizaEstadoNotificacion =
			" ";		

	private static String informaFechaEmisionNotificacion =
			" ";	
	
	private static String informaFechaEntregaNotificacion =
			" ";		
	
	private static String informaFechaEnvioNotificacion =
			" ";	
	
	private static String informaAcuseEntregaNotificacion =
			" ";		
	
	private static String informaAcuseEnvioNotificacion =
			" ";		
	
	private static String deleteNotificacionById =
			" ";	
	
	private static String selectNotificacionById =
			selectTodosLosCampos + " ";
	
	private static String marcaFicheroABorrado = "  "
			+ " ";	
	
	private static String poneACeroNumeroDeIntentosDeEnvio =
			" ";	
			
			
	private static String listadoGlobalNotificaciones =
			" ";
	
	private static String listadoGlobalNotificacionesTotal =
			"select count(*) from notificacion where {filtro} AND {busqueda}" +
			"";				

	
	// inserta documento asociado a notificacion manual
	private static String insertDocumento = " " ;
	
	// actualiza estado solicitu de ayuda
	private static String actualizaEstadoSolicitudAyuda = " ";

	
	public static void actualizaEstadoSolicitudAyuda(String pIdSolicitud, String pEstado, Bean dus) throws SQLException, NamingException {

		DataBase db = new DataBase();

		try {
			db.connect(dus);
			db.update(actualizaEstadoSolicitudAyuda, new Object[] { DataBase.parseVarchar2(pEstado), pIdSolicitud });
			db.commit();
			
		} finally {
			db.disconnect();
			db.getCon().close();
		}
	}
	
	/**
	 * M�todo para insertar un documento subido como archivo
	 */
	public static String insertaDocumento(Integer pIdNotificacion, String pIdDocumento) 
	{

		DataBase db=new DataBase();
		String idDocumento = "";
		Object[] array_IdNotificacionDocum = new Object[]{new Integer(-1)};

		
		try {
			// si hacemos una select    	
			//db.connect();
			// si hacemos un insert o update
			Bean dus = (Bean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("Bean");
			db.connect(dus);

			java.util.Date fechaAlta = new java.util.Date();

			db.update(insertDocumento, new Object[]{pIdNotificacion,
					DataBase.parseVarchar2(pIdDocumento),
					DataBase.parseDate(fechaAlta)},
					array_IdNotificacionDocum);

			idDocumento = array_IdNotificacionDocum[0].toString();

			db.commit();

		} catch (SQLException e) {    
			LOG.error(e,e);       	
			return "error";
		} catch (NamingException e) {    	   
			LOG.error(e,e);
			return "error";
		} finally {
			try {
				db.disconnect();
				db.getCon().close();
			} catch (Exception e) {
				LOG.error(e,e);
				return "error";
			}
		}

		return idDocumento;

	}  
	
	private static String filtrosWhere (Map<String, Object> filters) {
		String where = "";
		Set<String> filtros = filters.keySet();
		Iterator<String> itFiltros = filtros.iterator();
		while (itFiltros.hasNext()) {
			String filtro = itFiltros.next();
			String valorFiltro = (String)filters.get(filtro);
			if (where.length() > 0) where += " and ";
			
			// caso filtro campo uni�n de varios campos: nombre completo destinatario de la notificaci�n
			if (NotificacionFichero.dameCampo(filtro).contains(",")) {
				// campo m�ltiple
				String[] campos = NotificacionFichero.dameCampo(filtro).split(", ");
				where += "(";
				for (int i=0; i < campos.length; i++) {
					where += campos[i];
					where += " like '%" + valorFiltro + "%'";
					if (i!=campos.length-1) {
						where += " or ";
					} else {
						where += ")";
					}
				}
			} else {
			
			where += NotificacionFichero.dameCampo(filtro);
			if (filtro.equals("estado")) 
				where += " = " + valorFiltro;
			else 
				where += " like '%" + valorFiltro + "%'";
			}
		}
		if (where.length()==0) return "1=1";
		return where;
	}
	
	private static String filtroOrder (String sortField, SortOrder sortOrder) {
		// orden por defecto es modificadas m�s recientemente primero
		if (sortField == null) return " FH_AUDIT DESC";
		
		// caso filtro campo uni�n de varios campos: nombre completo destinatario de la notificaci�n
		String orden = "";
		if (NotificacionFichero.dameCampo(sortField).contains(",")) {
			// campo m�ltiple
			String[] campos = NotificacionFichero.dameCampo(sortField).split(", ");
			for (int i=0; i < campos.length; i++) {
				orden += " " + campos[i] + " " + (sortOrder.name().equalsIgnoreCase("ASCENDING")?"ASC":"DESC");
				if (i!=campos.length-1) {
					orden += ", ";
				}
			}
		} else {
			orden = " " + NotificacionFichero.dameCampo(sortField) 
				+ " " + (sortOrder.name().equalsIgnoreCase("ASCENDING")?"ASC":"DESC");
		}
		return orden;
	}
	
	private static String busqueda(String numExpediente, String tipoDocENI, String convocatoriaLinea, Date fechaInicial, Date fechaFinal, Integer cdGestion, String nombreTitular, String identTitular) {
		String where = "";
		// numExpediente
		if (numExpediente!=null && !numExpediente.trim().equals("")) {
			where += " NUM_EXPTE like '%" + numExpediente + "%' AND ";
		}
		// tipoDocENI
		if (tipoDocENI!=null) {
			where += " TIPODOC_ENI = '" + tipoDocENI + "' AND ";
		}
		// convocatoriaLinea
		if (convocatoriaLinea!=null) {
			where += " CONVOCATORIA_L = '" + convocatoriaLinea + "' AND ";
		}		
		// intervalo [fechaInicial,fechaFinal]
		if (fechaInicial!=null && fechaFinal!=null) {
			where += " FH_EMISION BETWEEN TO_DATE ('" + new SimpleDateFormat("yyyy-MM-dd").format(fechaInicial) + "', 'yyyy-mm-dd') AND TO_DATE ('" + new SimpleDateFormat("yyyy-MM-dd").format(fechaFinal) + "', 'yyyy-mm-dd') AND ";
		} else if (fechaInicial!=null) {
			where += " FH_EMISION >= TO_DATE ('" + new SimpleDateFormat("yyyy-MM-dd").format(fechaInicial) + "', 'yyyy-mm-dd') AND ";
		} else if (fechaFinal!=null) {
			where += " FH_EMISION <= TO_DATE ('" + new SimpleDateFormat("yyyy-MM-dd").format(fechaFinal) + "', 'yyyy-mm-dd') AND ";
		}		
		// cdGestion
		if (cdGestion!=null) {
			where += " CD_GESTION = '" + cdGestion + "' AND ";
		}		
		// nombreTitular
		if (nombreTitular!=null && !nombreTitular.trim().equals("")) {
			where += " NOMBRE_TIT || ' ' || APELLIDO1_TIT || ' ' || APELLIDO2_TIT like '%" + nombreTitular + "%' AND ";
		}		
		// identTitular
		if (identTitular!=null && !identTitular.trim().equals("")) {
			where += " CIF_TIT || DNI_TIT || NIF_TIT like '%" + identTitular + "%' AND ";
		}			

		return where + " 1=1 ";
	}
	
	public static int listadoGlobalNotificacionesTotal (
			String numExpediente, String tipoDocENI, String convocatoriaLinea,
			Date fechaInicial, Date fechaFinal, Integer cdGestion, String nombreTitular, String identTitular,
			Map<String, Object> filters)
			throws SQLException, NamingException {
		DataBase db = new DataBase();
		int total = 0;
		try {
			db.connect();
			ResultSet rs;
			String consulta = StringUtils.replace(listadoGlobalNotificacionesTotal, "{filtro}", filtrosWhere(filters));
			consulta = StringUtils.replace(consulta, "{busqueda}", busqueda(numExpediente, tipoDocENI, convocatoriaLinea, fechaInicial, fechaFinal, cdGestion, nombreTitular, identTitular));
			rs = db.select(consulta);
			
			while (rs.next()) {
				total = rs.getInt(1);
			}
			db.closeRs(rs);	
		} finally {
			
			db.disconnect();
			db.getCon().close();
		}
		return total;			
	}
	
	public static ArrayList<NotificacionFichero> listadoGlobalNotificaciones(int pageSize, int first,
			String numExpediente, String tipoDocENI, String convocatoriaLinea,
			Date fechaInicial, Date fechaFinal, Integer cdGestion, String nombreTitular, String identTitular,
			String sortField, SortOrder sortOrder, Map<String, Object> filters)
			throws SQLException, NamingException {

		ArrayList<NotificacionFichero> listado = new ArrayList<NotificacionFichero>();
		DataBase db = new DataBase();
		try {
			db.connect();
			ResultSet rs;
			String consulta = StringUtils.replace(StringUtils.replace(listadoGlobalNotificaciones,"{orden}", filtroOrder(sortField, sortOrder)), "{filtro}", filtrosWhere(filters));
			consulta = StringUtils.replace(consulta, "{busqueda}", busqueda(numExpediente, tipoDocENI, convocatoriaLinea, fechaInicial, fechaFinal, cdGestion, nombreTitular, identTitular));
			rs = db.select(consulta, new Object[] { DataBase.parseNumber(first),
						DataBase.parseNumber(first + pageSize) });

			while (rs.next()) {
				NotificacionFichero notificacionFichero = new NotificacionFichero();
				// ORDEN
				// ID_NOTIFICACION 
				notificacionFichero.setId(rs.getInt(1 + 1));

				// ...
				
				listado.add(notificacionFichero);
			}
			db.closeRs(rs);					
		} finally {
			db.disconnect();
			db.getCon().close();
		}
		return listado;
	}		



 	public static void 	ponerACeroNumeroDeIntentosDeEnvio(int idNotificacion, Bean dus)
			throws SQLException, NamingException {
		
		DataBase db = new DataBase();
		try {
			db.connect(dus);
			db.update(poneACeroNumeroDeIntentosDeEnvio, new Object[] { DataBase.parseNumber(idNotificacion) });
			db.commit();
		} finally {
			db.disconnect();
			db.getCon().close();
		}		
	}	
	
	public static NotificacionFichero selectNotificacionById (Integer id) throws SQLException, NamingException {
		
		return id==null?new NotificacionFichero():consultarNotificaciones (selectNotificacionById, id).get(0);
	}	
	
	
	
	
	public static void marcaFicheroABorrado(Integer id, Bean dus) throws SQLException, NamingException {

		DataBase db = new DataBase();
		try {
			db.connect(dus);
			db.update(marcaFicheroABorrado, new Object[] { DataBase.parseNumber(id) });
			db.commit();
		} finally {
			db.disconnect();
			db.getCon().close();
		}
	}	
	
	
	
	public static void deleteNotificacionById (Integer id, Bean dus) throws SQLException, NamingException {
		
		DataBase db = new DataBase();
		try {
			db.connect(dus);
			db.update(deleteNotificacionById, new Object[] { DataBase.parseNumber(id) });
			db.commit();
		} finally {
			db.disconnect();
			db.getCon().close();
		}			
	}
	
	
	
	public static void informaAcuseEntregaNotificacion(int idNotificacion, int idAcuseRecibo, Bean dus) 
			 throws SQLException, NamingException {
		
		DataBase db = new DataBase();
		try {
			db.connect(dus);
			db.update(informaAcuseEntregaNotificacion, new Object[] { DataBase.parseNumber(idAcuseRecibo), DataBase.parseNumber(idNotificacion) });
			db.commit();
		} finally {
			db.disconnect();
			db.getCon().close();
		}		
	}	
	
	
	
	public static void informaAcuseEnvioNotificacion(int idNotificacion, int idAcuseEnvio, Bean dus) 
			throws SQLException, NamingException {
		
		DataBase db = new DataBase();
		try {
			db.connect(dus);
			db.update(informaAcuseEnvioNotificacion, new Object[] { DataBase.parseNumber(idAcuseEnvio), DataBase.parseNumber(idNotificacion) });
			db.commit();
		} finally {
			db.disconnect();
			db.getCon().close();
		}		
	}		
	

	
	public static void informaFechaEmisionNotificacion(int idNotificacion, Bean dus) 
			throws SQLException, NamingException {
		
		DataBase db = new DataBase();
		try {
			db.connect(dus);
			db.update(informaFechaEmisionNotificacion, new Object[] { DataBase.parseDate(new java.util.Date()), DataBase.parseNumber(idNotificacion) });
			db.commit();
		} finally {
			db.disconnect();
			db.getCon().close();
		}		
	}	
	
	
	
	public static void informaFechaEntregaNotificacion(int idNotificacion, Date fecha, Bean dus) 
			throws SQLException, NamingException {
		
		DataBase db = new DataBase();
		try {
			db.connect(dus);
			db.update(informaFechaEntregaNotificacion, new Object[] { DataBase.parseDate(fecha), DataBase.parseNumber(idNotificacion) });
			db.commit();
		} finally {
			db.disconnect();
			db.getCon().close();
		}		
	}	
	
	
	
	public static void informaFechaEnvioNotificacion(int idNotificacion, Date fecha, Bean dus)
			throws SQLException, NamingException {
		
		DataBase db = new DataBase();
		try {
			db.connect(dus);
			db.update(informaFechaEnvioNotificacion, new Object[] { DataBase.parseDate(fecha), DataBase.parseNumber(idNotificacion) });
			db.commit();
		} finally {
			db.disconnect();
			db.getCon().close();
		}	
	}	
	
	
	
	public static void incrementarNumeroIntentosEnvioNotificacion(int idNotificacion, int numIntentos, Bean dus)
			throws SQLException, NamingException {
		
		DataBase db = new DataBase();
		try {
			db.connect(dus);
			db.update(incrementarNumeroIntentosEnvioNotificacion, new Object[] { DataBase.parseNumber(numIntentos), DataBase.parseNumber(idNotificacion) });
			db.commit();
		} finally {
			db.disconnect();
			db.getCon().close();
		}
	}
	
	
	
	public static void 	actualizarEstadoNotificacion(int idNotificacion, int estado, Bean dus)
			throws SQLException, NamingException {
		
		DataBase db = new DataBase();
		try {
			db.connect(dus);
			db.update(actualizaEstadoNotificacion, new Object[] { DataBase.parseNumber(estado), DataBase.parseNumber(idNotificacion) });
			db.commit();
		} finally {
			db.disconnect();
			db.getCon().close();
		}		
	}
	
	
	
	public static void informaNumeroRemesaNotificacion(int idNotificacion, int numRemesa, Bean dus)
			throws SQLException, NamingException {
		DataBase db = new DataBase();

		try {
			db.connect(dus);
			db.update(informaNumeroRemesaNotificacion, new Object[] { DataBase.parseNumber(numRemesa), DataBase.parseNumber(idNotificacion) });
			db.commit();
		} finally {
			db.disconnect();
			db.getCon().close();
		}
	}
	
	
	public static void modificaNotificacionManual(Integer id, Date acuse, String observaciones, Boolean checkActiva, Bean dus) 
			throws SQLException, NamingException {
		DataBase db = new DataBase();

		try {
			db.connect(dus);
			db.update(modificaNotificacionManual, new Object[] { 
					DataBase.parseDate(acuse), 
					DataBase.parseVarchar2(observaciones),
					DataBase.parseNumber(checkActiva?1:0),
					DataBase.parseNumber(id)});
			db.commit();
		} finally {
			db.disconnect();
			db.getCon().close();
		}		
	}
	
	
	public static void modificaActivacionNotificacionElectronica(Integer id, Integer activa, Bean dus)
			throws SQLException, NamingException {
		DataBase db = new DataBase();

		try {
			db.connect(dus);
			db.update(modificaActivacionNotificacionElectronica, new Object[] { 
					DataBase.parseNumber(activa),
					DataBase.parseNumber(id)});
			db.commit();
		} finally {
			db.disconnect();
			db.getCon().close();
		}			
	}
	
	public static ArrayList<NotificacionFichero> obtenerColaNotificaciones() 
			throws SQLException, NamingException {
		
		return consultarNotificaciones(obtenerColaNotificaciones, null);
		
	}
	
	// notificaciones sincronizables, estados EMITIDA y PUESTA_A_DISPOSICION
	public static ArrayList<NotificacionFichero> consultarNotificacionesSincronizables()
			throws SQLException, NamingException {
		return consultarNotificaciones(consultarNotificacionesSincronizables, null);
	}
	
	
	
	// notificaciones desistidas, intentos = NotificaManager.NUM_MAX_INTENTOS_ENVIO
	public static ArrayList<NotificacionFichero> consultarNotificacionesParaArchivado()
			throws SQLException, NamingException {
		return consultarNotificaciones(consultarNotificacionesParaArchivado, null);
	}
	
	
	
	// notificaciones nuevas desde el �ltimo env�o (intentos = 0)
	public static ArrayList<NotificacionFichero> consultarNotificacionesNuevas()
			throws SQLException, NamingException {
		return consultarNotificaciones(consultarNotificacionesNuevas, null);
	}
	
	
	
	// notificaciones fallidas reenviables, 0 < intentos < NotificaManager.NUM_MAX_INTENTOS_ENVIO
	public static ArrayList<NotificacionFichero> consultarNotificacionesFallidasParaReenvio()
			throws SQLException, NamingException {
		return consultarNotificaciones(consultarNotificacionesFallidasParaReenvio, null);
	}
	

	
	public static ArrayList<NotificacionFichero> consultarNotificaciones(String consulta, Integer id)
			throws SQLException, NamingException {

		ArrayList<NotificacionFichero> listado = new ArrayList<NotificacionFichero>();
		DataBase db = new DataBase();
		try {
			db.connect();
			ResultSet rs;
			if (id == null) {
				rs = db.select(consulta);
				rs.setFetchSize(100);
			} else {
				rs = db.select(consulta, new Object[] { DataBase.parseNumber(id) });
			}
			while (rs.next()) {
				NotificacionFichero notificacionFichero = new NotificacionFichero();
				// ...

				listado.add(notificacionFichero);
			}
			db.closeRs(rs);					
		} finally {
			db.disconnect();
			db.getCon().close();
		}
		return listado;
	}	
	
	
	public static Integer selectIdSOLIC_AYUDAPorNumExpediente (String numExpediente) throws SQLException, NamingException {
		
		
		Integer resultado = null;
		
		DataBase db = new DataBase();
		try {
			db.connect();
			ResultSet rs;
			rs = db.select(selectIdSOLIC_AYUDAPorNumExpediente, 
					new Object[] { DataBase.parseVarchar2(numExpediente) });

			while (rs.next()) {
				resultado = rs.getInt("id_SOLIC_AYUDA");
			}
			db.closeRs(rs);					
		} finally {
			db.disconnect();
			db.getCon().close();
		}
		return resultado;			
	}
	
	
	public static Integer consultaNotificacionActivaPorNumExpediente (String numExpediente, String tipo, int manual) throws SQLException, NamingException {
		
		
		Integer resultado = null;
		
		DataBase db = new DataBase();
		try {
			db.connect();
			ResultSet rs;
			rs = db.select(notificacionActivaPorExpte, 
					new Object[] { DataBase.parseVarchar2(numExpediente), DataBase.parseVarchar2(tipo) , manual });

			while (rs.next()) {
				resultado = rs.getInt("ID_NOTIFICACION");
			}
			db.closeRs(rs);					
		} finally {
			db.disconnect();
			db.getCon().close();
		}
		return resultado;			
	}	

	
	public static Long insertaNotificacionNueva (NotificacionFichero notificacionFichero, Bean dus) 
			throws SQLException, NamingException {

		
    	Connection con;
    	con = ConnectionFactory.getConnection(m_nombreJNDI);
		con.setAutoCommit(false); // Forzamos la llamada al COMMIT	    	
        PreparedStatement statement = con.prepareStatement(insertaNotificacionNueva,
                                      new String[]{"ID_NOTIFICACION"});      

        statement.setObject(1, notificacionFichero.getIdFichero(), Types.INTEGER);
        // ...
        
        int affectedRows = statement.executeUpdate();
        if (affectedRows == 0) {
            throw new SQLException("Creating item failed, no rows affected.");
        }
        ResultSet generatedKeys = statement.getGeneratedKeys();
        
        if (generatedKeys.next()) {
            Long idAuto = generatedKeys.getLong(1);
            con.commit();
            generatedKeys.close();
            con.close();            
            return idAuto;
        }
        else {
        	generatedKeys.close();
            con.close();        	
            throw new SQLException("Creating item failed, no ID obtained.");
        }
	}	
}
