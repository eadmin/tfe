package src.integracion;


// pantalla de control de notificaciones (carga lazy)

@ManagedBean(name = "notificaBean")
@ViewScoped
public class NotificaBean {

	private static final Logger LOG = Logger.getLogger(Bean.class);
	private int numNots;

	private Bean dus = (Bean) FacesContext.getCurrentInstance().getExternalContext()
			.getSessionMap().get("Bean");

	private List<NotificacionFichero> listadoNotificaciones;
	private List<NotificacionFichero> notificacionesSeleccionadas;

	private NotificacionFichero notificacionSeleccionada;

	private Boolean cmDetalleEnabled = false;
	private Boolean cmEnviarYaEnabled = false;
	private Boolean cmPausarEnvioEnabled = false;
	private Boolean cmReanudarEnvioEnabled = false;
	private Boolean cmReencolarEnabled = false;

	// filtro funcional

	// firmable/gesti�n
	Integer cdGestionFiltro;
	List<FirmableBean> listadoGestionesFiltro;
	// convocatoria
	String convocatoriaFiltro;
	List<ConvocatoriaBean> listadoConvocatoriasFiltro;
	// periodo
	Date fechaIniFiltro;
	Date fechaFinFiltro;
	// n� expediente
	String numExpdteFiltro;
	// nombre del titular
	String nombreTitularFiltro;
	// identificador del titular
	String identTitularFiltro;
	// tipo ENI de documento
	String tipoDocumentalFiltro;

	public NotificaBean() {
		super();
		try {

			// carga combos filtro funcional
			// convocatorias
			listadoConvocatoriasFiltro = UsuariosDAO
					.obtenerConvocatoriasByLinea(new LineaBean(CtesGenerales.CONV, new MedidaBean("MEDIDA")));
			// gestiones
			listadoGestionesFiltro = UsuariosDAO.obtenerAllFirmables();

			inicializaPanelBusqueda();

			// carga datos tabla
			lazyLoad();

		} catch (Exception e) {
			LOG.error(e, e);
		}
	}

	public void inicializaPanelBusqueda() {
		// firmable/gesti�n
		cdGestionFiltro = null;
		// convocatoria
		convocatoriaFiltro = null;
		// periodo
		fechaIniFiltro = null;
		fechaFinFiltro = null;
		// n� expediente
		numExpdteFiltro = null;
		// nombre del titular
		nombreTitularFiltro = null;
		// identificador del titular
		identTitularFiltro = null;
		// tipo ENI de documento
		tipoDocumentalFiltro = null;
	}

	public void mostrarDiagrama() {
		return;
	}

	public void seleccionoCheck() {
		actualizaVisibilidadBotonesAccion();
	}

	public void desseleccionoCheck() {
		if (((HttpRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest())
				.getParameter("buscar") != null) {
			// recarga datos tabla
			lazyLoad();
		}

		if (((HttpRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest())
				.getParameter("refrescar") != null
				|| ((HttpRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest())
						.getParameter("buscar") != null) {
			FacesContext.getCurrentInstance().addMessage("mensaje", new FacesMessage(FacesMessage.SEVERITY_INFO,
					"El contenido se ha actualizado", "El contenido se ha actualizado"));
			PrimeFaces.current().ajax().update("formulario:mensaje");
		}

		actualizaVisibilidadBotonesAccion();
	}

	private void actualizaVisibilidadBotonesAccion() {
		// cmDetalle visible cuando la selecci�n es �nica
		this.cmDetalleEnabled = notificacionesSeleccionadas.size() == 1;
		Boolean soloEnEstadoEnCola = false;
		Boolean soloEnEstadoDesistidaONoPuesta = false;
		Boolean soloEnEstadoEnPausa = false;
		if (notificacionesSeleccionadas.size() > 0) {

			// cmEnviarYa y cmPausar cuando s�lo hay notificaciones
			// seleccionadas en estado EN_COLA
			soloEnEstadoEnCola = true;
			Iterator<NotificacionFichero> it = notificacionesSeleccionadas.iterator();
			while (it.hasNext()) {
				if (it.next().getEstado() != EstadoNotificacion_enum.EN_COLA) {
					soloEnEstadoEnCola = false;
					break;
				}
			}

			// cmReencolar cuando s�lo hay notificaciones seleccionadas en
			// alguno de los estados DESISTIDA, BORRADA, NO_PUESTA_A_DISP
			soloEnEstadoDesistidaONoPuesta = true;
			it = notificacionesSeleccionadas.iterator();
			while (it.hasNext()) {
				NotificacionFichero elemento = it.next();
				if (elemento.getEstado() != EstadoNotificacion_enum.DESISTIDA
						&& elemento.getEstado() != EstadoNotificacion_enum.NO_PUESTA_A_DISP_PROBLEMA_TECNICO_NOTIFICA
						&& elemento.getEstado() != EstadoNotificacion_enum.NO_PUESTA_A_DISP_USUARIO_NO_ALTA_NOTIFICA
						&& elemento.getEstado() != EstadoNotificacion_enum.NO_PUESTA_A_DISP_USUARIO_NO_ALTA_SERVICIO) {
					soloEnEstadoDesistidaONoPuesta = false;
					break;
				}
			}

			// cmReanudar cuando s�lo hay notificaciones seleccionadas en estado
			// EN_PAUSA
			soloEnEstadoEnPausa = true;
			it = notificacionesSeleccionadas.iterator();
			while (it.hasNext()) {
				if (it.next().getEstado() != EstadoNotificacion_enum.EN_PAUSA) {
					soloEnEstadoEnPausa = false;
					break;
				}
			}

		}

		this.cmEnviarYaEnabled = soloEnEstadoEnCola;
		this.cmPausarEnvioEnabled = soloEnEstadoEnCola;
		this.cmReanudarEnvioEnabled = soloEnEstadoEnPausa;
		this.cmReencolarEnabled = soloEnEstadoDesistidaONoPuesta;

	}

	public Boolean getCmDetalleEnabled() {
		return cmDetalleEnabled;
	}

	public void setCmDetalleEnabled(Boolean cmDetalleEnabled) {
		this.cmDetalleEnabled = cmDetalleEnabled;
	}

	public Boolean getCmEnviarYaEnabled() {
		return cmEnviarYaEnabled;
	}

	public void setCmEnviarYaEnabled(Boolean cmEnviarYaEnabled) {
		this.cmEnviarYaEnabled = cmEnviarYaEnabled;
	}

	public Boolean getCmPausarEnvioEnabled() {
		return cmPausarEnvioEnabled;
	}

	public void setCmPausarEnvioEnabled(Boolean cmPausarEnvioEnabled) {
		this.cmPausarEnvioEnabled = cmPausarEnvioEnabled;
	}

	public Boolean getCmReanudarEnvioEnabled() {
		return cmReanudarEnvioEnabled;
	}

	public void setCmReanudarEnvioEnabled(Boolean cmReanudarEnvioEnabled) {
		this.cmReanudarEnvioEnabled = cmReanudarEnvioEnabled;
	}

	public Boolean getCmReencolarEnabled() {
		return cmReencolarEnabled;
	}

	public void setCmReencolarEnabled(Boolean cmReencolarEnabled) {
		this.cmReencolarEnabled = cmReencolarEnabled;
	}

	public void mostrarDetalle() throws NamingException, SQLException {
		// muestra el detalle de la notificaci�n seleccionada
		this.notificacionSeleccionada = NotificacionDAO
				.selectNotificacionById(notificacionesSeleccionadas.iterator().next().getId());

	}

	public void enviarAhoraSeleccionadas() throws NotificaExcepcion {
		// Adelanta el env�o de las notificaciones encoladas seleccionadas
		int errores = 0;

		Iterator<NotificacionFichero> it = notificacionesSeleccionadas.iterator();
		NotificaManager nm = new NotificaManager(dus);
		while (it.hasNext()) {
			try {
				nm.enviaAnticipadoNotificacion(it.next());
			} catch (Exception e) {
				errores++;
			}
		}

		// refresco de estado de las notificaciones tras la orden
		PrimeFaces.current().executeScript("document.getElementById('formulario:tabla:btnRefres').click();");

		if (errores > 0) {
			throw new NotificaExcepcion("** " + this.getClass() + "#"
					+ "enviarAhoraSeleccionadas(). Ocurri� un problema en el env�o manual de las notificaciones.");
		}

	}

	public void pausarSeleccionadas() throws NotificaExcepcion {
		// Pausa el env�o de las notificaciones encoladas seleccionadas (antes
		// del env�o programado del CRON)
		int errores = 0;

		Iterator<NotificacionFichero> it = notificacionesSeleccionadas.iterator();
		NotificaManager nm = new NotificaManager(dus);
		while (it.hasNext()) {
			errores += nm.pausaNotificacion(it.next());
		}

		// refresco de estado de las notificaciones tras la orden
		PrimeFaces.current().executeScript("document.getElementById('formulario:tabla:btnRefres').click();");

		if (errores > 0) {
			throw new NotificaExcepcion("** " + this.getClass() + "#" + "pausarSeleccionadas(). Ocurri� un problema.");
		}

	}

	public void reanudarSeleccionadas() throws NotificaExcepcion {
		// Reanuda el env�o de las notificaciones pausadas manualmente
		// seleccionadas
		int errores = 0;

		Iterator<NotificacionFichero> it = notificacionesSeleccionadas.iterator();
		NotificaManager nm = new NotificaManager(dus);
		while (it.hasNext()) {
			errores += nm.reanudaNotificacion(it.next());
		}

		// refresco de estado de las notificaciones tras la orden
		PrimeFaces.current().executeScript("document.getElementById('formulario:tabla:btnRefres').click();");

		if (errores > 0) {
			throw new NotificaExcepcion(
					"** " + this.getClass() + "#" + "reanudarSeleccionadas(). Ocurri� un problema.");
		}

	}

	public void reencolarSeleccionadas() throws NotificaExcepcion {
		// vuelve a poner en la cola notificaciones que ya no se revisaban
		int errores = 0;

		Iterator<NotificacionFichero> it = notificacionesSeleccionadas.iterator();
		NotificaManager nm = new NotificaManager(dus);
		while (it.hasNext()) {
			errores += nm.reencolaNotificacion(it.next());
		}

		// refresco de estado de las notificaciones tras la orden
		PrimeFaces.current().executeScript("document.getElementById('formulario:tabla:btnRefres').click();");

		if (errores > 0) {
			throw new NotificaExcepcion("** " + this.getClass() + "#"
					+ "reencolarSeleccionadas(). Ocurri� un problema en el reencolado manual de las notificaciones.");
		}

	}

	private LazyDataModel<NotificacionFichero> model;

	public LazyDataModel<NotificacionFichero> getModel() {
		return model;
	}

	public void setModel(LazyDataModel<NotificacionFichero> model) {
		this.model = model;
	}

	public String listadoNotificaciones() {
		return dus.listadoNotificaciones();
	}

	public List<NotificacionFichero> getListadoNotificaciones() {
		return listadoNotificaciones;
	}

	public void setListadoNotificaciones(List<NotificacionFichero> listadoNotificaciones) {
		this.listadoNotificaciones = listadoNotificaciones;
	}

	public List<NotificacionFichero> getNotificacionesSeleccionadas() {
		return notificacionesSeleccionadas;
	}

	public void setNotificacionesSeleccionadas(List<NotificacionFichero> notificacionesSeleccionadas) {
		this.notificacionesSeleccionadas = notificacionesSeleccionadas;
	}

	private void lazyLoad() {

		model = new LazyDataModel<NotificacionFichero>() {
			private static final long serialVersionUID = 1L;
			int totalRegistros = 0;

			// M�todo llamado al seleccionar una fila de la tabla
			@Override
			public NotificacionFichero getRowData(String rowKey) {
				@SuppressWarnings("unchecked")
				List<NotificacionFichero> nots = (List<NotificacionFichero>) getWrappedData();

				for (NotificacionFichero not : nots) {
					if (calculateRowKey(not).equals(rowKey)) {
						return not;
					}
				}
				return null;
			}

			@Override
			public Object getRowKey(NotificacionFichero not) {
				return not != null ? calculateRowKey(not) : null;
			}

			private String calculateRowKey(NotificacionFichero not) {
				// devuelve valor basado en la clave natural (�nico) y no debe
				// contener caracteres coma (,)
				return not.getId().toString();
			}

			@Override
			public List<NotificacionFichero> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {

				List<NotificacionFichero> listaNots = null;
				try {

					listaNots = NotificacionDAO.listadoGlobalNotificaciones(pageSize, first, numExpdteFiltro,
							tipoDocumentalFiltro, convocatoriaFiltro, fechaIniFiltro, fechaFinFiltro, cdGestionFiltro,
							nombreTitularFiltro, identTitularFiltro, sortField, sortOrder, filters);

					totalRegistros = NotificacionDAO.listadoGlobalNotificacionesTotal(numExpdteFiltro,
							tipoDocumentalFiltro, convocatoriaFiltro, fechaIniFiltro, fechaFinFiltro, cdGestionFiltro,
							nombreTitularFiltro, identTitularFiltro, filters);
					numNots = totalRegistros;

				} catch (Exception e) {
					LOG.error(e, e);
				}

				setRowCount(totalRegistros);


				return listaNots;
			}

		};

	}

	public int getNumNots() {
		return numNots;
	}

	public Set<Map.Entry<String, Object[]>> getEstadosEntries() {
		return EstadoNotificacion_enum.getAsMapa().entrySet();
	}

	public NotificacionFichero getNotificacionSeleccionada() {
		return notificacionSeleccionada;
	}

	public void setNotificacionSeleccionada(NotificacionFichero notificacionSeleccionada) {
		this.notificacionSeleccionada = notificacionSeleccionada;
	}

	public Integer getCdGestionFiltro() {
		return cdGestionFiltro;
	}

	public void setCdGestionFiltro(Integer cdGestionFiltro) {
		this.cdGestionFiltro = cdGestionFiltro;
	}

	public List<FirmableBean> getListadoGestionesFiltro() {
		return listadoGestionesFiltro;
	}

	public void setListadoGestionesFiltro(List<FirmableBean> listadoGestionesFiltro) {
		this.listadoGestionesFiltro = listadoGestionesFiltro;
	}

	public String getConvocatoriaFiltro() {
		return convocatoriaFiltro;
	}

	public void setConvocatoriaFiltro(String convocatoriaFiltro) {
		this.convocatoriaFiltro = convocatoriaFiltro;
	}

	public Date getFechaIniFiltro() {
		return fechaIniFiltro;
	}

	public void setFechaIniFiltro(Date fechaIniFiltro) {
		this.fechaIniFiltro = fechaIniFiltro;
	}

	public Date getFechaFinFiltro() {
		return fechaFinFiltro;
	}

	public void setFechaFinFiltro(Date fechaFinFiltro) {
		this.fechaFinFiltro = fechaFinFiltro;
	}

	public String getNumExpdteFiltro() {
		return numExpdteFiltro;
	}

	public void setNumExpdteFiltro(String numExpdteFiltro) {
		this.numExpdteFiltro = numExpdteFiltro;
	}

	public String getNombreTitularFiltro() {
		return nombreTitularFiltro;
	}

	public void setNombreTitularFiltro(String nombreTitularFiltro) {
		this.nombreTitularFiltro = nombreTitularFiltro;
	}

	public String getIdentTitularFiltro() {
		return identTitularFiltro;
	}

	public void setIdentTitularFiltro(String identTitularFiltro) {
		this.identTitularFiltro = identTitularFiltro;
	}

	public List<ConvocatoriaBean> getListadoConvocatoriasFiltro() {
		return listadoConvocatoriasFiltro;
	}

	public void setListadoConvocatoriasFiltro(List<ConvocatoriaBean> listadoConvocatoriasFiltro) {
		this.listadoConvocatoriasFiltro = listadoConvocatoriasFiltro;
	}

	public TipoDocumentalDocumento[] getListadoTiposDocumentalFiltro() {
		return TipoDocumentalDocumento.values();
	}

	public String getTipoDocumentalFiltro() {
		return tipoDocumentalFiltro;
	}

	public void setTipoDocumentalFiltro(String tipoDocumentalFiltro) {
		this.tipoDocumentalFiltro = tipoDocumentalFiltro;
	}

}
