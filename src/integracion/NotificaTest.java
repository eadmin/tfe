package src.integracion;

import java.util.Date;



public class NotificaTest { //extends TestCase {

	// * ---- *
	// LOG: (clase logger, de log4j).
	// * ---- *
	static Logger logger = Logger.getLogger(NotificaTest.class.getName());

	/**
	 * @uml.property name="mcsn"
	 * @uml.associationEnd multiplicity="(1 1)"
	 */
	private MCSN mcsn = null;

	public NotificaTest(String testName)
	{
//		super(testName);
		try {
			// Instanciamos el API cliente de Notific@ indicandole el archivo de
			// configuraci�n de acceso al Sistema de Notificaciones

			 Configuration config = new Configuration().loadProperties("C:/mcsn.properties");

			mcsn = new MCSN(config);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}


	/**
	 * Realiza una alta de un usuario al servicio de por defecto 1 (Empleo)
	 */
	public void testAltaAbonado() throws Exception {

		logger.info("Dando de alta el Abonado en el servicio 87");
		Abonado abonado = new Abonado("A99999989");
		abonado.setNombre("Empresa de pruebas");
		abonado.setEmail("email@pruebas.es");

		// A continuaci�n se debe indicar la firma realizada por el usuario
		// dando su consentimiento expreso de suscripcici�n a un servicio.
		// Como se puede observar se puede indicar el FormReference y el
		// transaction ID
		// devuelto por el servidor de @firma o la firma en formato DER
		// (PKCS#7)

//		FirmaInf firma = new FirmaInf(null, null);

		// String pathFirma = "C:/jf_base64.p7s";
		// java.io.File firmaFile = new java.io.File(pathFirma);
		// if (!firmaFile.exists())
		// throw new Exception("No se ha encontrado el adjunto en "
		// + pathFirma);

		// java.io.FileInputStream fis = new java.io.FileInputStream(firmaFile);
		// java.io.ByteArrayOutputStream baos = new java.io.ByteArrayOutputStream();
		// byte[] buffer = new byte[1024];
		// int i = 0;
		// while ((i = fis.read(buffer)) != -1) {
		// baos.write(buffer, 0, i);
		// }
		// fis.close();
		// baos.close();
		// //FirmaInf firma = new FirmaInf(baos.toByteArray());
		//
		// FirmaInf firma = new FirmaInf("OBSOLETO", "2146689757");

		// Finalmente invocamos al m�todo de alta de abonado, indicando el
		// objeto abonado,
		// la firma y el c�digo de servicio al que se tiene que suscribir
		mcsn.solicitarAltaAbonado(abonado, new FirmaInf(), 87);
		logger.info("Alta realizada correctamente");

//		assertTrue(true);
	}

	/**
	 * Realiza una baja de un abonado del servicio de por defecto 1 (Empleo)
	 */
	public void testBajaAbonado() {
		try {
			logger.info("Dando de baja el Abonado del servicio 87 ");
			Abonado abonado = new Abonado("A99999989");
			mcsn.solicitarBajaAbonado(abonado, 87);
			logger.info("Baja realizada correctamente");
		}
		catch (Exception e) {
			logger.error("ERROR", e);
//			fail("Error. " + e.getMessage());
		}
	}

	/**
	 * Comprueba si uno o varios abonados estan suscritos a un servicio
	 */
	public void testEstadoAbondadoServicio() {
		try {
			logger.info("Comprobando estado de usuario en servicio");
			String[] ids = new String[1];

			ids[0] = "33378964J";

			int[] res = mcsn.solicitarEstadoAbonadosServicio(ids, 87);
			for (int i = 0; i < res.length; i++) {
				logger.info("Resultado " + i + ":" + res[i]);

				if (res[i] == -1){
					logger.info("El usuario " + ids[i] + " NO est� dado de alta en el sistema");
				} else if (res[i] == 0){
					logger.info("El usuario " + ids[i] + " NO est� suscrito al SERVICIO 87, pero si esta dado de alta en el sistema");
				} else if (res[i] == 1){
					logger.info("El usuario " + ids[i] + " SI est� suscrito al SERVICIO 1");
				}
			}
			logger.info("Solicitud de Estado realizada correctamente");
		}
		catch (Exception e) {
			logger.error("ERROR", e);
//			fail("Error. " + e.getMessage());
		}
	}

	/**
	 * Obtiene el certificado de un conjunto de abonados suscritos
	 */
	public void testObtieneCertificadosAbonados() {
		try {
			logger.info("Obteniendo certificados de usuarios...");
			String[] ids = new String[1];
			ids[0] = "33378964J";

			CertificadoInf[] res = mcsn.solicitarCertificadoAbonados(ids);
			for (int i = 0; i < res.length; i++) {
				if (res[i] == null){
					logger.info("El usuario " + ids[i] + " NO est� dado de alta en el sistema o no se encuentra su certificado");
				} else {
					logger.info("Se ha obtenido el certificado del usuario " + res[i].getIdAbonado() + ": " + new String(Base64.encode(res[i].getCertificado())));
				}
			}
			logger.info("Solicitud de Obtenci�n realizada correctamente");

		}
		catch (Exception e) {
			logger.error("ERROR", e);
//			fail("Error. " + e.getMessage());
		}
	}

	/**
	 * Permite obtener informacion acerca de los abonados dados de alta por iniciativa propia a un
	 * servicio asociado a la Entidad Emisora M�todo importante.
	 */
	public void testInfAltasAbonado() {
		try {
			logger.info("Solicitando informacion de abonados dados de alta por iniciativa propia...");
			// Es importante indicar la fecha de inicio y final con el form�to siguiente
			java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
			java.util.Date fechaIni = formatter.parse("01.01.2015 00:00:00");
			java.util.Date fechaFin = formatter.parse("06.03.2016 23:59:59");
			AbonadoInf[] abonados = mcsn.solicitarInformacionAltasIniciativaAbonado(fechaIni, fechaFin, 87);
			if (abonados == null) {
				return;
			}

			for (int i = 0; i < abonados.length; i++) {
				logger.info("Nombre de abonado[" + i + "]: " + abonados[i].getNombre() + ", ID " + abonados[i].getIdAbonado());
				logger.info("Fecha de Alta:" + abonados[i].getFechaAlta());

			}
			logger.info("Solicitud realizada");
		}
		catch (Exception e) {
			logger.error("ERROR", e);
//			fail("Error. " + e.getMessage());
		}
	}

	/**
	 * Permite obtener informacion acerca de los abonados dados de alta por iniciativa propia a un
	 * servicio asociado a la Entidad Emisora M�todo importante.
	 */
	public void testInfBajasAbonado() {
		try {
			logger.info("Solicitando informacion de abonados dados de baja de un servicio entre una fecha inicial y final");
			java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
			java.util.Date fechaIni = formatter.parse("01.01.2011 12:00:00");
			java.util.Date fechaFin = formatter.parse("22.11.2016 11:15:59");
			AbonadoInf[] abonados = mcsn.solicitarInformacionBajasAbonado(fechaIni, fechaFin, 87);
			if (abonados == null) {
				return;
			}
			for (int i = 0; i < abonados.length; i++) {
				logger.info("Nombre de abonado[" + i + "]: " + abonados[i].getNombre());
				logger.info("Apellidos de abonado[" + i + "]: " + abonados[i].getApellidos());
				logger.info("Fecha de Baja: " + abonados[i].getFechaBaja());
				logger.info("Fecha de Alta:" + abonados[i].getFechaAlta());
				logger.info("M�vil:" + abonados[i].getTelefonoMovil());
				logger.info("Email:" + abonados[i].getEmail());
			}
			logger.info("Solicitud realizada");
		}
		catch (Exception e) {
			logger.error("ERROR", e);
//			fail("Error. " + e.getMessage());
		}
	}

	/**
	 * Permite obtener los datos de contacto de un abonado dado de alta en el servicio indicado que
	 * debe estar asociado a la entidad emisora solicitante.
	 */
	public void testInfAbonadoSuscrito() {
		try {
			logger.info("Solicitando informacion de abonados suscritos a un servicio...");
			String identificador = "33345678Z";

			Abonado abonado = mcsn.obtenerInfAbonadoSuscrito(identificador, 87);
			if (abonado == null) {
				return;
			} else {
				logger.info("ID: " + abonado.getIdAbonado() + ", Nombre de abonado: " + abonado.getNombre() + ", Apellidos:" + abonado.getApellidos() + ", Email " + abonado.getEmail() + ", Movil: " + abonado.getTelefonoMovil());
				logger.info("-----------------------------------------------------------------------");
			}

			logger.info("Solicitud finalizada");
		}
		catch (Exception e) {
			logger.error("ERROR", e);
//			fail("Error. " + e.getMessage());
		}
	}
	
	/**
     * Permite obtener los datos de contacto de un titular
     */
    public void testInfTitular() {
        try {
            logger.info("Solicitando informaci�n de titulares...");
            String nifTitular = "33349938P";

            Titular titular = mcsn.obtenerInfTitular( nifTitular );
            if (titular == null) {
                return;
            } else {
                logger.info("NIF: " + titular.getNif() + ", Nombre de titular: " + titular.getNombre() + ", Apellidos:" + titular.getApellidos());
                logger.info("-----------------------------------------------------------------------");
            }

            logger.info("Solicitud finalizada");
        }
        catch (Exception e) {
            logger.error("ERROR", e);
//            fail("Error. " + e.getMessage());
        }
    }

	/**
	 * Obtiene informaci�n acerca de las remesas enviadas entre una fecha inicial y final a un
	 * servicio determinado y que tienen notificaciones NO LEIDAS SIN RECHAZAR.
	 */
	public void testInfRemesasConNotifNoLeidas() {
		try {
			logger.info("Solicitando informacion de remesas que contengan notificaciones NO leidas.");
			int remesasEnviadas[] = new int[1];
			remesasEnviadas[0] = 0;
			java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("dd.MM.yyyy");
			java.util.Date fechaIni = formatter.parse("01.11.2017 00:00:00");
			java.util.Date fechaFin = formatter.parse("07.11.2017 23:59:59");
			// El campo false indica que no se quieren obtener aquellos que hayan sido rechazados
			RemesaInf[] remesaInfs = mcsn.obtenerInfRemesaConNotifNoLeidas(fechaIni, fechaFin, 87, true);
			if (remesaInfs == null) {
				logger.info("No hay ninguna notificacion no leida");
				return;
			}

			for (int i = 0; i < remesaInfs.length; i++) {
				mostrarRemesa(remesaInfs[i]);
			}
			logger.info("Solicitud realizada");

		}
		catch (Exception e) {
			logger.error("ERROR", e);
//			fail("Error. " + e.getMessage());
		}
	}

	/**
	 * Obtiene informaci�n acerca de las remesas enviadas entre una fecha inicial y final a un
	 * servicio determinado y que tienen notificaciones RECHAZADAS autom�ticamente por el sistema
	 * por la no lectura por el usuario en 10 d�as habile o por el usuario.
	 */
	public void testInfRemesasConNotifRechazadas() {
		try {
			logger.info("Solicitando informacion de remesas que contengan notificaciones NO leidas.");
			int remesasEnviadas[] = new int[1];
			remesasEnviadas[0] = 0;
			java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
			java.util.Date fechaIni = formatter.parse("01.01.2016 00:00:00");
			java.util.Date fechaFin = formatter.parse("06.03.2016 23:59:59");
			RemesaInf[] remesaInfs = mcsn.obtenerInfRemesaConNotifNoLeidas(fechaIni, fechaFin, 87, true);
			if (remesaInfs == null) {
				logger.info("No hay ninguna notificacion no leida");
				return;
			}

			for (int i = 0; i < remesaInfs.length; i++) {
				mostrarRemesa(remesaInfs[i]);
			}
			logger.info("Solicitud realizada");

		}
		catch (Exception e) {
			logger.error("ERROR", e);
//			fail("Error. " + e.getMessage());
		}
	}

	/**
	 * Obtiene informaci�n acerca de las remesas enviadas entre una fecha inicial y final a un
	 * servicio determinado y que tienen notificaciones LEIDAS.
	 */
	public void testInfRemesasConNotifLeidas() {
		try {
			logger.info("Solicitando informacion de remesas que contengan notificaciones leidas.");

			java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
			java.util.Date fechaIni = formatter.parse("01.11.2017 00:00:00");
			java.util.Date fechaFin = formatter.parse("07.11.2017 23:59:59");

			RemesaInf[] remesaInfs = mcsn.obtenerInfRemesaConNotifLeidas(fechaIni, fechaFin, 87);
			for (int i = 0; i < remesaInfs.length; i++) {
				mostrarRemesa(remesaInfs[i]);
			}
			logger.info("Solicitud realizada");
		}
		catch (Exception e) {
			logger.error("ERROR", e);
//			fail("Error. " + e.getMessage());
		}
	}

	/**
	 * Permite obtener informaci�n acerca de las remesas enviadas entre una fecha inicial y final a
	 * un servicio determinado
	 */
	public void testInfRemesasEnviadasEntreFechaIniyFinal() {
		try {
			logger.info("Solicitando informacion de remesas enviadas entre una fecha inicial \"2003.01.01\" y una final " + new java.util.Date(System.currentTimeMillis()));
			java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
			java.util.Date fechaIni = formatter.parse("01.02.2016 00:00:00");
			java.util.Date fechaFin = formatter.parse("15.02.2016 23:59:59");
			RemesaInf[] remesaInfs = mcsn.obtenerInfRemesa(fechaIni, fechaFin, 87);
			for (int i = 0; i < remesaInfs.length; i++) {
				mostrarRemesa(remesaInfs[i]);
			}
			logger.info("Solicitud realizada");

		}
		catch (Exception e) {
			logger.error("ERROR", e);
//			fail("Error. " + e.getMessage());
		}
	}

	/**
	 * Permite obtener informaci�n acerca de las remesas enviadas. A diferencia de los otros m�todos
	 * que obtienen informaci�n de remesas, en este m�todo es necesario indicar los c�digos de
	 * remesa devueltos en el env�o
	 */
	public void testInfRemesasEnviadas() {
		try {
			logger.info("Solicitando informacion de remesas enviadas.");
			int remesasEnviadas[] = new int[1];

			// ws031
			remesasEnviadas[0] = 151947;

			RemesaInf[] remesaInfs = mcsn.obtenerInfRemesas(remesasEnviadas, true);
			for (int i = 0; i < remesaInfs.length; i++) {
				mostrarRemesa(remesaInfs[i]);

				for (int j = 0; j < remesaInfs[i].getNotificaciones().length; j++) {
					byte[] acusePD = remesaInfs[i].getNotificaciones()[j].getAcusePuestaDisposicion();
					String acusePDString = new String(acusePD);
					System.out.println(acusePDString);
				}
			}
			logger.info("Solicitud realizada");
		}
		catch (Exception e) {
			logger.error("ERROR", e);
//			fail("Error. " + e.getMessage());
		}
	}

	/**
	 * Permite obtener informaci�n acerca de las remesas enviadas. A diferencia de los otros m�todos
	 * que obtienen informaci�n de remesas, en este m�todo es necesario indicar los c�digos de
	 * remesa devueltos en el env�o
	 */
	public void testInfCompletaRemesasEnviadas() {
		try {
			logger.info("Solicitando informacion de remesas enviadas.");
			int remesasEnviadas[] = new int[1];
			remesasEnviadas[0] = 151947;
			RemesaInfCompleta[] remesaInfs = mcsn.obtenerInfCompletaRemesas(remesasEnviadas);
			for (int i = 0; i < remesaInfs.length; i++) {
				mostrarRemesaCompleta(remesaInfs[i]);
				for (int j = 0; j < remesaInfs[i].getNotificacionesCompleta().length; j++) {
					byte[] acusePD = remesaInfs[i].getNotificacionesCompleta()[j].getAcusePuestaDisposicion();
					String acusePDString = new String(acusePD);

					System.out.println(acusePDString);
				}
			}
			logger.info("Solicitud realizada");
		}
		catch (Exception e) {
			logger.error("ERROR", e);
//			fail("Error. " + e.getMessage());
		}
	}

	/**
	 * Obtiene informaci�n acerca de las remesas enviadas entre una fecha inicial y final a un
	 * servicio determinado y que tienen notificaciones asociadas a un conjunto de abonados (1..n).
	 */
	public void testInfRemesasEnviadasEntreFechaIniyFinalConNotifAsociadasAunosAbonados() {
		try {
			logger.info("Solicitando informacion de remesas enviadas.");
			java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
			java.util.Date fechaIni = formatter.parse("01.01.2016 00:00:00");
			java.util.Date fechaFin = formatter.parse("06.03.2016 23:59:59");
			Abonado abonado = new Abonado("15978964J");
			Abonado[] abonados = new Abonado[1];
			abonados[0] = abonado;
			RemesaInf[] remesaInfs = mcsn.obtenerInfRemesa(fechaIni, fechaFin, 87, abonados);
			if (remesaInfs != null) {
				for (int i = 0; i < remesaInfs.length; i++) {
					mostrarRemesa(remesaInfs[0]);
				}
			}
			logger.info("Solicitud realizada");

		}
		catch (Exception e) {
			logger.error("ERROR", e);
//			fail("Error. " + e.getMessage());
		}
	}

	/**
	 * Muestra informaci�n de una remesa de informaci�n obtenida
	 */
	private void mostrarRemesa(RemesaInf remesaInf) throws Exception {
		logger.info("***********************************************");
		logger.info("Cod Remesa: " + remesaInf.getIdRemesa());
		logger.info("EntidadEmisora: " + remesaInf.getNombreEntidad());
		logger.info("Fecha y Hora de admision: " + remesaInf.getFechaYHoraAdmision());
		// Mostramos las notificaciones
		NotificacionInf[] infNotificaciones = remesaInf.getNotificaciones();
		logger.info("Numero de notificaciones de la remesa: " + infNotificaciones.length);
		logger.info("***********************************************");
		for (int o = 0; o < infNotificaciones.length; o++) {
			logger.info("-----------------------------------------------");
			logger.info("Estado de la notificacion : " + EstadoNotificacion.getCadenaEstado(infNotificaciones[o].getEstado()));
			logger.info("Identificador de receptor : " + infNotificaciones[o].getReceptor().getIdAbonado());
			if (infNotificaciones[o].getEstado() >= 0) {
				logger.info("Nombre del Receptor : " + infNotificaciones[o].getReceptor().getNombre() + " " + infNotificaciones[o].getReceptor().getApellidos());
				logger.info("Asunto del mensaje : " + infNotificaciones[o].getAsuntoNotificacion());
				logger.info("Fecha y Hora de puesta a disposicion : " + infNotificaciones[o].getFechaYHoraPuestaDisposicion());
				logger.info("IdOrigen : " + infNotificaciones[o].getId());
				Date fechaRecibo = infNotificaciones[o].getFechaYHoraRecibo();
				if (fechaRecibo != null){
					logger.info("Fecha y Hora de recepci�n de la notificacion : " + fechaRecibo);
				}
				Date fechaRechazo = infNotificaciones[o].getFechaYHoraRechazo();
				if (fechaRechazo != null){
					logger.info("Fecha y Hora de rechazazo de la notificacion : " + fechaRechazo);
				}
			}
			logger.info("-----------------------------------------------");
		}
		logger.info("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-");
	}

	/**
	 * Muestra informaci�n de una remesa de informaci�n obtenida
	 */
	private void mostrarRemesaCompleta(RemesaInfCompleta remesaInf) throws Exception {
		logger.info("***********************************************");
		logger.info("Cod Remesa: " + remesaInf.getIdRemesa());
		logger.info("EntidadEmisora: " + remesaInf.getNombreEntidad());
		logger.info("Fecha y Hora de admision: " + remesaInf.getFechaYHoraAdmision());
		// Mostramos las notificaciones
		NotificacionInfCompleta[] infNotificaciones = remesaInf.getNotificacionesCompleta();
		logger.info("Numero de notificaciones de la remesa: " + infNotificaciones.length);
		logger.info("***********************************************");
		for (int o = 0; o < infNotificaciones.length; o++) {
			logger.info("-----------------------------------------------");
			logger.info("Estado de la notificacion : " + EstadoNotificacion.getCadenaEstado(infNotificaciones[o].getEstado()));
			logger.info("Identificador de receptor : " + infNotificaciones[o].getReceptor().getIdAbonado());
			if (infNotificaciones[o].getEstado() >= 0) {
				logger.info("Nombre del Receptor : " + infNotificaciones[o].getReceptor().getNombre() + " " + infNotificaciones[o].getReceptor().getApellidos());
				logger.info("Asunto del mensaje : " + infNotificaciones[o].getAsuntoNotificacion());
				logger.info("Fecha y Hora de puesta a disposicion : " + infNotificaciones[o].getFechaYHoraPuestaDisposicion());
				logger.info("IdOrigen : " + infNotificaciones[o].getId());
				Date fechaRecibo = infNotificaciones[o].getFechaYHoraRecibo();
				if (fechaRecibo != null){
					logger.info("Fecha y Hora de recepci�n de la notificacion : " + fechaRecibo);
				}
				Date fechaRechazo = infNotificaciones[o].getFechaYHoraRechazo();
				if (fechaRechazo != null){
					logger.info("Fecha y Hora de rechazazo de la notificacion : " + fechaRechazo);
				}
				String numeroRegistro = infNotificaciones[o].getNumeroRegistroAries();
				if (numeroRegistro != null){
					logger.info("N�mero registro @ries: " + numeroRegistro);
				}
				String fechaRegistroAries = infNotificaciones[o].getFechaRegistroAries();
				if (fechaRegistroAries != null){
					logger.info("Fecha registro @ries: " + fechaRegistroAries);
				}
			}
			logger.info("-----------------------------------------------");

		}
		logger.info("+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-");
	}

	/**
	 * Realiza el env�o de una remesa de notificaciones.
	 */
	public void testEnviarRemesa() {
		try {
			logger.info("Construyendo remesa...");

			String pathAdjunto1 = "K:/Trabajo/pdf-test.pdf";

			Abonado abonado1 = new Abonado("A99999989");

			java.io.File adjunto = new java.io.File(pathAdjunto1);
			if (!adjunto.exists()){
				throw new Exception("No se ha encontrado el adjunto en " + pathAdjunto1);
			}

			java.io.FileInputStream fis = new java.io.FileInputStream(adjunto);
			java.io.ByteArrayOutputStream baos = new java.io.ByteArrayOutputStream();
			byte[] buffer = new byte[1024];
			int i = 0;
			while ((i = fis.read(buffer)) != -1) {
				baos.write(buffer, 0, i);
			}
			fis.close();

			baos.close();

			// Construimos el adjunto
			Adjunto adj1 = new Adjunto(baos.toByteArray(), adjunto.getName());

			Notificacion notif1 = new Notificacion(false);
			notif1.setAsunto("Prueba de API 3.2.2.5 - env�o tradicional");
			notif1.setCuerpo("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras enim nunc, aliquam non sodales sed, elementum nec velit. Donec consectetur magna quis mauris imperdiet, nec blandit ligula bibendum. Aliquam cursus, nisi ut facilisis gravida, est justo tristique neque, at dignissim urna tellus quis metus. Aliquam eu fringilla elit. Integer vehicula tortor ut sagittis condimentum. Suspendisse odio felis, sagittis vitae interdum feugiat, finibus ut leo. Sed venenatis imperdiet erat non accumsan. Donec venenatis ex blandit, bibendum mi vitae, imperdiet dui.\n"
					+ "Integer eget metus lorem. Curabitur nisi massa, accumsan at ante at, condimentum mattis magna. Fusce posuere in libero ultricies convallis. Vestibulum eu porta purus. In placerat diam tellus, vehicula rutrum erat cursus eu. Integer semper aliquet velit at fringilla. Suspendisse potenti. Proin quis consequat turpis. Nullam ultrices interdum tellus id sodales. In fringilla elementum velit, vel laoreet massa dignissim non. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nunc sit amet ipsum sagittis purus dictum fermentum.");
			notif1.addAdjunto(adj1);
			notif1.addDestinatario(abonado1);

			Remesa remesa = new Remesa(87);
			remesa.addNotificacion(notif1);
			logger.info("Remesa construida.");

			int num = mcsn.enviarRemesa(remesa);
			logger.info("Remesa enviada: " + num);
		}
		catch (Exception e) {
			logger.error("ERROR", e);
//			fail("Error. " + e.getMessage());
		}
	}

	/**
	 * Realiza el env�o de una remesa con metadatos
	 */
	public void testEnviarRemesaDocMetadatos() {
		try {
			logger.info("Construyendo remesa...");
			String pathAdjunto1 = "K:/Trabajo/pdf-test.pdf";
			String pathAdjunto2 = "K:/Trabajo/pdf-test.pdf";

			Abonado abonado1 = new Abonado("48333806L");
			abonado1.setEmail("empresa.pruebas.opcional@pruebas.es");
			abonado1.setTelefonoMovil("555555555");
			
			String dir3 = "E00000000";
			boolean obligacionRelacionarseEletronicamente = true;
			boolean notificacionComplementaria = false;
			Titular titular = new Titular("E66233305");
			titular.setNombre( "razonSocialTitular" );

			java.io.File adjunto = new java.io.File(pathAdjunto1);
			java.io.File adjunto2 = new java.io.File(pathAdjunto2);
			
			if (!adjunto.exists()){
				throw new Exception("No se ha encontrado el adjunto en " + pathAdjunto1);
			}

			if (!adjunto2.exists()){
				throw new Exception("No se ha encontrado el adjunto 2 en " + pathAdjunto2);
			}
			
			java.io.FileInputStream fis = new java.io.FileInputStream(adjunto);
			java.io.ByteArrayOutputStream baos = new java.io.ByteArrayOutputStream();
			byte[] buffer = new byte[1024];
			int i = 0;
			while ((i = fis.read(buffer)) != -1) {
				baos.write(buffer, 0, i);
			}
			fis.close();

			baos.close();
			
			java.io.FileInputStream fis2 = new java.io.FileInputStream(adjunto2);
			java.io.ByteArrayOutputStream baos2 = new java.io.ByteArrayOutputStream();
			byte[] buffer2 = new byte[1024];
			int i2 = 0;
			while ((i2 = fis2.read(buffer2)) != -1) {
				baos2.write(buffer2, 0, i2);
			}
			fis2.close();

			baos2.close();

			// Construimos el adjunto
			Adjunto adj1 = new Adjunto(baos.toByteArray(), adjunto.getName());
			Adjunto adj2 = new Adjunto(baos2.toByteArray(), adjunto2.getName());

			NotificacionDocMetadatos notif1 = new NotificacionDocMetadatos(false);

			notif1.setAsunto("Prueba de API 3.3 - env�o con metadatos");
			notif1.setCuerpo("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras enim nunc, aliquam non sodales sed, elementum nec velit. Donec consectetur magna quis mauris imperdiet, nec blandit ligula bibendum. Aliquam cursus, nisi ut facilisis gravida, est justo tristique neque, at dignissim urna tellus quis metus. Aliquam eu fringilla elit. Integer vehicula tortor ut sagittis condimentum. Suspendisse odio felis, sagittis vitae interdum feugiat, finibus ut leo. Sed venenatis imperdiet erat non accumsan. Donec venenatis ex blandit, bibendum mi vitae, imperdiet dui.\n"
					+ "Integer eget metus lorem. Curabitur nisi massa, accumsan at ante at, condimentum mattis magna. Fusce posuere in libero ultricies convallis. Vestibulum eu porta purus. In placerat diam tellus, vehicula rutrum erat cursus eu. Integer semper aliquet velit at fringilla. Suspendisse potenti. Proin quis consequat turpis. Nullam ultrices interdum tellus id sodales. In fringilla elementum velit, vel laoreet massa dignissim non. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nunc sit amet ipsum sagittis purus dictum fermentum.");
			notif1.addAdjunto(adj1);
			notif1.addAdjunto(adj2);
			notif1.addDestinatarioV3( abonado1, dir3, obligacionRelacionarseEletronicamente, notificacionComplementaria, titular );
			notif1.setDocNotificacion(baos.toByteArray());
            notif1.setEnidocFechaCaptura(FechayHoraUtil.getFormatoFechaHora(new Date()));
            //notif1.setEnidocIdentificador("000000021474836471487925415675");
            notif1.setEnidocIdentificador("ES_A01002823_2015_PK2JM0000333000000000000141262");
            notif1.setEnidocOrgano("A01333823");
            notif1.setEnidocVersionNti("http://administracionelectronica.gob.es/ENI/XSD/v1.0/documento-e");
            notif1.setEniDocOrigenCiuAdmin("1");
            notif1.setEniDocTipoDocumental(TipoDocumentalDocumento.TD01.getValue());
            notif1.setEniDocValorEstElab("EE01");
            notif1.setCodigoRpaSia("10287");
			
			RemesaDocMetadatos remesa = new RemesaDocMetadatos(87, "A03334228");
			remesa.addNotificacion(notif1);
			logger.info("Remesa construida.");

			int num = mcsn.enviarRemesaEni(remesa);
			logger.info("Remesa enviada: " + num);
		}
		catch (Exception e) {
			logger.error("ERROR", e);
//			fail("Error. " + e.getMessage());
		}
	}

	/**
	 * Realiza el env�o de una remesa con un documento ENI
	 */
	public void testEnviarRemesaEni() {
		try {
			logger.info("Construyendo remesa...");

			Abonado abonado1 = new Abonado("A99999989"); //PF
			abonado1.setEmail("empresa.pruebas.opcional@pruebas.es");
			abonado1.setTelefonoMovil("555555555");
			
			String dir3 = "E00000000";
            boolean obligacionRelacionarseEletronicamente = true;
            boolean notificacionComplementaria = true;
            Titular titular = new Titular("28149938P");
            titular.setNombre( "Nombre Titular" );
			titular.setApellidos( "Apellidos Titular" );

			String pathEni = "K:\\Trabajo/documentoENI.xml";
			java.io.File eniDoc = new java.io.File(pathEni);
			if (!eniDoc.exists()){
				throw new Exception("No se ha encontrado el adjunto en " + pathEni);
			}

			java.io.FileInputStream fis = new java.io.FileInputStream(eniDoc);
			java.io.ByteArrayOutputStream baos = new java.io.ByteArrayOutputStream();
			byte[] buffer = new byte[1024];
			int i = 0;
			while ((i = fis.read(buffer)) != -1) {
				baos.write(buffer, 0, i);
			}
			fis.close();

			baos.close();

			// Construimos el DOC ENI
			byte[] eniDocByteArray = baos.toByteArray();

			// Construimos la notificaci�n
			NotificacionEniDoc notif1 = new NotificacionEniDoc(false);

			notif1.setAsunto("Prueba de API 3.3 - env�o con enidoc");
			notif1.addDestinatarioV3( abonado1, dir3, obligacionRelacionarseEletronicamente, notificacionComplementaria, titular );
			notif1.setCodigoExpediente("ES_A03332823_2018_EXP_11");
			notif1.setCodigoRpaSia("2036");
			notif1.setDocEni(eniDocByteArray);
			
			// Construimos la remesa EniDoc
			RemesaEniDoc remesa = new RemesaEniDoc(87, "A33314228");
			remesa.addNotificacion(notif1);
			logger.info("Remesa construida.");

			int num = mcsn.enviarRemesaEni(remesa);
			logger.info("Remesa enviada: " + num);

		}
		catch (Exception e) {
			logger.error("ERROR", e);
//			fail("Error. " + e.getMessage());
		}
	}

	/**
	 * Obtiene el n� de notificaciones pendientes
	 */
	public void testSolicitarNumNotifPendientes(){
		try {
			String anagramasAbons[] = new String[1];
			anagramasAbons[0] = "12345678Z";
			CertificadoInf[] ci = mcsn.solicitarCertificadoAbonados(anagramasAbons);
			for (CertificadoInf certificadoInf : ci) {
				System.out.println("CERTIFICADO: " + new String(certificadoInf.getCertificado()));
				System.out.println("ID ABONADO: " + certificadoInf.getIdAbonado());
			}
		}
		catch (Exception e) {
			logger.error("ERROR", e);
//			fail("Error. " + e.getMessage());
		}
	}
}

