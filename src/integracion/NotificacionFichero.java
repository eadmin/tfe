package src.integracion;



public class NotificacionFichero {
	
	// ID_NOTIFICACION			NUMBER(10,0)	PK
	private Integer id;
	// ID_FICHERO 				NUMBER(10,0)	FK
	private Integer idFichero;
	// ID_ACUSE_PUESTA_A_DISPOSICION NUMBER(10,0)	FK
	private Integer idAcusePuestaADisposicion;
	// ID_ACUSE_RECIBO 			NUMBER(10,0)	FK
	private Integer idAcuseRecibo;
	// CD_GESTION				NUMBER(5,0)
	private Integer cdGestion;
	// NUM_EXPTE 			VARCHAR2(25 BYTE)
	private String numExpediente;
	// TIPODOC_ENI 			VARCHAR2(2 BYTE)
	private String tipoDocEni;
	// CIF	 				VARCHAR2(1 BYTE)
	private String cif;
	// DNI 					VARCHAR2(8 BYTE)
	private String dni;
	// NIF 					VARCHAR2(1 BYTE)
	private String nif;	
	// NOMBRE 				VARCHAR2(25 BYTE)
	private String nombre;
	// APELLIDO1 			VARCHAR2(40 BYTE)
	private String apellido1;	
	// APELLIDO2 			VARCHAR2(25 BYTE)
	private String apellido2;
	// EMAIL 				VARCHAR2(50 BYTE)
	private String email;	
	// TELEFONO 				VARCHAR2(9 BYTE)
	private String telefono;	
	// CIF_TIT	 			VARCHAR2(1 BYTE)
	private String cifTitular;
	// DNI_TIT				VARCHAR2(8 BYTE)
	private String dniTitular;
	// NIF_TIT				VARCHAR2(1 BYTE)
	private String nifTitular;
	// NOMBRE_TIT 			VARCHAR2(25 BYTE)
	private String nombreTitular;
	// APELLIDO1_TIT 		VARCHAR2(40 BYTE)
	private String apellido1Titular;
	// APELLIDO2_TIT 		VARCHAR2(25 BYTE)
	private String apellido2Titular;
	// ASUNTO 				VARCHAR2(255 BYTE)
	private String asunto;
	// FH_EMISION 	DATE
	private Date fechaEmision;
	// FH_PUESTA_A_DISPOSICION 	DATE
	private Date fechaPuestaADisposicion;
	// FH_ACUSE_RECIBO 			DATE
	private Date fechaRecibo;
	// COD_REMESA 				NUMBER(10,0)
	private Integer codigoRemesa;
	// CD_ESTADO 				NUMBER(10,0)
	private EstadoNotificacion_enum estado;
	// INTENTOS 				NUMBER(1,0)
	private Integer numIntentos;	
	// USUARIO_AUDIT			VARCHAR2(20 BYTE)
	private String usuarioAuditoria;
	// FH_AUDIT					DATE
	private Date fechaAuditoria;
	// CONVOCATORIA_LINEA		VARCHAR2(6 BYTE)
	private String convocatoriaLinea;
	// OBSERVACIONES    		VARCHAR2(2000 BYTE)
	private String observaciones;
	// ES_MANUAL 				NUMBER(1,0)
	private Boolean manual = false;
	// ES_ACTIVA 				NUMBER(1,0)
	private Boolean activa = false;	
	
	public NotificacionFichero() {
		super();
	}
	
	public static String dameCampo(String atributo) {
		String [] atributos = 
				new String [] {"id", "idFichero", "idAcusePuestaADisposicion", "idAcuseRecibo", "cdGestion", "numExpediente", 
						"tipoDocEni", "cif", "dni", "nif", "nombre", "apellido1", "apellido2", "email", "telefono", 
						"asunto", "fechaPuestaADisposicion", "fechaRecibo", "codigoRemesa", "estado", 
						"numIntentos", "usuarioAuditoria", "fechaAuditoria", "convocatoriaLinea",
						"cifTitular", "dniTitular", "nifTitular", "nombreTitular", 
						"apellido1Titular", "apellido2Titular", "nombreCompletoDestinatario", "observaciones", "manual", "activa" };
		String [] campos = 
				new String [] {"ID_NOTIFICACION", "ID_FICHERO", "ID_ACUSE_PUESTA_A_DISPOSICION", "ID_ACUSE_RECIBO", "CD_GESTION", "NUM_EXPTE", 
						"TIPODOC_ENI", "CIF", "DNI", "NIF", "NOMBRE", "APELLIDO1", "APELLIDO2", "EMAIL", "TELEFONO", 
						"ASUNTO", "FH_PUESTA_A_DISPOSICION", "FH_RECIBO", "COD_REMESA", "CD_ESTADO", 
						"INTENTOS", "USUARIO_AUDIT", "FH_AUDIT", "CONVOCATORIA_LINEA", "CIF_TIT",
						"DNI_TIT", "NIF_TIT", "NOMBRE_TIT", "APELLIDO1_TIT", "APELLIDO2_TIT", "APELLIDO1, APELLIDO2, NOMBRE",
						"OBSERVACIONES","ES_MANUAL","ES_ACTIVA"};
		Map<String,String> mapa = new HashMap<String,String>();
		
		for (int i = 0 ; i < campos.length ; i++)
			mapa.put(atributos[i], campos[i]);
	
		return mapa.get(atributo);
	}	
	
	
	// constructor para notificaci�n manual
	public NotificacionFichero(
			String numExpediente, Integer cdGestion, Date acuse, String observaciones, String convocatoriaLinea, Boolean activa) {
		
		this(null, null, null, numExpediente, cdGestion, 
				null, null, null, null, null, null, null, null, null, null, null, null, convocatoriaLinea,
				null, null, null, null, null, null, observaciones, true, acuse, activa);		
	}

	public NotificacionFichero(Integer idFichero, Integer idAcusePuestaADisposicion, Integer idAcuseRecibo,
			String numExpediente, Integer cdGestion, String sAsunto, String cif, String dni, String nif,
			String nombre, String apellido1, String apellido2, String email, String telefono,
			EstadoNotificacion_enum estado, String tipoDocEni, Integer numIntentos, String convocatoriaLinea,
			String cifTitular, String dniTitular, String nifTitular, String nombreTitular, 
			String apellido1Titular, String apellido2Titular, String observaciones, Boolean manual, Date acuseManual, Boolean activa) {
		super();
		// mapea objeto de Notifica a Modelo
		this.idFichero = idFichero;
		this.idAcusePuestaADisposicion = idAcusePuestaADisposicion;
		this.idAcuseRecibo = idAcuseRecibo;
		this.cdGestion = cdGestion;
		this.numExpediente = numExpediente;
		this.asunto = sAsunto;
		this.cif = cif;
		this.dni = dni;
		this.nif = nif;
		this.nombre = nombre;
		this.apellido1 = apellido1;
		this.apellido2 = apellido2;
		this.email = email;
		this.telefono = telefono;
		this.estado= estado;
		this.tipoDocEni = tipoDocEni;
		this.numIntentos = numIntentos;
		this.convocatoriaLinea = convocatoriaLinea;
		this.cifTitular = cifTitular;
		this.dniTitular = dniTitular;
		this.nifTitular = nifTitular;
		this.nombreTitular = nombreTitular;
		this.apellido1Titular = apellido1Titular;
		this.apellido2Titular = apellido2Titular;
		this.observaciones = observaciones;
		this.manual = manual;
		// una notificaci�n electr�nica nueva siempre pasa a ser la activa
		// no as� una manual, que se puede grabar como inactiva
		this.activa = activa;
		this.fechaRecibo = acuseManual;
				
	}	
	
	// campo calculado
	public String getNombreCompletoDestinatario() {
		String nombre =  nvl(this.apellido1) + " " + nvl(this.apellido2) + ", " + nvl(this.nombre);
		return nombre.trim().equals(",")?"-":nombre;
	}
	
	private String nvl(String valor) {
		if (valor == null) return ""; else return valor; 
	}


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNumExpediente() {
		return numExpediente;
	}

	public void setNumExpediente(String numExpediente) {
		this.numExpediente = numExpediente;
	}

	public String getTipoDocEni() {
		return tipoDocEni;
	}

	public void setTipoDocEni(String tipoDocEni) {
		this.tipoDocEni = tipoDocEni;
	}

	public String getAsunto() {
		return asunto;
	}

	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}

	public Date getFechaPuestaADisposicion() {
		return fechaPuestaADisposicion;
	}

	public void setFechaPuestaADisposicion(Date fechaPuestaADisposicion) {
		this.fechaPuestaADisposicion = fechaPuestaADisposicion;
	}

	public Integer getCodigoRemesa() {
		return codigoRemesa;
	}

	public void setCodigoRemesa(Integer codigoRemesa) {
		this.codigoRemesa = codigoRemesa;
	}

	public String getUsuarioAuditoria() {
		return usuarioAuditoria;
	}

	public void setUsuarioAuditoria(String usuarioAuditoria) {
		this.usuarioAuditoria = usuarioAuditoria;
	}

	public Date getFechaAuditoria() {
		return fechaAuditoria;
	}

	public void setFechaAuditoria(Date fechaAuditoria) {
		this.fechaAuditoria = fechaAuditoria;
	}

	public Integer getNumIntentos() {
		return numIntentos;
	}

	public void setNumIntentos(Integer numIntentos) {
		this.numIntentos = numIntentos;
	}


	public Date getFechaRecibo() {
		return fechaRecibo;
	}


	public void setFechaRecibo(Date fechaRecibo) {
		this.fechaRecibo = fechaRecibo;
	}


	public EstadoNotificacion_enum getEstado() {
		return estado;
	}


	public void setEstado(EstadoNotificacion_enum estado) {
		this.estado = estado;
	}


	public Integer getIdFichero() {
		return idFichero;
	}


	public void setIdFichero(Integer idFichero) {
		this.idFichero = idFichero;
	}


	public Integer getIdAcusePuestaADisposicion() {
		return idAcusePuestaADisposicion;
	}


	public void setIdAcusePuestaADisposicion(Integer idAcusePuestaADisposicion) {
		this.idAcusePuestaADisposicion = idAcusePuestaADisposicion;
	}


	public Integer getIdAcuseRecibo() {
		return idAcuseRecibo;
	}


	public void setIdAcuseRecibo(Integer idAcuseRecibo) {
		this.idAcuseRecibo = idAcuseRecibo;
	}


	public String getCif() {
		return cif;
	}


	public void setCif(String cif) {
		this.cif = cif;
	}


	public String getDni() {
		return dni;
	}


	public void setDni(String dni) {
		this.dni = dni;
	}


	public String getNif() {
		return nif;
	}


	public void setNif(String nif) {
		this.nif = nif;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getApellido1() {
		return apellido1;
	}


	public void setApellido1(String apellido1) {
		this.apellido1 = apellido1;
	}


	public String getApellido2() {
		return apellido2;
	}


	public void setApellido2(String apellido2) {
		this.apellido2 = apellido2;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getTelefono() {
		return telefono;
	}


	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}


	public String getConvocatoriaLinea() {
		return convocatoriaLinea;
	}


	public void setConvocatoriaLinea(String convocatoriaLinea) {
		this.convocatoriaLinea = convocatoriaLinea;
	}


	public String getCifTitular() {
		return cifTitular;
	}


	public void setCifTitular(String cifTitular) {
		this.cifTitular = cifTitular;
	}


	public String getDniTitular() {
		return dniTitular;
	}


	public void setDniTitular(String dniTitular) {
		this.dniTitular = dniTitular;
	}


	public String getNifTitular() {
		return nifTitular;
	}


	public void setNifTitular(String nifTitular) {
		this.nifTitular = nifTitular;
	}


	public String getNombreTitular() {
		return nombreTitular;
	}


	public void setNombreTitular(String nombreTitular) {
		this.nombreTitular = nombreTitular;
	}


	public String getApellido1Titular() {
		return apellido1Titular;
	}


	public void setApellido1Titular(String apellido1Titular) {
		this.apellido1Titular = apellido1Titular;
	}


	public String getApellido2Titular() {
		return apellido2Titular;
	}


	public void setApellido2Titular(String apellido2Titular) {
		this.apellido2Titular = apellido2Titular;
	}

	public Date getFechaEmision() {
		return fechaEmision;
	}

	public void setFechaEmision(Date fechaEmision) {
		this.fechaEmision = fechaEmision;
	}

	public Integer getCdGestion() {
		return cdGestion;
	}

	public void setCdGestion(Integer cdGestion) {
		this.cdGestion = cdGestion;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public Boolean getManual() {
		return manual;
	}

	public void setManual(Boolean manual) {
		this.manual = manual;
	}

	public Boolean getActiva() {
		return activa;
	}

	public void setActiva(Boolean activa) {
		this.activa = activa;
	}



}
