package src.edu.desarrollo.tramitador.tramite.controlador;



public class ResolRecursoBean extends RecursoRecursoBean {

	
	private static final Logger LOG = Logger.getLogger(RecursoRecursoBean.class);
	
	private static String notificaAsuntoAvisoResolucionRecurso = "Notificacion de resoluci�n de recurso.";
	private static String notificaAsuntoAvisoResolucionRecursoModifica = "Notificacion de modificaci�n de resoluci�n recurso.";	

	// ...
	
	private ResolucionRecursoNotificacion resRecNotificacion;
	private ResolucionRecursoNotificacion resRecNotificacionModifica;	

	// ...
	
	private boolean mostrarNotificacion;
	private boolean mostrarNotificacionElectronica;

	// ...


	
	// NOTIFICACION

	// notificaciones del acto
	private NotificacionFichero notificacionElectronica;
	private NotificacionFichero notificacionManual;

	
	private Boolean envioAnticipado = true;
	private Boolean envioAnticipadoModifica = true;	
	
	private String textoDocumentoASubir;

	
	private Boolean bloqueoNotifElectronica;

	private Boolean bloqueoNotifManual;

	private Boolean bloqueoCheckCambioTipoNotificacion;

	private Boolean bloqueoActualizarNotifica;	
	
	private Boolean existeAceptacion;

	private Boolean existeNotificacionElectronicaConAcuse;

	private Boolean existeNotificacionManualActiva;

	
	// ...
	
	public ResolRecursoBean() {
		super();
		inicializaRecursoResolucion();
		actualizarEstadoNotificacion();

	}
	
	public void descargarInformeAcuse() {
		Date fechaActual = new Date();
		DateFormat fecha = new SimpleDateFormat("dd/MM/yyyy");
		String fechaActualconvertido = fecha.format(fechaActual);
		
		// Imprimimos el report
		String nombreReport = "TRAMITADOR_ACUSE";
		String parametrosReport = "";
		try {
			parametrosReport = this.getNotificacionElectronica().getId() + ";" +
				NotificaManager.dameValorAcuseReciboNotificacion(this.getNotificacionElectronica().getId(),"idNotificacion") + ";" +
				NotificaManager.dameValorAcuseReciboNotificacion(this.getNotificacionElectronica().getId(),"numRegistro") + ";" +
				NotificaManager.dameValorAcuseReciboNotificacion(this.getNotificacionElectronica().getId(),"fRegistro") + ";" +
				NotificaManager.dameValorAcuseReciboNotificacion(this.getNotificacionElectronica().getId(),"/gestorPF/identificador:/gestorRPJ/identificador") + ";" +
				NotificaManager.dameValorAcuseReciboNotificacion(this.getNotificacionElectronica().getId(),"/gestorPF/nombre:/gestorRPJ/nombre") + " " +
				NotificaManager.dameValorAcuseReciboNotificacion(this.getNotificacionElectronica().getId(),"/gestorPF/apellidos:/gestorRPJ/apellidos") + ";" ;
			
		
		DatosReport datosReports = new DatosReport();
		ArrayList<DatosVersionReport> listado = null;

		listado = InformesDAO.recuperaParametrosReport(nombreReport, fechaActualconvertido);

		datosReports.setPlantillaReport(listado.get(0).getNombrePlantilla());
		datosReports.setNombreReport(nombreReport);
		datosReports.setFecha(fechaActualconvertido);
		datosReports.setParametros(parametrosReport);

		HttpRequest request = (HttpRequest) FacesContext.getCurrentInstance().getExternalContext()
				.getRequest();
		Context ctx = request.getSession().getContext();
		InputStream reportSource = ctx
				.getResourceAsStream("/WEB-INF/reports/" + datosReports.getPlantillaReport() + ".jasper");

		byte[] informe = Utilidades.generarReport(reportSource, datosReports.getPlantillaReport(), "pdf",
				datosReports.getNombreReport(), datosReports.getFecha(), datosReports.getParametros());

		InputStream is = new ByteArrayInputStream(informe);

		this.ficheroInforme = new DefaultStreamedContent(is, "application/pdf", nombreReport + ".pdf");
		
		} catch (Exception e) {
			LOG.error(e, e);
			dus.muestraMensaje(CtesGenerales.ERROR, "No es posible generar el informe de acuse.");
		}

	}
	

	
	public void evitaPasoAMarcado(AjaxBehaviorEvent event) {
		// si valor de modelo es true, evitar cambio en el valor del modelo 
		// y refrescar en pantalla para que el check siga apareciendo como desmarcado 
		// la condici�n de bloqueo se convierte en bloqueo s�lo para paso de desmarcado a marcado
		Boolean cb = (Boolean)((SelectBooleanCheckbox) event.getSource()).getValue();
		
		this.setNoEsCambioTipo(false);
		
		grabarNotificacionManual();
//		es.getBloqueoNotifManual() && 
		if(cb && existeNotificacionElectronicaConAcuse) {
			this.getNotificacionManual().setActiva(false);
			((SelectBooleanCheckbox) event.getSource()).setValue(false);
			PrimeFaces.current().dialog().
				showMessageDynamic(new FacesMessage(FacesMessage.SEVERITY_INFO, "Mensaje", 
						"No es posible el cambio a notificaci�n manual al existir una notificaci�n electr�nica con fecha de acuse"));			
		}
		actualizaMensajeNotificacion();
		actualizaActivacionPanelesNotificacion();
	}
	
	public void actualizaMensajeNotificacion() {
		if (!mostrarNotificacionElectronica) return;
		mensajeNotificacion = "Seleccionada la notificaci�n ";
		if (this.notificacionManual!=null && this.notificacionManual.getActiva()!=null && this.notificacionManual.getActiva()) {
			mensajeNotificacion += "por otros medios  (externos a esta aplicaci�n) ";
			if (this.notificacionManual.getFechaRecibo()==null) {
				mensajeNotificacion += "todav�a sin fecha de acuse ";
			} else {
				mensajeNotificacion += "con fecha de acuse " 
						+ new SimpleDateFormat("dd-MM-YYYY").format(this.notificacionManual.getFechaRecibo());
				mensajeNotificacion += " y con estado Notificado.";
			}
			
		} else {
			mensajeNotificacion += "por Notific@ (gestionada por la aplicaci�n) ";
			if (this.notificacionElectronica.getFechaRecibo()==null) {
				mensajeNotificacion += "todav�a sin fecha de acuse ";
			} else {
				mensajeNotificacion += "con fecha de acuse " 
						+ new SimpleDateFormat("dd-MM-YYYY").format(this.notificacionElectronica.getFechaRecibo());
			}
			// estado
			if (this.notificacionElectronica.getEstado()==null) {
				mensajeNotificacion += " y sin estado.";
			} else {				
				mensajeNotificacion += " y con estado ";				
				mensajeNotificacion += this.notificacionElectronica.getEstado().getNombre();
				mensajeNotificacion += " (" + this.notificacionElectronica.getEstado().getDescripcion() + ")";	
				mensajeNotificacion += ".";
			}
		}
	}
	
	
	private void actualizaActivacionPanelesNotificacion() {
		existeAceptacion = RecursoResolucionDAO.existeAceptacionRegistradaParaExpediente(this.getIdSOLIC_AYUDA()); 
		// si existe notificaci�n electr�nica con fecha de acuse de recibo entonces desactivar todo el panel de notificaci�n manual
		existeNotificacionElectronicaConAcuse = this.notificacionElectronica.getId() !=null &&
				this.notificacionElectronica.getFechaRecibo() != null 
				&& (this.notificacionElectronica.getEstado() == EstadoNotificacion_enum.LEIDA_POR_USUARIO
				|| this.notificacionElectronica.getEstado() == EstadoNotificacion_enum.RECHAZADA_POR_USUARIO);	
		existeNotificacionManualActiva = this.notificacionManual.getId() !=null &&
				this.notificacionManual.getActiva();
		Boolean existeSinFallo = 	RecursoResolucionDAO.existeNotificacionSinFallo(this.getIdSOLIC_AYUDA(),true,false);	
		bloqueoNotifManual = existeNotificacionElectronicaConAcuse || existeAceptacion || this.getNotificacionManual()!=null && !this.getNotificacionManual().getActiva() || existeSinFallo;
		// si la notificaci�n manual est� activa se bloquea el panel de notificaci�n electr�nica
		bloqueoNotifElectronica = existeNotificacionManualActiva || existeAceptacion || existeSinFallo;	
		bloqueoActualizarNotifica = ! existeSinFallo;
		bloqueoCheckCambioTipoNotificacion = existeAceptacion;
	}


	private void inicializaRecursoResolucion() {
		try {

			// ...
			
			mostrarNotificacion = false;
			mostrarNotificacionElectronica = false;


			// ...
			
			
			// PANELES DE NOTIFICACION - INI
			
			// notificacion electronica lanzada por TRAMITADOR
			Integer idNotificacion = RecursoResolucionDAO.selectIdNotificacionResolucionRecursoPorIdSOLIC_AYUDA (this.getIdSOLIC_AYUDA(), true, false); 			
			this.notificacionElectronica = NotificacionDAO.selectNotificacionById (idNotificacion);  
			
			//Pasamos el recurso correspondiente a notificado
			if (idNotificacion != null)
				RecursoDAO.actualizaEstadoRecursoNoArchivadoByIdSOLIC_AYUDACdEstadoRecurso(this.getIdSOLIC_AYUDA(), RecEstadoRecurso_enum.NOTIFICADO.getValue());
		
				// notificacion manual, no lanzada por TRAMITADOR
				idNotificacion = RecursoResolucionDAO.selectIdNotificacionResolucionRecursoPorIdSOLIC_AYUDA (this.getIdSOLIC_AYUDA(), false, false); 			
				this.notificacionManual = NotificacionDAO.selectNotificacionById (idNotificacion);
	
				if (this.notificacionManual.getId()!=null) {
					// La documentacion subida los obtenemos de BBDD
					this.documentacionSubidaNotifica = FicheroSubidoDAO.obtenerDocumentacionSubidaNotificacion(this.notificacionManual.getId());
				}
				
				//Pasamos el recurso correspondiente a notificado
				if (idNotificacion != null)
					RecursoDAO.actualizaEstadoRecursoNoArchivadoByIdSOLIC_AYUDACdEstadoRecurso(this.getIdSOLIC_AYUDA(), RecEstadoRecurso_enum.NOTIFICADO.getValue());
			
			
			// refresco activacion de paneles notificaci�n
			actualizaActivacionPanelesNotificacion();
			
			// PANELES DE NOTIFICACION - FIN


			// ...
			
		} catch (Exception e) {
			dus.muestraMensaje(CtesGenerales.ERROR, "Error al recuperar los datos del c�lculo");
			LOG.error(e, e);
		}

	}
	

	
	
	private void desactivarNotificados()
	{
		
		try {
			
			Integer id = NotificacionDAO.consultaNotificacionActivaPorNumExpediente(this.getNumeroExpediente(),Tipo_documento_portafirma_enum.REF_RECRES_RESOLUCION.getValue(), 0);		
		
		if(id != null)
		{
			NotificacionDAO.modificaActivacionNotificacionElectronica(id , 0,  dus);
		}	

		id = NotificacionDAO.consultaNotificacionActivaPorNumExpediente(this.getNumeroExpediente(),Tipo_documento_portafirma_enum.REF_RECRES_RESOLUCION.getValue(), 1);
		if(id != null)		
		{
			NotificacionDAO.modificaActivacionNotificacionElectronica(id , 0,  dus);
		}

		
		} catch (Exception e) {
			LOG.error(e, e);
			dus.muestraMensaje(CtesGenerales.INFO, "No es posible desactivar la notificaci�n electr�nica anterior.");
		} 
				
	}
		

	public void grabarDatosNotificacionManual() {
		this.setNoEsCambioTipo(true);
		if(existeNotificacionElectronicaConAcuse) {
			PrimeFaces.current().dialog().
				showMessageDynamic(new FacesMessage(FacesMessage.SEVERITY_INFO, "Mensaje", 
						"No es posible guardar la notificaci�n manual al existir una notificaci�n electr�nica con fecha de acuse"));			
		return;
		}		
		grabarNotificacionManual();
	}
	
	private void grabarNotificacionManual() {
		Controlest controlest = (Controlest) Utilidades.getBeanSession("beanA"); controlest.est();

		Boolean esCambioTipo = false;		
		Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		esCambioTipo = Boolean.getBoolean(params.get("esCambioTipo"));
		
		// objeto con la info necesaria para una notificaci�n
		resRecNotificacion = RecursoResolucionDAO.consultaResolucionRecursoANotificarPorIdSOLIC_AYUDA(this.getIdSOLIC_AYUDA(), false);
		
				
		// debe haber fecha
		if (this.notificacionManual!=null && this.notificacionManual.getFechaRecibo()== null) {
			dus.muestraMensaje(CtesGenerales.INFO, "La fecha de acuse es obligatoria");
			return;	
		}

		
		// si la fecha es futura se avisa con mensaje
		Date fechaActual = new Date();
			if (this.notificacionManual!=null && this.notificacionManual.getFechaRecibo()!= null && this.notificacionManual.getFechaRecibo().compareTo(fechaActual) > 0) {
				dus.muestraMensaje(CtesGenerales.INFO, "La fecha de acuse no puede ser posterior al d�a de hoy.");
				return;				
			}
		
			
		// graba en base de datos (alta/modificacion)
		NotificaManager nm;
		try {
			nm = new NotificaManager(dus, true);
			
			// si existe notificaci�n electr�nica se marca ES_activa para evitar duplicidad transitoria
			if (this.notificacionElectronica !=null && this.notificacionElectronica.getId() != null) {
				nm.modificaActivacionNotificacionElectronica(this.notificacionElectronica.getId(), 
						-1);
			}
						
			// modificacion
			if(this.notificacionManual.getId()!=null) {
				nm.modificacionNotificacionManual(this.notificacionManual.getId(),
						this.notificacionManual.getFechaRecibo(), 
						this.notificacionManual.getObservaciones(), 
						this.notificacionManual.getActiva());
			} else {
			// alta
				this.notificacionManual.
				setNumExpediente(RecursoResolucionDAO.selectNumExpedientePorIdRecResGeneracion(this.getRecursoResolucionGeneracion().getIdRecresGeneracion()));
				this.notificacionManual.setConvocatoriaLinea(dus.getConvocatoriaLinea());
				this.notificacionManual.setCdGestion(15);
				
				this.notificacionManual.setId(
					nm.altaNotificacionManual(this.notificacionManual.getNumExpediente(), 
							this.notificacionManual.getConvocatoriaLinea(), 
							this.notificacionManual.getCdGestion(), 
							this.notificacionManual.getFechaRecibo(), 
							this.notificacionManual.getObservaciones(),
							this.notificacionManual.getActiva())
				);
				
				// guardamos la relaci�n entre la notificaci�n y el acto notificado
				RecursoResolucionDAO.insertaNotificacionActoNotificado(resRecNotificacion.getIdPortDocPet(),
						this.notificacionManual.getId(), null);
				
			}
			// si existe notificaci�n electr�nica se actualiza su activaci�n a la contraria de la manual
			//	ya que s�lo una puede estar activada
			if (this.notificacionElectronica.getId() != null) {
				nm.modificaActivacionNotificacionElectronica(this.notificacionElectronica.getId(), this.notificacionManual.getActiva()?0:1);
			}
			
			// se actualiza estado del expediente
			actualizaEstadoExpedienteSegunNotificacion();
			
			if (noEsCambioTipo)
				dus.muestraMensaje(CtesGenerales.INFO, "Los cambios se han grabado correctamente.");
			else
				dus.muestraMensaje(CtesGenerales.INFO, "El modo de notificaci�n ha sido cambiado correctamente.");	
			
		} catch (Exception e) {
			dus.muestraMensaje(CtesGenerales.ERROR, "Se produjo un error al modificar la informaci�n de notificaci�n.");
			LOG.error(e, e);
			return;
		}
		
		actualizaMensajeNotificacion();
		actualizaActivacionPanelesNotificacion();		
	}
	
	
	

	
	private void actualizaEstadoExpedienteSegunNotificacion() {
		String estadoFinal;
		try {
		if (RecursoResolucionDAO.existeFechaAcuseActivaNotificacionExpediente(this.getIdSOLIC_AYUDA()) && (!existeSentencia && ( esDesestimado || esInadmitido) )) {
			// estado RR
			estadoFinal = CtesGenerales.ESTADO_RECURSO_RESUELTO;
		} else {
			// estado PR
			estadoFinal = CtesGenerales.ESTADO_RECURSO_PRESENTADO;
		}
		
		SolicitudAyudaDAO.actualizaEstado(Integer.toString(this.getIdSOLIC_AYUDA()), estadoFinal, null);

		} catch (Exception e) {
			LOG.error(e, e);
		}
	}
	
	
	private void actualizaEstadoExpedienteSegunNotificacionModifica() {
		String estadoFinal;
		try {
		if (RecursoResolucionDAO.existeFechaAcuseActivaNotificacionExpedienteModifica(this.getIdSOLIC_AYUDA())) {
			// estado RR
			estadoFinal = CtesGenerales.ESTADO_RECURSO_RESUELTO;
		} else {
			// estado PR
			estadoFinal = CtesGenerales.ESTADO_RECURSO_PRESENTADO;
		}
		
		SolicitudAyudaDAO.actualizaEstado(Integer.toString(this.getIdSOLIC_AYUDA()), estadoFinal, null);

		} catch (Exception e) {
			LOG.error(e, e);
		}
	}

	

	public void actualizaFicherosSubidos() {
		// La documentacion subida los obtenemos de BBDD
		this.setDocumentacionSubidaNotifica(FicheroSubidoDAO.obtenerDocumentacionSubidaNotificacion(notificacionManual.getId()));
		// grabar datos de notificaci�n manual para evitar posible p�rdida de datos de pantalla
		grabarDatosNotificacionManual();
	}
	
	public void borrarFicheroSeleccionado() {
		Controlest controlest = (Controlest) Utilidades.getBeanSession("beanA"); controlest.est();
		// Borro el fichero
		Integer idFichero = ficheroABorrarNotifica.getIdentificador();

		// Marcamos como borrado el fichero
		String resultado = FicheroSubidoDAO.marcaFicheroBorrado(idFichero);
		if (resultado.equals("error")) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERROR", " Error al borrar el fichero."));
		}
		// actualizo los datos de la tabla
		else {
			if(notificacionManual != null && notificacionManual.getId() != null)
				this.setDocumentacionSubidaNotifica(FicheroSubidoDAO.obtenerDocumentacionSubidaNotificacion(notificacionManual.getId()));
			else if (notificacionManualModifica != null)
				this.setDocumentacionSubidaNotificaModifica(FicheroSubidoDAO.obtenerDocumentacionSubidaNotificacion(notificacionManualModifica.getId()));
			
		}
	}
	
	
	

	
	
	public void actualizarEstadoNotificacion() {
		NotificaManager nm;
		try {
			Integer idNotifica = RecursoResolucionDAO.idNotificacionSinFallo(this.getIdSOLIC_AYUDA(),false) ;
			if (idNotifica ==null) return;
			NotificacionFichero notificacion = NotificacionDAO.selectNotificacionById(idNotifica);
			nm = new NotificaManager(dus);
			nm.sincronizaNotificacion(notificacion);
			// actualiza modelo
			this.notificacionElectronica = NotificacionDAO.selectNotificacionById(idNotifica);
			// se actualiza estado del expediente
			if (esDesestimado || esInadmitido)
				actualizaEstadoExpedienteSegunNotificacion();
		} catch (NotificaExcepcion e) {
			dus.muestraMensaje(CtesGenerales.INFO, e.getMessage());
			LOG.error(e, e);
			return;
		} catch (Exception e) {
			if (e.getMessage()!=null) {
				if (e.getMessage().contains("503")) {
					dus.muestraMensaje(CtesGenerales.ERROR, "Existe un problema de comunicaci�n con Notific@. Por favor, int�ntelo de nuevo en unos minutos.");
				} else {
					dus.muestraMensaje(CtesGenerales.ERROR, "Se produjo un error al actualizar el estado de la notificaci�n.");
				}
			}
			LOG.error(e, e);
		} finally {
			actualizaMensajeNotificacion();
			actualizaActivacionPanelesNotificacion();			
		}
		
	}
	
	


	public void notificaResRec() {
		// comando de encolado y/o emisi�n de la notificaci�n electr�nica
		NotificaManager nm;
		try {
			nm = new NotificaManager(dus);
		} catch (NotificaExcepcion e) {
			dus.muestraMensaje(CtesGenerales.ERROR, "Se produjo un error al instanciar notificaManager.");
			LOG.error(e, e);
			setEnvioAnticipado(true);
			return;
		}		
		
		// hay que distinguir encolado de emisi�n. El encolado s�lo se har� una vez. La emisi�n se har�
		// hasta que �sta sea efectiva. Por tanto se permiten reemisiones de encoladas con error de emisi�n
		// o de encoladas no emitidas a�n (en espera de env�o diferido)
		
		// una notificaci�n se env�a una sola vez al motor de notificaci�n (encolado de la notificaci�n)
		if (RecursoResolucionDAO.compruebaSiEstaEncoladaResolucionesRecursoPorIdSOLIC_AYUDA(this.getIdSOLIC_AYUDA(),false)) {
			// si la notificaci�n tiene error interno entonces se intenta la reemisi�n
			if (this.notificacionElectronica.getEstado() == EstadoNotificacion_enum.NO_EMITIDA_PROBLEMA_TECNICO ||
					this.notificacionElectronica.getEstado() == EstadoNotificacion_enum.DESISTIDA ||
					this.notificacionElectronica.getEstado() == EstadoNotificacion_enum.YA_NO_EXISTE_EN_NOTIFICA ||		
					this.notificacionElectronica.getEstado() == EstadoNotificacion_enum.NO_PUESTA_A_DISP_PROBLEMA_TECNICO_NOTIFICA ||					
					this.notificacionElectronica.getEstado() == EstadoNotificacion_enum.EN_PAUSA ||
					this.notificacionElectronica.getEstado() == EstadoNotificacion_enum.EN_COLA) {
				
				if (this.envioAnticipado) {
					try {
						nm.enviaAnticipadoNotificacion (this.notificacionElectronica);
						this.notificacionElectronica = NotificacionDAO.selectNotificacionById (this.notificacionElectronica.getId());
					} catch (NotificaExcepcion e) {
						LOG.error(e, e);
						dus.muestraMensaje(CtesGenerales.INFO, "No es posible enviar la notificaci�n electr�nica.");
						setEnvioAnticipado(true);
						return;
					} catch (Exception e) {
						LOG.error(e, e);
					}
				} else {
					dus.muestraMensaje(CtesGenerales.INFO, "No es posible encolar la notificaci�n electr�nica una segunda vez. Pruebe a marcar el check 'Directo a Notific@' para un env�o inmediato.");
				}
				return;
				
			} else {
				// si no mensaje de no posible
				dus.muestraMensaje(CtesGenerales.INFO, "No es posible encolar la notificaci�n electr�nica una segunda vez.");
				setEnvioAnticipado(true);
				return ;
			}
		} else {

		// objeto con la info necesaria para una notificaci�n
		resRecNotificacion = RecursoResolucionDAO.consultaResolucionRecursoANotificarPorIdSOLIC_AYUDA(this.getIdSOLIC_AYUDA(), false);

		
		if (resRecNotificacion == null || resRecNotificacion.getIdSOLIC_AYUDA()==0) {
			dus.muestraMensaje(CtesGenerales.ERROR, "Error al recuperar el informe a notificar.");
			setEnvioAnticipado(true);
			return;
		}
		
		List<ResolucionRecursoNotificacion> documentosANotificar = new ArrayList<ResolucionRecursoNotificacion>();	
		documentosANotificar.add(resRecNotificacion);
		
		int total = documentosANotificar.size();
		int encoladosOK = 0;
		int encoladosKO = 0;
		
		
		String convocatoriaLinea = dus.getConvocatoriaLinea();
		Integer idNotifica = null;

		for (ResolucionRecursoNotificacion resRecdNotificacion : documentosANotificar) {

			// Asunto de la notificaci�n
			String numExpediente = resRecdNotificacion.getSOLIC_AYUDA().getNumExpediente();
			//PropertyResourceBundle bundle = CargadorSingleton.getInstance().getPropertyResourceBundle();
			String asunto = ResolRecursoBean.notificaAsuntoAvisoResolucionRecurso;
			asunto = asunto + " Expediente " + numExpediente;
						
			// fichero firmado a notificar
			byte[] fichero = Utilidades.recuperaFicheroEnBytes(resRecdNotificacion.getIdDocFirmado());
			
			// Si no existe Entidad Habilita o existe y no asume las notificaciones
			// 	Cuando haya representante legal ser� el destinatario y el solicitante ser� el titular
			//	cuando no lo haya el solicitante ser� el destinatario
			// Si existe Entidad Habilita y asume las notificaciones
			//  Siempre ser� el destinatario la EH y el titular el interesado
			
			boolean existeRepLegal = resRecdNotificacion.getSOLIC_AYUDA().getDniRepLegal()!=null &&
					!resRecdNotificacion.getSOLIC_AYUDA().getDniRepLegal().equals("");
			
			SASolicitud datosBasicosSolicitud = new SASolicitud();			
			SALugarNotifYDatosBancarios lugarNotifDatosBancarios = new SALugarNotifYDatosBancarios();
			
			DataBase db = new DataBase();
			try {
				db.connect();	
				SALugarNotifYDatosBancariosDao.obtenerLugarNotificacionSOLIC_AYUDA(lugarNotifDatosBancarios, resRecdNotificacion.getSOLIC_AYUDA().getIdSOLIC_AYUDA(), db);
				if (lugarNotifDatosBancarios!=null && lugarNotifDatosBancarios.getLugarNotificacion()!=null && lugarNotifDatosBancarios.getLugarNotificacion().isNotificaEntHabi()) {
					datosBasicosSolicitud = SASolicitudDao.obtenerSOLIC_AYUDA(Integer.valueOf(resRecdNotificacion.getSOLIC_AYUDA().getIdSOLIC_AYUDA()), db);
				}
			} catch (Exception e) {
				LOG.error(e, e);
				dus.muestraMensaje(CtesGenerales.ERROR, CtesErrores.TXT_ERROR_GENERICO_CONSULTA);
			} finally {
				try {
					db.disconnect();
					db.getCon().close();
				} catch (Exception e) {
					LOG.error(e, e);
					dus.muestraMensaje(CtesGenerales.ERROR, CtesErrores.TXT_ERROR_GENERICO_CONSULTA);
				}
			}	
			
			boolean asumeUnaEntidadHabilitada = datosBasicosSolicitud.getIdUsuarioRegistra() != null && datosBasicosSolicitud.getEntidad() != null && !"".equals(datosBasicosSolicitud.getEntidad());
			
			String cif, dni, nif, nombre, apellido1, apellido2, email, telefono;
			String cifTitular, dniTitular, nifTitular, nombreTitular, apellido1Titular, apellido2Titular;
			if (asumeUnaEntidadHabilitada) {
				
				// necesaria lectura de la Entidad Habilitada en Base de Datos
				EntidadBean entidadUsuarioRegistroEntiHabil = ParametrosDAO.obtenerEntidadPorCd(datosBasicosSolicitud.getEntidad());
				
				cif = nm.nvl(entidadUsuarioRegistroEntiHabil.getCif());
				dni = nm.nvl(entidadUsuarioRegistroEntiHabil.getDni());
				nif = nm.nvl("");
				nombre = nm.nvl(entidadUsuarioRegistroEntiHabil.getDenominacion());
				apellido1 = nm.nvl("");
				apellido2 = nm.nvl("");
				
		
				
				cifTitular = nm.nvl(resRecdNotificacion.getSOLIC_AYUDA().getCif());
				dniTitular = nm.nvl(resRecdNotificacion.getSOLIC_AYUDA().getDni());
				nifTitular = nm.nvl(resRecdNotificacion.getSOLIC_AYUDA().getNif());
				nombreTitular = nm.nvl(resRecdNotificacion.getSOLIC_AYUDA().getRazonSocial());
				apellido1Titular = nm.nvl(resRecdNotificacion.getSOLIC_AYUDA().getApellido1Solicitante());
				apellido2Titular = nm.nvl(resRecdNotificacion.getSOLIC_AYUDA().getApellido2Solicitante());
			} else {
				if (!existeRepLegal) {
					
					cif = nm.nvl(resRecdNotificacion.getSOLIC_AYUDA().getCif());
					dni = nm.nvl(resRecdNotificacion.getSOLIC_AYUDA().getDni());
					nif = nm.nvl(resRecdNotificacion.getSOLIC_AYUDA().getNif());
					nombre = nm.nvl(resRecdNotificacion.getSOLIC_AYUDA().getRazonSocial());
					apellido1 = nm.nvl(resRecdNotificacion.getSOLIC_AYUDA().getApellido1Solicitante());
					apellido2 = nm.nvl(resRecdNotificacion.getSOLIC_AYUDA().getApellido2Solicitante());
					
					// el titular siempre existe y ser� el interesado
					cifTitular = nm.nvl(resRecdNotificacion.getSOLIC_AYUDA().getCif());
					dniTitular = nm.nvl(resRecdNotificacion.getSOLIC_AYUDA().getDni());
					nifTitular = nm.nvl(resRecdNotificacion.getSOLIC_AYUDA().getNif());
					nombreTitular = nm.nvl(resRecdNotificacion.getSOLIC_AYUDA().getRazonSocial());
					apellido1Titular = nm.nvl(resRecdNotificacion.getSOLIC_AYUDA().getApellido1Solicitante());
					apellido2Titular = nm.nvl(resRecdNotificacion.getSOLIC_AYUDA().getApellido2Solicitante());
					
				} else {
					
					cif = nm.nvl(resRecdNotificacion.getSOLIC_AYUDA().getCifRepLegal());
					dni = nm.nvl(resRecdNotificacion.getSOLIC_AYUDA().getDniRepLegal());
					nif = nm.nvl(resRecdNotificacion.getSOLIC_AYUDA().getNifRepLegal());
					nombre = nm.nvl(resRecdNotificacion.getSOLIC_AYUDA().getNombreRepLegal());
					apellido1 = nm.nvl(resRecdNotificacion.getSOLIC_AYUDA().getApellido1RepLegal());
					apellido2 = nm.nvl(resRecdNotificacion.getSOLIC_AYUDA().getApellido2RepLegal());
			
					
 					cifTitular = nm.nvl(resRecdNotificacion.getSOLIC_AYUDA().getCif());
					dniTitular = nm.nvl(resRecdNotificacion.getSOLIC_AYUDA().getDni());
					nifTitular = nm.nvl(resRecdNotificacion.getSOLIC_AYUDA().getNif());
					nombreTitular = nm.nvl(resRecdNotificacion.getSOLIC_AYUDA().getRazonSocial());
					apellido1Titular = nm.nvl(resRecdNotificacion.getSOLIC_AYUDA().getApellido1Solicitante());
					apellido2Titular = nm.nvl(resRecdNotificacion.getSOLIC_AYUDA().getApellido2Solicitante());
				}
			}
			
			// el aviso se har� en todos los casos seg�n lo indicado en la solicitud
			email = nm.nvl(resRecdNotificacion.getNotif().getEmailAutorizoNotifica());
			telefono = nm.nvl(resRecdNotificacion.getNotif().getTelefMovilAutorizoNotifica());
			
			try {
				// se indica el tipo de gesti�n/firmable que se notifica para la localizaci�n cuando sea necesario de la misma
				// a partir de la informaci�n de la notificaci�n en p_NOTIFICACION
				Integer cdGestion = 15; // Resoluci�n de Recurso (firmable)
				
				// encolar notificaci�n para su env�o
				idNotifica = nm.encolaNotificacion(fichero, TipoDocumentalDocumento.TD01, cdGestion, asunto, numExpediente, cif, dni,
						nif, nombre, apellido1, apellido2, email, telefono, convocatoriaLinea, 
						cifTitular, dniTitular, nifTitular, nombreTitular, apellido1Titular, apellido2Titular);

				// guardamos la relaci�n entre la notificaci�n y el acto notificado
				RecursoResolucionDAO.insertaNotificacionActoNotificado(resRecdNotificacion.getIdPortDocPet(),
						idNotifica, resRecdNotificacion.getIdDocFirmado());
				
				// No se implementa "si existe una notificaci�n manual activa entonces se pasa a inactiva"
				// porque si existe notificaci�n manual activa es que la notificaci�n electr�nica est� bloqueada
				
				this.notificacionElectronica = NotificacionDAO.selectNotificacionById (idNotifica);
				
				if (this.envioAnticipado) {
					nm.enviaAnticipadoNotificacion (this.notificacionElectronica);
				}
				
				this.notificacionElectronica = NotificacionDAO.selectNotificacionById (idNotifica);
				
				encoladosOK++;

			} catch (Exception e) {
				if (idNotifica != null) {
					try {nm.anulaNotificacion(idNotifica);} catch(Exception ns){LOG.error(ns, ns);}
				}
				encoladosKO++;
				LOG.error(e, e);
			} finally {
				//actualizarEstadoNotificacion();
			}
		}

		if (encoladosKO == 0) {
			if (this.envioAnticipado)
				dus.muestraMensaje(CtesGenerales.INFO, "La notificaci�n fue enviada con �xito.");
			else
				dus.muestraMensaje(CtesGenerales.INFO, "La notificaci�n fue preparada para env�o con �xito.");
		} else if (encoladosOK == 0) {
			if (this.envioAnticipado)
				dus.muestraMensaje(CtesGenerales.INFO, "No fue posible el env�o de la notificaci�n. Int�ntelo m�s tarde.");
			else
				dus.muestraMensaje(CtesGenerales.INFO, "No fue posible la preparaci�n para env�o de la notificaci�n. Int�ntelo m�s tarde.");
		}
		
	}
		
		actualizaMensajeNotificacion();
		
		actualizaActivacionPanelesNotificacion();
		
		setEnvioAnticipado(true);
	}	
	
	
	
// ...

	
}
