package src.edu.desarrollo.tramitador.tramite.estado;


public class GestionEstadoBean {

	// logger
	private static final Logger LOG = Logger.getLogger(GestionEstadoBean.class);

	// ...

	/**
	 * M�todo que comprueba si la petici�n ha cambiado de estado.
	 * 
	 * @param peticion
	 */
	public boolean comprobarCambioEstadoPeticionBySolicitud(PeticionPortafirmaBean peticion) {

			// ...

			// Si hemos actualizado el estado a firmada o descartada hay que
			// complementar el
			// cambio de estado con acciones sobre la solicitud.
			if (estado != peticion.getEstado().getEstado() && (peticion.getEstado().getEstado().equals(Estado_portafirma_enum.FIRMADO.getValue())
					|| peticion.getEstado().getEstado().equals(Estado_portafirma_enum.DESCARTADO.getValue()))) {
				complementarCambioEstadoPeticion(peticion);
			}

		} else {
			dus.muestraMensaje("Error", "No se ha podido comunicar con portafirmas");
			return false;

		}
		return true;
	}


	private void complementarCambioEstadoPeticion(PeticionPortafirmaBean peticion) {
		DataBase db = new DataBase();
		switch (peticion.getTipoPeticion()) {

		// ...
		
			break;
		case TIPO_PET_PORT_RECRES_RES_IND:
			// el paso a estado RECURSO RESUELTO se hace cuando existe una fecha de acuse (aceptaci�n/rechazo) 
			// de notificaci�n (electr�nica/fuera de TRAMITADOR)
			if (!dus.getConvocatoriaLinea().contentEquals(CtesGenerales.CONVOCATORIA)) {
				LOG.info("Cambiamos el estado del Tr�mite a estado Finalizado ya que se ha firmado la petici�n de portafirmas.");
				try {
					db.connect(dus);			
					SolicitudAyudaDAO.actualizaEstado(db, Integer.toString(peticion.getIdSOLIC_AYUDA()), CtesGenerales.ESTADO_RECURSO_RESUELTO);
					db.commit();
				} catch (Exception e) {
					LOG.error("Error al finalizar el control del pago DT  " + peticion.getIdPrecontrolPago(), e);
				} finally {
					try {
						db.disconnect();
						db.getCon().close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
			break;
		case TIPO_PET_PORT_LC4:
			
			// ...
			
			break;
		default:
			break;
		}

	}


	// ...

}
