package src.edu.desarrollo.tramitador.tramite.model;



public class DocPortafirmaNotifica {

	// Relaciona la notificación con el documento notificado del procedimiento, que ya fue gestionado a través de Portafirma 
	//r_NOTIF_PORT_DOC
	
	//ID_NOTIF_PORT_DOC
	private int identificador;
	//ID_PORT_DOC_PET	
	private int idPortDocPet;
	//ID_NOTIFICACION
	private int idNotificacion;
	
	// ...
	
	
	public int getIdentificador() {
		return identificador;
	}
	public void setIdentificador(int identificador) {
		this.identificador = identificador;
	}
	public int getIdPortDocPet() {
		return idPortDocPet;
	}
	public void setIdPortDocPet(int idPortDocPet) {
		this.idPortDocPet = idPortDocPet;
	}
	public int getIdNotificacion() {
		return idNotificacion;
	}
	public void setIdNotificacion(int idNotificacion) {
		this.idNotificacion = idNotificacion;
	}

	// ...
	
	
	
	
}
