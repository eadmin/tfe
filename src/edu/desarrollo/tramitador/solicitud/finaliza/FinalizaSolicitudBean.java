package src.edu.desarrollo.tramitador.solicitud.finaliza;




@ManagedBean(
		name = "finalizaSolicitud")
@ViewScoped
public class FinalizaSolicitudBean extends FinalizaBean implements IFinalizaBean{
	
	private static final Logger LOG = Logger.getLogger(FinalizaSolicitudBean.class);
		
	private String provinciaGestion;
	private String idOficinaComarcal;
	private String razonSocialRepLegal;
	private String nifRepLegal;
	private Integer idSolicitudRgActual;
	
	private StreamedContent ficheroDescarga;
	
	
	
	public FinalizaSolicitudBean() {
		super();
		prepararReportAFirmar();
		// TODO Auto-generated constructor stub
		provinciaGestion = Integer.toString(SolicitudBean.getProvinciaGestion());
		idOficinaComarcal = Integer.toString(SolicitudBean.getOficinaComarcal());
		razonSocialRepLegal = (SolicitudBean.getNombreRepLegal()==null?"":" " + SolicitudBean.getNombreRepLegal()) + (SolicitudBean.getApellido1RepLegal()==null?"":" " + SolicitudBean.getApellido1RepLegal()) + (SolicitudBean.getApellido2RepLegal()==null?"":" " + SolicitudBean.getApellido2RepLegal());
		nifRepLegal = (SolicitudBean.getCifRepLegal()==null?"":SolicitudBean.getCifRepLegal()) + (SolicitudBean.getDniRepLegal()==null?"":SolicitudBean.getDniRepLegal()) + (SolicitudBean.getNifRepLegal()==null?"":SolicitudBean.getNifRepLegal());

		if (provinciaGestion == null || provinciaGestion.equals("0")){			
			provinciaGestion = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("provinciaGestionFirmaTelematica");
			idOficinaComarcal = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("oficinaComarcalFirmaTelematica");
			razonSocialRepLegal = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("razonSocialrepresentanteLegalFirmaTelematica");
			nifRepLegal = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("dnirepresentanteLegalFirmaTelematica");
			idSolicitudRgActual = (Integer) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("idSolicitudRgActual");
		}
	}

	/**
	 * Metodo para registrar el fichero telematicamente
	 */
	public String ficheroRegistrarTelematicamente() {
		

		
		LOG.debug("ficheroRegistrarTelematicamente: 1-Obtener solicitud");


		LOG.debug("2-Generacion del numero de expediente");


		LOG.debug("3-Generado numero de expediente: " + nuExpediente);

		
		LOG.debug("4-Registra solicitud");


		LOG.debug("5-Registra solicitud: " + nuExpediente);


		LOG.debug("7-solicitud registrada expediente : " + nuExpediente);


		
		// Alta anticipada en Notifica de personas f�sicas y/o jur�dica
		
		// API 
		NotificaManager nm = null;
		// Condiciones
		boolean UsuarioHabilitadoAsumeNotificaciones = false;
		boolean InteresadoTieneRepresentanteLegal = false;		
		// Datos de la solicitud
		SARepLegal repLegal = new SARepLegal();
		SALugarNotifYDatosBancarios lugarNotifDatosBancarios = new SALugarNotifYDatosBancarios();
		SASolicitud solicitud = new SASolicitud();	
		SASolicitante interesado = new SASolicitante();
		EntidadBean entidadUsuarioRegistroEntiHabil = new EntidadBean();
		UsuarioBean usuarioRegistroEntiHabil = new UsuarioBean();
		// altas
		List<Abonado> altasAbonados = new ArrayList<Abonado>();
		
		try {
			
			DataBase db = new DataBase();
			try {
				try {
					db.connect();
					LOG.debug("Notifica - Lectura de Solicitud");
					solicitud = SASolicitudDao.obtenerSolicitud(Integer.valueOf(SolicitudBean.getIdSolicitud()), db);
					LOG.debug("Notifica - Lectura de Interesado");
					interesado = SASolicitanteDao.obtenerSolicitante(Integer.valueOf(SolicitudBean.getIdSolicitud()), db);
					LOG.debug("Notifica - Lectura de Representante Legal");
					repLegal = SARepLegalDao.obtenerRepresentante(SolicitudBean.getIdSolicitud(), db);
					LOG.debug("Notifica - Lectura de Lugar de Notificaci�n");
					SALugarNotifYDatosBancariosDao.obtenerLugarNotificacionSolicitud(lugarNotifDatosBancarios, SolicitudBean.getIdSolicitud(), db);
					
				} catch (Exception e) {
					LOG.debug("Notifica - Finalizaci�n de lecturas de informaci�n asociada");

				} finally {
					db.disconnect();
					db.getCon().close();
				}
				
			} catch (Exception e) {
				LOG.error(e, e);
				dus.muestraMensaje(CtesGenerales.ERROR, CtesErrores.TXT_ERROR_GENERICO);
			}	
			
			try {
			
				if (solicitud.getIdUsuarioRegistra() != null && solicitud.getEntidad() != null && !"".equals(solicitud.getEntidad())) {
					LOG.debug("Notifica - Lectura de Usuario que Registra que exist�a");
					usuarioRegistroEntiHabil = UsuariosDAO.recuperaDatosUsuario(solicitud.getIdUsuarioRegistra()).get(0);
					LOG.debug("Notifica - Lectura de Entidad de Usuario que Registra que exist�a");
					entidadUsuarioRegistroEntiHabil = ParametrosDAO.obtenerEntidadPorCd(solicitud.getEntidad());
				} else {
					LOG.debug("Notifica - Lectura de Usuario que Registra que NO exist�a");
					usuarioRegistroEntiHabil = UsuariosDAO.recuperaDatosUsuario(dus.getUsuarioLogueado().getIdentificador()).get(0);
					LOG.debug("Notifica - Lectura de Entidad de Usuario que Registra que NO exist�a");
					entidadUsuarioRegistroEntiHabil = ParametrosDAO.obtenerEntidadPorCd(dus.getUsuarioLogueado().getCdEntidad());
				}
				LOG.debug("Notifica - Establecimiento de condiciones para altas");
				LOG.debug("Notifica - InteresadoTieneRepresentanteLegal");
				InteresadoTieneRepresentanteLegal = repLegal!=null && repLegal.getNombreRepLegal()!=null && !repLegal.getNombreRepLegal().equals("");
				LOG.debug("Notifica - UsuarioHabilitadoAsumeNotificaciones");
				UsuarioHabilitadoAsumeNotificaciones = lugarNotifDatosBancarios!=null && lugarNotifDatosBancarios.getLugarNotificacion()!=null && lugarNotifDatosBancarios.getLugarNotificacion().isNotificaEntHabi();
				LOG.debug("Notifica - Finalizaci�n de lecturas");
			
			} catch (Exception e) {
				LOG.debug("Notifica - Finalizaci�n de lecturas de informaci�n asociada (2)");
				
			}
			
			
			nm = new NotificaManager(dus);
			
			// PARA PRUEBAS: Baja abonado anterior al alta: nm.bajaAbonadoNotifica(abonado);		
			LOG.debug("Notifica - Interesado");
			// Alta interesado. Siempre. Mapeo SASolicitante a Abonado
			Abonado abonadoInteresado = new Abonado(interesado.getDni());
			abonadoInteresado.setApellidos(nvl(interesado.getApellido1Solicitante()) + " " + nvl(interesado.getApellido2Solicitante()));

			abonadoInteresado.setEmail(interesado.getEmail());
			abonadoInteresado.setNombre(interesado.getRazonSocial());
			abonadoInteresado.setTelefonoFijo(interesado.getTelefono());
			abonadoInteresado.setTelefonoMovil(interesado.getFax());
			
			altasAbonados.add(abonadoInteresado);
			
			if (!UsuarioHabilitadoAsumeNotificaciones) {
				if (InteresadoTieneRepresentanteLegal) {
					LOG.debug("Notifica - Rep. Legal");
					// Alta representante legal. Mapeo SARepLegal a Abonado
					Abonado abonadoRepLegal = new Abonado(repLegal.getDniRepLegal());
					abonadoRepLegal.setApellidos(nvl(repLegal.getApellido1RepLegal()) + " " + nvl(repLegal.getApellido2RepLegal()));
					abonadoRepLegal.setEmail(repLegal.getEmailRepLegal());
					abonadoRepLegal.setNombre(repLegal.getNombreRepLegal());
					abonadoRepLegal.setTelefonoFijo(repLegal.getTelefonoRepLegal());
					abonadoRepLegal.setTelefonoMovil(repLegal.getFaxRepLegal());
					
					altasAbonados.add(abonadoRepLegal);
				}
			} else {
				LOG.debug("Notifica - Entidad Habilitada");
				// Alta entidad habilitada. Mapeo EntidadBean a Abonado
				Abonado abonadoEntHabilitada = new Abonado(entidadUsuarioRegistroEntiHabil.getCifNif());
				abonadoEntHabilitada.setNombre(nvl(entidadUsuarioRegistroEntiHabil.getDenominacion()));
				abonadoEntHabilitada.setLocalidad(entidadUsuarioRegistroEntiHabil.getLocalidad());
				
				altasAbonados.add(abonadoEntHabilitada);
			}
			
			LOG.debug("Notifica - Altas de abonados");
			
			for (Abonado abonado: altasAbonados) {
				try {
						nm.altaAbonadoNotifica(abonado);
				} catch (NotificaExcepcion e) {
					if (e.getCause() != null && e.getCause().toString().contains("El Abonado ya est� dado de alta en el servicio")) {
						LOG.error("> El usuario " + abonado.getNif() + " ya estaba dado de alta en Notifica. IdSolicitud: " + idSolicitud + ".");
					}
					LOG.error("> NotificaExcepcion AL SOLICITAR EL ALTA EN NOTIFICA DE " + abonado.getNif() + ". IdSolicitud: " + idSolicitud + ".");					
				}
			}
			
		} catch (Exception e) {
			LOG.error("> Exception AL SOLICITAR ALTAS EN NOTIFICA. IdSolicitud: " + idSolicitud + ".");
		}
		
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("idSolicitudAyuda",
				String.valueOf(SolicitudBean.getIdSolicitud()));
		
		return "solicitudAyudaConsulta";
	}
	


// ...

}
