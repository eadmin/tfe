package src.edu.desarrollo.tramitador.solicitud.dao;



public class NotificaEnSolicitudDAO {

	//
	// Propiedades.
	//
	private static final Logger LOG = Logger
			.getLogger(NotificaEnSolicitudDAO.class);

	public static String insertSolicitudNotificaNueva = " ";

	private static String updateSolicitudNotifica = " ";

	private static String updateSolicitudDatosBancarios = " ";

	private static String consultaSolicitudAyudaNotifica = " ";

	private static String consultaNumeroSolicitudAyudaNotifica = " ";

	// Metodos

	public static String insertaSolicitudNotificaNueva(String pIdSolicitud)

	{

		DataBase db = new DataBase();

		try {

			// si hacemos una select
			// db.connect();
			// si hacemos un insert o update
			Bean dus = (Bean) FacesContext
					.getCurrentInstance().getExternalContext().getSessionMap()
					.get("Bean");
			db.connect(dus);

			// Si no existe la solicitud la insertamos, sino la actualizamos

			// En la actualizacion no modificamos el usuario que grabo la
			// primera vez la solicitud
			db.update(insertSolicitudNotificaNueva,
					new Object[] { pIdSolicitud });

			db.commit();

		} catch (SQLException e) {
			LOG.error(e, e);
			return "error";
		} catch (NamingException e) {
			LOG.error(e, e);
			return "error";
		} finally {
			try {
				db.disconnect();
				db.getCon().close();
			} catch (Exception e) {
				LOG.error(e, e);
				return "error";
			}
		}
		// Si se inserto correctamente devuelvo el id
		return "ok";
	}

	public static String actualizaSolicitudNotifica(String pIdSolicitud,
			SolicitudAyudaNotifica pSolicitudAyudaNotifica)

	{

		DataBase db = new DataBase();

		// hacemos la decodificacion de false/true a 0/ 1
		String sOpto1 = pSolicitudAyudaNotifica.isOpto1() == true ? "1" : "0";
		String sDirSolicitante = pSolicitudAyudaNotifica.isDirSolicitante() == true ? "1"
				: "0";
		String sDirRepLegal = pSolicitudAyudaNotifica.isDirRepLegal() == true ? "1"
				: "0";
		String sOtraDir = pSolicitudAyudaNotifica.isOtraDir() == true ? "1"
				: "0";
		String sOpto2 = pSolicitudAyudaNotifica.isOpto2() == true ? "1" : "0";
		String sManifiestoDispongoNotifica = pSolicitudAyudaNotifica
				.isManifiestoDispongoNotifica() == true ? "1" : "0";
		String sManifiestoNoDispongoNotifica = pSolicitudAyudaNotifica
				.isManifiestoNoDispongoNotifica() == true ? "1" : "0";

		String pNumeroViaNotifica = pSolicitudAyudaNotifica
				.getNumeroViaNotifica();
		String pKmViaNotifica = pSolicitudAyudaNotifica.getKmViaNotifica();

		if (pNumeroViaNotifica != null && !pNumeroViaNotifica.isEmpty()) {
			pNumeroViaNotifica = (pNumeroViaNotifica + "").replace(".", ",");
			int posini = 0;
			int posFin = pNumeroViaNotifica.indexOf(",");
			if (posFin > 0) {
				pNumeroViaNotifica = pNumeroViaNotifica.substring(posini,
						posFin);
			}
		}

		if (pKmViaNotifica != null && !pKmViaNotifica.isEmpty()) {
			pKmViaNotifica = (pKmViaNotifica + "").replace(".", ",");
		}

		try {

			// si hacemos una select
			// db.connect();
			// si hacemos un insert o update
			Bean dus = (Bean) FacesContext
					.getCurrentInstance().getExternalContext().getSessionMap()
					.get("Bean");
			db.connect(dus);

			// Si no existe la solicitud la insertamos, sino la actualizamos

			// En la actualizacion no modificamos el usuario que grabo la
			// primera vez la solicitud
			db.update(
					updateSolicitudNotifica,
					new Object[] {
							DataBase.parseNumber(sOpto1),
							// ...

			db.commit();

		} catch (SQLException e) {
			LOG.error(e, e);
			return "error";
		} catch (NamingException e) {
			LOG.error(e, e);
			return "error";
		} finally {
			try {
				db.disconnect();
				db.getCon().close();
			} catch (Exception e) {
				LOG.error(e, e);
				return "error";
			}
		}
		// Si se inserto correctamente devuelvo el id
		return "ok";
	}

	public static String insertaSolicitudDatosBancarios(String pIdSolicitud,
			SolicitudAyudaNotifica pSolicitudAyudaNotifica)

	{
		DataBase db = new DataBase();
		Object[] array_IdSolicitud = new Object[] { new Integer(-1) };
		String idSolicitud = pIdSolicitud;

		try {

			// si hacemos una select
			// db.connect();
			// si hacemos un insert o update
			Bean dus = (Bean) FacesContext
					.getCurrentInstance().getExternalContext().getSessionMap()
					.get("Bean");
			db.connect(dus);

			// Si no existe la solicitud la insertamos, sino la actualizamos

			// En la actualizacion no modificamos el usuario que grabo la
			// primera vez la solicitud
			db.update(
					updateSolicitudDatosBancarios,
					new Object[] {
							DataBase.parseVarchar2(pSolicitudAyudaNotifica
									.getIBANCampo1()),
							// ...

			db.commit();

		} catch (SQLException e) {
			LOG.error(e, e);
			return "error";
		} catch (NamingException e) {
			LOG.error(e, e);
			return "error";
		} finally {
			try {
				db.disconnect();
				db.getCon().close();
			} catch (Exception e) {
				LOG.error(e, e);
				return "error";
			}
		}
		// Si se inserto correctamente devuelvo el id
		return idSolicitud;

	}

	public static SolicitudAyudaNotifica obtenerSolicitudAyudaNotifica(
			String pIdSolicitud) {

		SolicitudAyudaNotifica c = null;

		// Conexi�n DB
		DataBase db = new DataBase();

		try {
			// si hacemos una select
			db.connect();

			// Obtiene datos Usuario
			ResultSet rs = db.select(consultaSolicitudAyudaNotifica,
					new Object[] { pIdSolicitud });

			if (rs.next()) {
				// Guarda datos de las convocatorias de las lineas
				c = new SolicitudAyudaNotifica();

				c.setIdentificador(rs.getString(1)); // ID_SOLIC_AYUDA_NOTIF,
				c.setIdSOLIC_AYUDA(rs.getString(2)); // ID_SOLIC_AYUDA,
				// ...

			}
			db.closeRs(rs);
		} catch (SQLException e) {
			LOG.error(e, e);
		} catch (NamingException e) {
			LOG.error(e, e);
		} finally {
			try {
				db.disconnect();
				db.getCon().close();
			} catch (SQLException e) {
				LOG.error(e, e);
			}
		}
		return c;
	}

	public static int obtenerNumeroSolicitudAyudaNotifica(String pIdSolicitud) {

		int total = -1;

		// Conexi�n DB
		DataBase db = new DataBase();

		try {
			// si hacemos una select
			db.connect();

			// Obtiene datos Usuario
			ResultSet rs = db.select(consultaNumeroSolicitudAyudaNotifica,
					new Object[] { pIdSolicitud });

			if (rs.next()) {
				total = rs.getInt(1);
			}
			db.closeRs(rs);
		} catch (SQLException e) {
			LOG.error(e, e);
		} catch (NamingException e) {
			LOG.error(e, e);
		} finally {
			try {
				db.disconnect();
				db.getCon().close();
			} catch (SQLException e) {
				LOG.error(e, e);
			}
		}
		return total;
	}

}
