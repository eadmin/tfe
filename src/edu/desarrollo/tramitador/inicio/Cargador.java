package src.edu.desarrollo.tramitador.inicio;




public class Cargador {
	
	public final static Logger LOG = Logger.getLogger(Cargador.class);
	
	/**
	 * Obtiene el punto remoto de lectura de la configuración
	 * 
	 * @return Map
	 */		
	public static Map<String, String> obtieneDatosConexionConfiguracionRemota() throws MalformedURLException {

	}
	
	
	/**
	 * Lee la última versión de la configuración de la aplicación que exista en el Servidor de Configuración
	 * 
	 * @return Properties
	 */		
	public static ByteArrayOutputStream aplicaConfiguracionRemotaParaApplicationProperties() throws ClientProtocolException, UnsupportedOperationException, MalformedURLException, IOException {
		
		
	}
	
	/**
	 * Ejecuta la configuración del logger de la aplicación con la última
	 * versión de configuración que exista en el Servidor de Configuración
	 * 
	 */	
	public static void aplicaConfiguracionRemotaParaLog4j() throws ClientProtocolException, IOException {
		
			
	}
	
	public static void sacaMensajeInicializacionLogPorLog (String modo) {
		
		LOG.info("**** LA CONFIGURACION DEL LOGGER FUE REALIZADA EN " + modo + ".");
	}
	
	public static void sacaMensajeInicializacionPropertiesPorLog (String modo) {
		final Logger LOG = Logger.getLogger(Cargador.class);
		LOG.info("**** LA CARGA DE LAS PROPERTIES FUE REALIZADA EN " + modo + ".");
	}	
}
