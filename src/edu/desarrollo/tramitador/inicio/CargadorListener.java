package src.edu.desarrollo.tramitador.inicio;



public class CargadorListener implements ContextListener {
	
	// logger
	private static final Logger LOG = Logger.getLogger(CargadorListener.class);	
	
	// Se a�ade nuevo CRON, para la gesti�n de las notificaciones: Env�o, Reenv�o de emisiones
	//  fallidas, sincronizaci�n de estado no finales y registro en log de env�os no posibles (desistidos)

    Scheduler scheduler = null;	

    @Override
    public void contextInitialized(ContextEvent Context) {
    	    	
        try { 
        	

        	
        	// inicializaci�n de logger con valores remotos
        	Cargador.aplicaConfiguracionRemotaParaLog4j(); 
        	

        	
        	// las propiedades remotas se leen la primera vez aqu� para evitar problemas por multihilo
        	CargadorSingleton.getInstance();
        	

        	
        	// Llamada a l�gica de antigua clase listener Planificador
        	contextInitializedPlanificadorListener(Context);
        	

        	
        } catch (Exception e) {

        	LOG.error(">>> ERROR CARGANDO LISTENERS DE CONTEXTO: " + e.getMessage());
        	LOG.error(e,e);
        } 
	}
    
    @Override
    public void contextDestroyed(ContextEvent Context) { 
    	// Llamada a l�gica de antigua clase listener Planificador
    	contextDestroyedPlanificadorListener(Context);
    }
    
    public void contextInitializedPlanificadorListener(ContextEvent Context) throws Exception {
    	if (true) return;
    	PropertyResourceBundle bundle = CargadorSingleton.getInstance().getPropertyResourceBundle();
    	
        String planificacionNotifica = bundle.getString("notifica.cron.planificacion");          	
    
            
      	try {
      			// NOTA: Si fuera necesario que existiese un tiempo entre la inicializaci�n de la aplicaci�n primero
      			// y a continuaci�n la del Scheduler, meter el c�digo de este m�todo en una clase que extienda de Thread                                                                        
      			// o bien implemente Runnable, instanciarla aqu� y ejecutarla con start, a�adiendo en el c�digo
      			// un retardo con sleep

        		// NOTA: Es posible activar/desactivar cada proceso (Job) del Planificador desde la aplicaci�n y/o
      			//	desde la base de datos (tabla PLANIFICADOR). El uso de la base de datos es obligado al
      			// 	operar TRAMITADOR en cluster y no disponer de elementos como EJB o similares
      			
      			// Copia valores iniciales (en properties) a valores de referencia (base de datos)
      		
        		String planificadorEstadoPFirmas  = bundle.getString("planificador.estado");
        		String planificadorEstadoNotifica  = bundle.getString("notifica.cron.estado");
        		
        		PlanificadorDAO.actualizarValor(null, "REV_PFIRMAS_ACTIVO", planificadorEstadoPFirmas.equalsIgnoreCase("activo")?"1":"0");
        		PlanificadorDAO.actualizarValor(null, "NOTIFICADOR_ACTIVO", planificadorEstadoNotifica.equalsIgnoreCase("activo")?"1":"0");
        		
      		
      			String horaCorte = bundle.getString("planificador.horaCorteJob"); 
                
    
                       
                JobDetail job = newJob(PlanificadorJob.class).withIdentity(
                        "CronQuartzJob", "Group").build();
                JobDetail jobNotifica = newJob(NotificaJob.class).withIdentity(
                		"CronQuartzJobNotifica", "Group").build();
        
                

                // Disparador para bajada de ficheros firmados en Portafirmas
                Trigger trigger = newTrigger()
                .withIdentity("TriggerName", "Group")
                .withSchedule(CronScheduleBuilder.cronSchedule(planificacion))
                .build();
                // Disparador para gesti�n de Notificaciones
                Trigger triggerNotifica = newTrigger()
                .withIdentity("TriggerNameNotifica", "Group")
                .withSchedule(CronScheduleBuilder.cronSchedule(planificacionNotifica))
                .build();
                
                
                
                // introducimos la inicializaci�n con el fichero de propiedades "propiedades"
                //  para indicar la configuraci�n del modo CLUSTERED (TRAMITADOR se despliega en dos nodos)
                Properties propiedades = new Properties();
                
                // VALOR: 3
                propiedades.put("org.quartz.threadPool.threadCount", bundle.getString("planificador.org.quartz.threadPool.threadCount"));
                // VALOR: org.quartz.impl.jdbcjobstore.JobStoreTX     	        
    	        propiedades.put("org.quartz.jobStore.class", bundle.getString("planificador.org.quartz.jobStore.class"));
                // VALOR: org.quartz.impl.jdbcjobstore.WebLogicDelegate
    	        // VALOR: org.quartz.impl.jdbcjobstore.oracle.weblogic.WebLogicOracleDelegate
    	        propiedades.put("org.quartz.jobStore.driverDelegateClass", bundle.getString("planificador.org.quartz.jobStore.driverDelegateClass"));
                // VALOR: QRTZ_
    	        propiedades.put("org.quartz.jobStore.tablePrefix", bundle.getString("planificador.org.quartz.jobStore.tablePrefix"));
    	        // VALOR: myDS
    	        propiedades.put("org.quartz.jobStore.ORGIENDATOS", bundle.getString("planificador.org.quartz.jobStore.ORGIENDATOS"));
    	        // VALOR: true
    	        propiedades.put("org.quartz.jobStore.isClustered", bundle.getString("planificador.org.quartz.jobStore.isClustered"));
    	        // VALOR: false
    	        propiedades.put("org.quartz.jobStore.useProperties", bundle.getString("planificador.org.quartz.jobStore.useProperties"));
    	        // VALOR: jdbc/TRAMITADOR
    	        propiedades.put("org.quartz.ORGIENDATOS.myDS.jndiURL", bundle.getString("planificador.org.quartz.ORGIENDATOS.myDS.jndiURL"));                    
                                 
                
                // Setup the Job and Trigger with Scheduler & schedule jobs
                scheduler = new StdSchedulerFactory(propiedades).getScheduler();
                
                // elimina datos de las tablas de trabajo propias de Quartz en base de datos (QRTZ_)
                scheduler.clear();    
                
                // inicia CRON
                scheduler.start();
                scheduler.scheduleJob(job, trigger);
                scheduler.scheduleJob(jobNotifica, triggerNotifica);
                                   
                
                LOG.info("Iniciado CRON Notifica: [" + planificacionNotifica + "]\r\n  - Hora corte: " + "NO DEFINIDA" + "\r\n  - Estado inicial: " + planificadorEstadoNotifica);
                
                LOG.info("Informaci�n del Scheduler tras inicio:" + "\r\n - isInStandbyMode: " + (scheduler.isInStandbyMode()?"TRUE":"FALSE") + 
                		"\r\n - isShutdown: " + (scheduler.isShutdown()?"TRUE":"FALSE") + 
                		"\r\n - isStarted: " + (scheduler.isStarted()?"TRUE":"FALSE"));
                
        }
        catch (SchedulerException e) {
        	LOG.error(e,e);
        }
        catch (Exception e) {
        	LOG.error(e,e);
        }   	
    }
    
    private void contextDestroyedPlanificadorListener(ContextEvent Context) {
    	
    	PropertyResourceBundle bundle = CargadorSingleton.getInstance().getPropertyResourceBundle();
    	
    	String planificacion = bundle.getString("planificador.planificacion");
    	String planificacionNotifica = bundle.getString("notifica.cron.planificacion"); 

            try 
            {
                if (scheduler!=null) {
                    scheduler.shutdown();
                    LOG.info("Detenido CRON Notifica: [" + planificacionNotifica + "]");
                    
                    LOG.info("Informaci�n del Scheduler tras parada:" + "\r\n - isInStandbyMode: " + (scheduler.isInStandbyMode()?"TRUE":"FALSE") + 
                    		"\r\n - isShutdown: " + (scheduler.isShutdown()?"TRUE":"FALSE") + 
                    		"\r\n - isStarted: " + (scheduler.isStarted()?"TRUE":"FALSE"));                
				}  
            } 
            catch (SchedulerException e) 
            {
            	LOG.error(e,e);
            }  	
    }    
}
