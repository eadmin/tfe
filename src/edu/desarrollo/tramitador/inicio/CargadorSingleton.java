package src.edu.desarrollo.tramitador.inicio;



public class CargadorSingleton {
	
	
    private static CargadorSingleton cargadorSingleton = new CargadorSingleton();

    private Properties properties = null;
    private PropertyResourceBundle propertyResourceBundle = null;
    
    private CargadorSingleton() {     	
    	
    }

    public static CargadorSingleton getInstance() {
    	
    	// si se realiz� la carga remota, no se remite
    	if (!(cargadorSingleton!=null && 
    			cargadorSingleton.propertyResourceBundle!=null &&  
    			cargadorSingleton.properties!=null)) {
    		
	    	// la l�gica de inicializaci�n no puedo meterla en el constructor porque ah� el objeto a�n no est� construido
	    	InputStream inStream = null;
	    	ByteArrayOutputStream outStream = null;
	    	try {
	    		outStream = Cargador.aplicaConfiguracionRemotaParaApplicationProperties();   
	    		inStream = new ByteArrayInputStream(outStream.toByteArray());
	    		
	    		// proporciono en el singleton las dos estructuras que se usaban originalmente en la aplicaci�n
	    		//	para que el impacto del paso a remoto sea m�nimo
	    		cargadorSingleton.propertyResourceBundle = new PropertyResourceBundle(inStream);
	    		cargadorSingleton.properties = new Properties(); 
	    		inStream = new ByteArrayInputStream(outStream.toByteArray()); 
	    		cargadorSingleton.properties.load(inStream);
	    		
	    	} catch (Exception e) {
	    		Logger.getLogger(CargadorSingleton.class).error("SE PRODUJO UN ERROR EN LA INICIALIZACION DEL SINGLETON DE CONFIGURACION REMOTA.",e);
	    	}  
    	}
        return cargadorSingleton; 
    }

	public Properties getProperties() {
		return properties;
	}

	public PropertyResourceBundle getPropertyResourceBundle() {
		return propertyResourceBundle;
	}
}
