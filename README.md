# TFE

## Name
Trabajo Fin de Grado Estudio de caso de una integración de un tramitador electrónico con el servicio de notificación Notific@. UNIR/2024


## Description
Este proyecto de Eclipse no es instalable y su único propósito es servir como referencia a los contenidos presentados en el documento de TFE.


## Usage
Este proyecto de Eclipse contiene ficheros que pueden no estar completos y contener sólo algunos fragmentos de los contenidos originales. 

Además, en todos ellos se han cambiado los identificadores por cuestiones de seguridad. 

Por tanto, el proyecto no es ejecutable, ni tampoco lo es el código contenido en los ficheros, aunque sí será útil como base en otros desarrollos. 


## License
GNU GENERAL PUBLIC LICENSE
